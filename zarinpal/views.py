# -*- coding: utf-8 -*-
# Github.com/Rasooll
from django.http import JsonResponse
from django.core.mail import send_mail
from django.shortcuts import redirect
from zeep import Client
from profiles.models import HomeReservation, ExperienceReservation

MERCHANT = 'e2505052-a379-11e8-a01b-005056a205be'
client = Client('https://www.zarinpal.com/pg/services/WebGate/wsdl')
description = "توضیحات مربوط به تراکنش را در این قسمت وارد کنید"  # Required
email = 'parmisstavassoli@gmail.com'  # Optional
mobile = '09393399545'  # Optional
error_codes = {1: 'Information submitted is incomplete.', 2: 'Merchant ID or Acceptor IP is not correct.', 
    3: 'Amount should be above 100 Toman.', 4: 'Approved level of Acceptor is Lower than silver.',
    11: 'Request not found.', 12: 'Request editing is not possible',
    21: 'Financial operations for this transaction was not found.',
    22: 'Transaction is unsuccessful.', 33: 'Transaction amount does not match the amount paid.',
    34: 'Limit the number of transactions or number has crossed the divide',
    40: 'This method is unaccessable', 41: 'Additional Data related to information submitted is invalid.',
    54: 'Request archived', 100: 'Operation was successful', 
    101: 'Operation was successful but PaymentVerification operation on this transaction have already been done.'}
client_errors = {3, 22}


def send_request(request):
    uuid = request.GET.get('uuid')
    request_type = request.GET.get('type')
    if request_type == 'home':
        amount = HomeReservation.objects.get(uuid=uuid).price
    elif request_type == 'experience':
        amount = ExperienceReservation.objects.get(uuid=uuid).price
    else:
        message = 'The request is not valid.'
        explanation = 'Reservation type is not valid.'
        status_code = 400
        return JsonResponse({'message':message,'explanation':explanation}, status=status_code)
    CallbackURL = 'http://localhost:8000/verify/?type=' + request_type + "&uuid=" + uuid # Important: need to edit for real server.

    result = client.service.PaymentRequest(MERCHANT, amount, description, email, mobile, CallbackURL)
    if result.Status == 100:
        return redirect('https://www.zarinpal.com/pg/StartPay/' + str(result.Authority))
    else:
        if result.Status in client_errors:
            return JsonResponse({'message': error_codes[result.Status], 'explanation': ''}, status=400)
        return JsonResponse({'message': 'The payment method is currently unavailable.','explanation': ''}, status=400)


def verify(request):
    # if request.GET.get('Status') == 'OK':
    request_type = request.GET.get('type')
    uuid = request.GET.get('uuid')
    # if not request_type:
    #     return JsonResponse({'message': 'Type field is required.', 'explanation': 'Invalid request'}, status=400)
    # if not uuid:
    #     return JsonResponse({'message': 'UUId field is required.', 'explanation': 'Invalid request'}, status=400)
    if request_type == 'home':
        reservation = HomeReservation.objects.get(uuid=uuid)
    elif request_type == 'experience':
        reservation = ExperienceReservation.objects.get(uuid=uuid)
    # else:
    #     return JsonResponse({'message': 'Invalid type field.', 'explanation': 'Invalid request'}, status=400)

    result = client.service.PaymentVerification(MERCHANT, request.GET['Authority'], reservation.price)
    payment_result = False
    if result.Status == 100:
        payment_result = reservation.pay(reservation.price)
        if payment_result:
            subject = 'New Payment'
            message = 'New {} Payment!'.format(request_type)
            send_mail(subject, message, 'info@jadehkhaki.com', ['info@jadehkhaki.com'])
    #     if payment_result:
    #         return JsonResponse({'message': 'Transaction success.', 'explanation': 'RefID: ' + str(result.RefID)}, status=200)
    #     else:
    #         return JsonResponse(
    #             {'message': 'Transaction succeeded, but payment amount does not match the reservation price',
    #              'explanation': 'RefID: ' + str(result.RefID)}, status=400)
    #
    # elif result.Status == 101:
    #     return JsonResponse({'message': 'Transaction submitted.', 'explanation': ''}, status=200)
    #
    # elif result.Status in client_errors:
    #     return JsonResponse({'message': error_codes[result.Status], 'explanation': ''}, status=400)
    # else:
    #     return JsonResponse({'message': 'Transaction failed or cancelled by user.', 'explanation': ''}, status=400)

    # else:
    #     return JsonResponse({'message':'Transaction failed or cancelled by user.', 'explanation':''}, status=400)
    return redirect('/profile#orders/?payment={}'.format(payment_result))
