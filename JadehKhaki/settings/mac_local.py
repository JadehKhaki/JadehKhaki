from .base import *
DEBUG=True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'i6)i3=(b7wh(4kqlyud%%+49-fk*xsf0yj1ln5a=1*n#$)h4x)'

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'jadetest',
        'USER': 'jadeuser',
        'PASSWORD': 'jade123',
        'HOST': 'localhost',
        'PORT': '5433',
    }
}

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

MEDIA_URL = '/static/media/'
