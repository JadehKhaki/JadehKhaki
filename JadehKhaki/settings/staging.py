import json

from .base import *


# JSON-based secrets module
with open('secrets.json') as f:
    secrets = json.loads(f.read())


# Get the secret variable or return explicit exception.
def get_secret(setting, secrets=secrets):
    try:
        return secrets[setting]
    except KeyError:
        error_msg = 'Set the {0} environment variable'.format(setting)
        raise ImproperlyConfigured(error_msg)

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = ['104.131.110.186', 'arzanfud.com', ]

SECRET_KEY = get_secret('SECRET_KEY')

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': get_secret('DATABASE_NAME'),
        'USER': get_secret('DATABASE_USER'),
        'PASSWORD': get_secret('DATABASE_PASS'),
        'HOST': 'localhost',
        'PORT': get_secret('DATABASE_PORT'),
        'CONN_MAX_AGE': 300,
    }
}

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.IsAdminUser',
    ],
    'DEFAULT_RENDERER_CLASSES': [
        'rest_framework.renderers.JSONRenderer',
    ]
}

EMAIL_USE_TLS = get_secret('EMAIL_USE_TLS')
EMAIL_HOST = get_secret('EMAIL_HOST')
EMAIL_HOST_USER = get_secret('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = get_secret('EMAIL_HOST_PASSWORD')
EMAIL_PORT = get_secret('EMAIL_PORT')

ADMINS = [('paromise', 'paromise@jadehkhaki.com'), ('golrokh', 'gol.hamidi@gmail.com')]

SECURE_SSL_REDIRECT = True

MEDIA_URL = 'https://jadehkhaki.ir/static/media/'
