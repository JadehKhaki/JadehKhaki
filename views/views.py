from django.core.mail import send_mail
from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt, ensure_csrf_cookie
from rest_framework import generics, permissions
from rest_framework.parsers import JSONParser
from django.urls import resolve

from homes.models import Home
from experiences.models import Experience

def index(request, arg1=None):
    return render(request, 'index.html', {'page_file': "homePage.html", 'page_title': 'جاده‌ خاکی | تجربه‌ی بومی | اقامت بومی',
                                        'page_description': "پل ارتباطی میان گردشگران و جامعه‌ی محلی برای اشتراک تجربه‌های هیجان‌انگیز"})

def homes(request, arg1=None):
    items = Home.objects.values_list('title', 'url_title')
    return render(request, 'index.html', {'page_file': "items.html", 'category': 'home', 'h1': 'اقامتگاه‌ها', 'page_title': "جاده‌خاکی | اقامتگاه‌های بومی",
                                        'page_description': "تجربه اقامت در اقامتگاه‌های بومی", 'items': items})

def experiences(request, arg1=None):
    items = Experience.objects.values_list('title', 'url_title')
    return render(request, 'index.html', {'page_file': "items.html", 'category': 'experience', 'h1': 'تجربه‌ها', 'page_title': "جاده‌خاکی | تجربه‌های بومی",
                                        'page_description': "شرکت در تجربه‌های بومی", 'items': items})

def home_item(request, home_id=''):
    obj = Home.objects.get(url_title=home_id)
    address = str(obj.address.city_town.province) + ", " + str(obj.address.city_town.city) + ", " + str(obj.address.city_town.town)
    description = obj.description_tag
    if not description:
        description = obj.explanation
    return render(request, 'index.html', {'page_file': "homeItem.html", 'page_title': "جاده‌خاکی | " + obj.title, 'page_description': description,
                                        'home_title': obj.title, 'home_explanation': obj.explanation, 'home_tiny_explanation': obj.tiny_explanation,
                                        'home_address':  address})

def experience_item(request, experience_id=''):
    obj = Experience.objects.get(url_title=experience_id)
    address = str(obj.address.city_town.province) + ", " + str(obj.address.city_town.city) + ", " + str(obj.address.city_town.town)
    description = obj.description_tag
    if not description:
        description = obj.explanation
    return render(request, 'index.html', {'page_file': "experienceItem.html", 'page_title': "جاده‌خاکی | " + obj.title, 'page_description': description,
                                        'experience_title': obj.title, 'experience_explanation': obj.explanation, 'experience_address':  address})

def about_us(request, arg1=None):
    return render(request, 'index.html', {'page_file': "aboutUs.html", 'page_title': 'جاده‌خاکی | درباره ما',
                                        'page_description': "جاده‌خاکی سامانه ارائه تجربه‌های متفاوت و لذت‌بخش در مناطق مختلف کشوره که سعی داره به عنوان یک پل ارتباطی بین گردشگرها و جامعه محلی، سفرهای شخصی‌سازی شده‌ و جذاب‌تری رو برای گردشگرها رقم بزنه و اون‌ها رو با جامعه محلی همراه‌تر کنه. گردشگری خلاق و تجربه‌گرا موضوعیه که در دنیا جا افتاده و بسیار طرفدار داره، و جاده‌خاکی به عنوان پیشرو در ارائه این تجربه‌ها در ایران فعالیت میکنه."})


def contact_us(request, arg1=None):
    return render(request, 'index.html', {'page_file': "contactUs.html", 'page_title': 'جاده‌خاکی | تماس با ما',
                                        'page_description': "خیابان ولیعصر، ابتدای خیابان شهید مطهری، کوچه منصور، پلاک ۸۳، واحد ۴، استارتاپ جاده‌خاکی"})


def rules(request, arg1=None):
    return render(request, 'index.html', {'page_file': "rules.html", 'page_title': 'جاده‌خاکی | قوانین و مقررات',
                                        'page_description': "خیابان ولیعصر، بعد از چهارراه زرتشت، نرسیده به سه راه فاطمی، کوچه میرهادی، پلاک ۳ (سازمان تجاری‌سازی فناوری و اشتغال دانش‌آموختگان – جهاد دانشگاهی تهران)، واحد۲"})

def empty(request, arg1=None):
    return render(request, 'index.html', {'page_file': "empty.html", 'page_title': 'جاده‌ خاکی | تجربه‌ی بومی | اقامت بومی',
                                        'page_description': "پل ارتباطی میان گردشگران و جامعه‌ی محلی برای اشتراک تجربه‌های هیجان‌انگیز"})