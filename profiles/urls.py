from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from rest_framework.urlpatterns import format_suffix_patterns

from . import views as profile_views

urlpatterns = static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + [
    url(r'^api/home_reservations[/]?$',
        profile_views.HomeResListView.as_view(),
        name='home_reservations'),
    url(r'^api/create_home_reservation[/]?$',
        profile_views.HomeResCreateView.as_view(),
        name='create-home_reservation'),
    url(r'^api/experience_reservations[/]?$',
        profile_views.ExpResListView.as_view(),
        name='experience_reservations'),
    url(r'^api/create_experience_reservation[/]?$',
        profile_views.ExpResCreateView.as_view(),
        name='create-experience_reservation'),
    url(r'^api/create_profile[/]?$',
        profile_views.ProfileCreateView.as_view(),
        name='profile-create'),
    url(r'^api/profile[/]?$',
        profile_views.ProfileRetrieveUpdateView.as_view(),
        name='profile'),
    url(r'^api/cancel[/]?$',
        profile_views.cancel,
        name='cancel'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
