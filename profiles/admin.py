from django.contrib import admin
from .models import (Profile, HomeReservation, ExperienceReservation, Comment)
# Register your models here.
@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    pass


@admin.register(HomeReservation)
class HomeReservationAdmin(admin.ModelAdmin):
    pass


@admin.register(ExperienceReservation)
class ExperienceReservationAdmin(admin.ModelAdmin):
    pass
