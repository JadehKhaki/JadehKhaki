import traceback

from rest_framework import serializers

from .models import ExperienceReservation, HomeReservation, Profile
from homes.models import Room
from core.serializers import ImageSerializer, PhoneNoSerializer
from core.models import PhoneNo, Image
from experiences.models import Experience


class HomeResCreateSerializer(serializers.ModelSerializer):

    room_uuid = serializers.UUIDField(source='room.uuid')
    # def validate_room(self, value):
    #     print("value:: " + str(value))
    #     return Room.objects.get(uuid=value).id

    def create(self, validated_data):
        # raise_errors_on_nested_writes('create', self, validated_data)
        room_uuid = 1
        for attr, value in self.data.items():
            if attr == 'room_uuid':
                room_uuid = value
        r = validated_data.pop('room')
        room_id = Room.objects.get(uuid=room_uuid)
        ModelClass = self.Meta.model

        try:
            instance = ModelClass.objects.create(**validated_data, room=room_id)
        except TypeError:
            tb = traceback.format_exc()
            msg = (
                'Got a `TypeError` when calling `%s.objects.create()`. '
                'This may be because you have a writable field on the '
                'serializer class that is not a valid argument to '
                '`%s.objects.create()`. You may need to make the field '
                'read-only, or override the %s.create() method to handle '
                'this correctly.\nOriginal exception was:\n %s' %
                (
                    ModelClass.__name__,
                    ModelClass.__name__,
                    self.__class__.__name__,
                    tb
                )
            )
            raise TypeError(msg)

        instance.save()
        return instance

    class Meta:
        model = HomeReservation
        fields = ('check_in', 'check_out', 'room_uuid', 'count')


class HomeResListSerializer(serializers.ModelSerializer):
    home = serializers.SerializerMethodField()
    room = serializers.SerializerMethodField()
    url_title = serializers.SerializerMethodField()

    def get_room(self, reservation):
        return reservation.room.name

    def get_home(self, reservation):
        return reservation.room.home.all()[0].title

    def get_url_title(self, reservation):
        return reservation.room.home.all()[0].url_title

    class Meta:
        model = HomeReservation
        fields = ('uuid', 'created_at', 'home', 'room', 'status', 'check_in', 'check_out',
                  'price', 'count', 'url_title')


class ExpResCreateSerializer(serializers.ModelSerializer):
    experience_uuid = serializers.UUIDField(source='experience.uuid')

    def create(self, validated_data):
        # raise_errors_on_nested_writes('create', self, validated_data)
        experience_uuid = 1
        for attr, value in self.data.items():
            if attr == 'experience_uuid':
                experience_uuid = value
        e = validated_data.pop('experience')
        experience_id = Experience.objects.get(uuid=experience_uuid)
        ModelClass = self.Meta.model

        try:
            instance = ModelClass.objects.create(**validated_data, experience=experience_id)
        except TypeError:
            tb = traceback.format_exc()
            msg = (
                'Got a `TypeError` when calling `%s.objects.create()`. '
                'This may be because you have a writable field on the '
                'serializer class that is not a valid argument to '
                '`%s.objects.create()`. You may need to make the field '
                'read-only, or override the %s.create() method to handle '
                'this correctly.\nOriginal exception was:\n %s' %
                (
                    ModelClass.__name__,
                    ModelClass.__name__,
                    self.__class__.__name__,
                    tb
                )
            )
            raise TypeError(msg)

        instance.save()
        return instance

    class Meta:
        model = ExperienceReservation
        fields = ('check_in', 'count', 'experience_uuid')


class ExpResListSerializer(serializers.ModelSerializer):
    experience = serializers.SerializerMethodField()
    url_title = serializers.SerializerMethodField()

    def get_url_title(self, reservation):
        return reservation.experience.url_title

    def get_experience(self, reservation):
        return reservation.experience.title

    class Meta:
        model = ExperienceReservation
        fields = ('uuid', 'created_at', 'experience', 'status', 'check_in', 'price', 'count', 'url_title')


class ProfileSerializer(serializers.ModelSerializer):
    phone_no = PhoneNoSerializer()
    # photo = ImageSerializer(required=False)
    # card_photo = ImageSerializer(required=False)
    username = serializers.CharField(source='user.username')
    first_name = serializers.CharField(source='user.first_name')
    last_name = serializers.CharField(source='user.last_name')
    email = serializers.EmailField(source='user.email')

    def create(self, validated_data):
        # raise_errors_on_nested_writes('create', self, validated_data)
        phone_no_data = validated_data.pop('phone_no')
        # photo_data = validated_data.pop('photo')
        # card_photo_data = validated_data.pop('card_photo')

        ModelClass = self.Meta.model
        # Remove many-to-many relationships from validated_data.
        # They are not valid arguments to the default `.create()` method,
        # as they require that the instance has already been saved.
        try:
            instance = ModelClass.objects.create(**validated_data)
        except TypeError:
            tb = traceback.format_exc()
            msg = (
                'Got a `TypeError` when calling `%s.objects.create()`. '
                'This may be because you have a writable field on the '
                'serializer class that is not a valid argument to '
                '`%s.objects.create()`. You may need to make the field '
                'read-only, or override the %s.create() method to handle '
                'this correctly.\nOriginal exception was:\n %s' %
                (
                    ModelClass.__name__,
                    ModelClass.__name__,
                    self.__class__.__name__,
                    tb
                )
            )
            raise TypeError(msg)
        user = instance.user
        for attr, value in self.data.items():
            if attr in ('first_name', 'last_name', 'username', 'email'):
                setattr(user, attr, value)
        user.save()

        phone_no = PhoneNo.objects.create(**phone_no_data)
        instance.phone_no = phone_no

        # photo = Image.objects.create(**photo_data)
        # instance.photo = photo
        #
        # card_photo = Image.objects.create(**card_photo_data)
        # instance.card_photo = card_photo

        instance.save()
        return instance

    def update(self, instance, validated_data):
        print("hereeee!!!!")
        user = instance.user
        phone_no_data = validated_data.pop('phone_no', None)
        # photo_data = validated_data.pop('photo', None)
        # card_photo_data = validated_data.pop('card_photo', None)

        for attr, value in validated_data.items():
            # print("valueeeeee:: " + str(value))
            if attr == 'user':
                for attr2, value2 in value.items():
                    setattr(user, attr2, value2)
            else:
                setattr(instance, attr, value)

        if phone_no_data:
            phone_no = PhoneNo.objects.create(**phone_no_data)
            instance.phone_no = phone_no

        # if photo_data:
        #     photo = Image.objects.create(**photo_data)
        #     instance.photo = photo
        #
        # if card_photo_data:
        #     card_photo = Image.objects.create(**card_photo_data)
        #     instance.card_photo = card_photo

        instance.save()
        user.save()
        print("thereeee!!!")
        return instance

    class Meta:
        model = Profile
        fields = ('username', 'first_name', 'last_name', 'email',
                  'phone_no', 'birth_date', 'national_id')
        # , 'photo', 'card_photo')
