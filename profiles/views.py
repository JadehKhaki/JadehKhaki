from django.http import JsonResponse
from django.core.mail import send_mail
from django.shortcuts import render
from django.http import Http404
from .permissions import IsOwner
from rest_framework import generics, permissions
from .models import HomeReservation, ExperienceReservation, Profile
from .serializers import (HomeResCreateSerializer,
                          HomeResListSerializer, ExpResCreateSerializer,
                          ExpResListSerializer, ProfileSerializer)


class HomeResListView(generics.ListAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = HomeResListSerializer
    # lookup_field = 'uuid'

    def get_queryset(self):
        customer = self.request.user
        return customer.home_reservations


class HomeResCreateView(generics.CreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = HomeResCreateSerializer
    # lookup_field = 'uuid'

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user, status='PR')
        subject = 'Reservation Request'
        message = 'New Home Reservation Request!'
        send_mail(subject, message, 'info@jadehkhaki.com', ['info@jadehkhaki.com'])


class ExpResListView(generics.ListAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = ExpResListSerializer
    # lookup_field = 'uuid'

    def get_queryset(self):
        customer = self.request.user
        return customer.exp_reservations


def cancel(request):
    request_type = request.GET.get('type')
    uuid = request.GET.get('uuid')
    if request_type == 'home':
        reservation = HomeReservation.objects.get(uuid=uuid)
    elif request_type == 'experience':
        reservation = ExperienceReservation.objects.get(uuid=uuid)
    status = reservation.cancel()
    if status:
        return JsonResponse({'status': 'Cancelled'}, status=200)
    else:
        return JsonResponse({'status': 'Cancellation failed'}, status=403)


class ExpResCreateView(generics.CreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = ExpResCreateSerializer
    # lookup_field = 'uuid'

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user, status='PR')
        subject = 'Reservation Request'
        message = 'New Experience Reservation Request!'
        send_mail(subject, message, 'info@jadehkhaki.com', ['info@jadehkhaki.com'])


class ProfileCreateView(generics.CreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = ProfileSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ProfileRetrieveUpdateView(generics.RetrieveUpdateAPIView):
    permission_classes = (permissions.IsAuthenticated, )
    serializer_class = ProfileSerializer

    def get_object(self):
        try:
            return self.request.user.profile
        except Profile.DoesNotExist:
            raise Http404

