import uuid as uuid_lib

from django.db import models
from django.contrib.auth.models import User


class Profile(models.Model):
    uuid = models.UUIDField(db_index=True, default=uuid_lib.uuid4, unique=True, editable=False)
    user = models.OneToOneField(User, related_name='profile', on_delete=models.CASCADE)
    phone_no = models.ForeignKey('core.PhoneNo', blank=True, null=True, on_delete=models.CASCADE)
    birth_date = models.DateField(blank=True, null=True)
    national_id = models.CharField(max_length=11, blank=True, null=True)
    photo = models.ForeignKey('core.Image', blank=True, null=True, related_name='profile_of_photo', on_delete=models.CASCADE)
    card_photo = models.ForeignKey('core.Image', blank=True, null=True, related_name='profile_of_card_photo', on_delete=models.CASCADE)

    def __str__(self):
        return self.user.username + "'s profile"


class Reservation(models.Model):
    PROCESSING = 'PR'
    POSSIBLE = 'PO'
    IMPOSSIBLE = 'IM'
    PAID = 'PA'
    LATE = 'LA'
    CANCELLED = 'CA'
    STATUS_CHOICES = (
        (PROCESSING, 'Processing'),
        (POSSIBLE, 'Possible'),
        (IMPOSSIBLE, 'Impossible'),
        (PAID, 'Paid'),
        (LATE, 'Late'),
        (CANCELLED, 'Cancelled'),
    )
    uuid = models.UUIDField(db_index=True, default=uuid_lib.uuid4, unique=True, editable=False)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    status = models.CharField(max_length=2, choices=STATUS_CHOICES, default=PROCESSING)
    check_in = models.DateField(null=True)
    check_out = models.DateField(null=True)
    count = models.IntegerField(null=True)
    price = models.IntegerField(null=True, blank=True)
    
    def pay(self, amount):
        if amount == self.price:
            self.status = self.PAID
            print(self.status)
            self.save()
            return True
        else:
            return False

    def cancel(self):
        if self.status in (self.PROCESSING, self.POSSIBLE):
            self.status = self.CANCELLED
            self.save()
            return True
        else:
            return False

    class Meta:
        abstract = True


class HomeReservation(Reservation):
    room = models.ForeignKey('homes.Room', null=True, on_delete=models.CASCADE)
    owner = models.ForeignKey(User, related_name='home_reservations', on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        self.price = self.room.get_cost(self.count)
        super().save(*args, **kwargs)

    def __str__(self):
        return str(self.owner) + "'s reservation of " + str(self.room)


class ExperienceReservation(Reservation):
    experience = models.ForeignKey('experiences.Experience', on_delete=models.CASCADE)
    owner = models.ForeignKey(User, related_name='exp_reservations', on_delete=models.CASCADE)
    check_out = models.DateField(null=True, blank=True)

    def save(self, *args, **kwargs):
        self.price = self.experience.get_cost(self.count)
        super().save(*args, **kwargs)
    
    def __str__(self):
        return str(self.owner) + "'s reservation of " + str(self.experience)


class Comment(models.Model):
    uuid = models.UUIDField(db_index=True, default=uuid_lib.uuid4, unique=True, editable=False)
    owner = models.ForeignKey(User, related_name='comments', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
