from django.contrib import admin
from .models import (Constraints, ExpCategory, Experience,
                     ExperienceService, CostCount)


@admin.register(Constraints)
class ConstraintAdmin(admin.ModelAdmin):
    list_display = ('id',)


@admin.register(ExpCategory)
class ExpCategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'title',)


@admin.register(CostCount)
class CostCountAdmin(admin.ModelAdmin):
    pass


class CostCountInline(admin.TabularInline):
    model = CostCount


@admin.register(ExperienceService)
class ExperienceServiceAdmin(admin.ModelAdmin):
    pass


@admin.register(Experience)
class ExperienceAdmin(admin.ModelAdmin):
    list_display = ('id', 'title',)
    filter_horizontal = ('hosts', 'homes', 'category', 'gallery', 'services')



