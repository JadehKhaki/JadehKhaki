from rest_framework import generics, permissions

from core.libs.language import to_persian
from .serializers import ExpMinSerializer, ExpSerializer
from .models import Experience


class ExpList(generics.ListAPIView):
    permission_classes = (permissions.AllowAny,)
    queryset = Experience.objects.all()
    serializer_class = ExpMinSerializer
    lookup_field = 'url_title'


class ExpDetail(generics.RetrieveAPIView):
    permission_classes = (permissions.AllowAny,)
    queryset = Experience.objects.all()
    serializer_class = ExpSerializer
    lookup_field = 'url_title'


class BestExperiences(generics.ListAPIView):
    permission_classes = (permissions.AllowAny,)

    def get_queryset(self):
        queryset = Experience.objects.order_by('-rating')
        return queryset[0:4]
    serializer_class = ExpMinSerializer


class ExpSearch(generics.ListAPIView):
    permission_classes = (permissions.AllowAny,)

    def get_queryset(self):
        location = self.request.GET.get('location', '')
        title = self.request.GET.get('title', '')
        location = to_persian(location)
        title = to_persian(title)
        if location == 'all':
            location = ''
        if title == 'all':
            title = ''
        return Experience.objects.search(location, title)
    serializer_class = ExpMinSerializer


