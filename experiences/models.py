import uuid as uuid_lib

from django.db import models
from django.db.models import Q

from core.models import Person, Address, Image


class CostCount(models.Model):
    uuid = models.UUIDField(db_index=True, default=uuid_lib.uuid4, unique=True, editable=False)
    cost = models.IntegerField()
    min_count = models.IntegerField()  # Should we delete it and use the last max_count instead?
    max_count = models.IntegerField()


class ExperienceService(models.Model):
    uuid = models.UUIDField(db_index=True, default=uuid_lib.uuid4, unique=True, editable=False)
    name = models.CharField(max_length=80)
    explanation = models.CharField(max_length=2048)
    prices = models.ManyToManyField(CostCount, related_name='service')

    def get_min_cost(self):
        costs = [price.cost for price in self.prices.all()]
        return min(costs)

    def get_max_cost(self):
        costs = [price.cost for price in self.prices.all()]
        return max(costs)


class ExpCategory(models.Model):
    uuid = models.UUIDField(db_index=True, default=uuid_lib.uuid4, unique=True, editable=False)
    title = models.CharField(unique=True, max_length=50)
    explanation = models.CharField(max_length=2048)
    # Images:
    image = models.ForeignKey(Image, related_name='exp_expcategory', null=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.title


class Constraints(models.Model):
    FEMALE = 'FE'
    MALE = 'MA'
    BOTH = 'FM'
    GENDER_CHOICES = (
        (FEMALE, 'Female'),
        (MALE, 'Male'),
        (BOTH, 'Both'),
    )
    uuid = models.UUIDField(db_index=True, default=uuid_lib.uuid4, unique=True, editable=False)
    gender = models.CharField(max_length=2, choices=GENDER_CHOICES, default=BOTH)
    min_age = models.IntegerField(default=0, blank=True, null=True)
    max_age = models.IntegerField(default=100, blank=True, null=True)
    other_constraints = models.CharField(max_length=256, blank=True)
    min_group_size = models.IntegerField(default=1, blank=True, null=True)
    max_group_size = models.IntegerField(default=1, blank=True, null=True)

    def __str__(self):
        return '[Constraints] '  # add Experience title


class SearchManager(models.Manager):
    def search(self, location, title):
        return super().get_queryset().filter(
            (
                Q(address__city_town__city__contains=location) |
                Q(address__city_town__town__contains=location) |
                Q(address__city_town__province__name__contains=location)
            ) &
            (
                Q(title__contains=title) |
                Q(category__title__contains=title)
            )
        )


class Experience(models.Model):
    uuid = models.UUIDField(db_index=True, default=uuid_lib.uuid4, unique=True, editable=False)
    title = models.CharField(max_length=64)
    url_title = models.CharField(max_length=100, unique=True)
    explanation = models.TextField(max_length=2048, blank=True)
    description_tag = models.CharField(max_length=400, blank=True)
    rating = models.IntegerField(blank=True, null=True, default=0)  # change into sp or view in the future?
    cancellation = models.CharField(max_length=256, blank=True)
    note = models.CharField(max_length=256, blank=True)  # ending notes
    duration = models.CharField(max_length=15)
    host_equipments = models.CharField(max_length=512, blank=True)
    guest_equipments = models.CharField(max_length=512, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)

    # Relations:
    base_prices = models.ManyToManyField(CostCount, related_name='experience')
    services = models.ManyToManyField(ExperienceService, blank=True, related_name='experience')
    hosts = models.ManyToManyField(Person, related_name='experiences')
    address = models.ForeignKey(Address, related_name='exps_address', on_delete=models.CASCADE)
    constraints = models.ForeignKey(Constraints, on_delete=models.CASCADE)
    homes = models.ManyToManyField('homes.Home', related_name='exphome', blank=True)
    category = models.ManyToManyField(ExpCategory)

    # Images:
    main_image = models.ForeignKey(Image, null=True, related_name='exp_im', on_delete=models.CASCADE)
    gallery = models.ManyToManyField(Image, related_name='exp_expGallery')

    # Managers
    objects = SearchManager()

    def get_min_cost(self):
        bp_costs = [bp.cost for bp in self.base_prices.all()]
        return min(bp_costs)

    def get_max_cost(self):
        bp_costs = [bp.cost for bp in self.base_prices.all()]
        exp_costs = [service.get_max_cost() for service in self.services.all()]
        return max(bp_costs) + sum(exp_costs)

    def get_cost(self, count):
        base_prices = [bp for bp in self.base_prices.all()]
        cost = 0
        for bp in base_prices:
            if bp.min_count <= count <= bp.max_count:
                cost = bp.cost
        return cost*count

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['created_at']


