from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from rest_framework.urlpatterns import format_suffix_patterns

from . import views as core_views

urlpatterns = static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + [
    url(r'^api/search_experiences[/]?$',
        core_views.ExpSearch.as_view(),
        name='experience-search'),
    url(r'^api/best_experiences[/]?$',
        core_views.BestExperiences.as_view(),
        name='best-experiences'),
    url(r'^api/experiences[/]?$',
        core_views.ExpList.as_view(),
        name='experience-list'),
    url(r'^api/experiences/(?P<url_title>[\w-]+)[/]?$',
        core_views.ExpDetail.as_view(),
        name='experience-detail'),
]
