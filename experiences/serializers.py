from rest_framework import serializers

from core.serializers import ImageSerializer, PersonSerializer, AddressSerializer
from .models import (Constraints, ExpCategory,
                     Experience, ExperienceService, CostCount)


class CostCountSerializer(serializers.ModelSerializer):
    class Meta:
        model = CostCount
        exclude = ('id',)


class ExperienceServiceSerializer(serializers.ModelSerializer):
    prices = CostCountSerializer(many=True)

    class Meta:
        model = ExperienceService
        exclude = ('id',)


class ConstraintsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Constraints
        exclude = ('id',)


class ExperienceCategorySerializer(serializers.ModelSerializer):
    image = ImageSerializer()

    class Meta:
        model = ExpCategory
        fields = ('title', 'explanation', 'image')


class ExpSerializer(serializers.ModelSerializer):
    main_image = ImageSerializer()
    gallery = ImageSerializer(many=True)
    category = ExperienceCategorySerializer(many=True)
    hosts = PersonSerializer(many=True)
    address = AddressSerializer()
    constraints = ConstraintsSerializer()
    services = ExperienceServiceSerializer(many=True)
    base_prices = CostCountSerializer(many=True)

    class Meta:
        model = Experience
        exclude = ('id',)


class ExpMinSerializer(serializers.HyperlinkedModelSerializer):
    main_image = ImageSerializer()
    address = AddressSerializer()
    type = serializers.SerializerMethodField()
    cost = serializers.SerializerMethodField()
    min_cost = serializers.SerializerMethodField()
    max_cost = serializers.SerializerMethodField()
    url = serializers.HyperlinkedIdentityField(view_name='experience-detail', lookup_field='url_title')

    def get_cost(self, experience):
        return experience.get_min_cost()

    def get_min_cost(self, experience):  # to be added in front
        return experience.get_min_cost()

    def get_max_cost(self, experience):  # to be added in front
        return experience.get_max_cost()

    def get_type(self, experience):
        return 'experience'

    class Meta:
        model = Experience
        fields = ('url', 'uuid', 'url_title', 'title', 'explanation', 'main_image', 'rating', 'address',
                  'created_at', 'type', 'cost', 'min_cost', 'max_cost')
