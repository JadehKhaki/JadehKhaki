#!/bin/bash
pg_dump   -h localhost   -p 5432   -U jadeuser -W   --table="core_cost"   --data-only   --column-insert   jadetest > home/cost.sql
pg_dump   -h localhost   -p 5432   -U jadeuser -W   --table="core_homecategory"   --data-only   --column-insert   jadetest > home/homecategory.sql
pg_dump   -h localhost   -p 5432   -U jadeuser -W   --table="core_menu"   --data-only   --column-insert   jadetest > home/menu.sql
pg_dump   -h localhost   -p 5432   -U jadeuser -W   --table="core_room"   --data-only   --column-insert   jadetest > home/room.sql
pg_dump   -h localhost   -p 5432   -U jadeuser -W   --table="core_serviceplace"   --data-only   --column-insert   jadetest > home/serviceplace.sql
pg_dump   -h localhost   -p 5432   -U jadeuser -W   --table="core_supplies"   --data-only   --column-insert   jadetest > home/supplies.sql
pg_dump   -h localhost   -p 5432   -U jadeuser -W   --table="core_home"   --data-only   --column-insert   jadetest > home/home.sql
pg_dump   -h localhost   -p 5432   -U jadeuser -W   --table="core_home_category"   --data-only   --column-insert   jadetest > home/home_category.sql
pg_dump   -h localhost   -p 5432   -U jadeuser -W   --table="core_home_gallery"   --data-only   --column-insert   jadetest > home/home_gallery.sql
pg_dump   -h localhost   -p 5432   -U jadeuser -W   --table="core_home_rooms"   --data-only   --column-insert   jadetest > home/home_rooms.sql
pg_dump   -h localhost   -p 5432   -U jadeuser -W   --table="core_home_service_places"   --data-only   --column-insert   jadetest > home/home_service_places.sql

pg_dump   -h localhost   -p 5432   -U jadeuser -W   --table="core_constraints"   --data-only   --column-insert   jadetest > experience/constraints.sql
pg_dump   -h localhost   -p 5432   -U jadeuser -W   --table="core_expcategory"   --data-only   --column-insert   jadetest > experience/expcategory.sql
pg_dump   -h localhost   -p 5432   -U jadeuser -W   --table="core_expcost"   --data-only   --column-insert   jadetest > experience/expcost.sql
pg_dump   -h localhost   -p 5432   -U jadeuser -W   --table="core_experience"   --data-only   --column-insert   jadetest > experience/experience.sql
pg_dump   -h localhost   -p 5432   -U jadeuser -W   --table="core_experience_additional_price"   --data-only   --column-insert   jadetest > experience/experience_additional_price.sql
pg_dump   -h localhost   -p 5432   -U jadeuser -W   --table="core_experience_category"   --data-only   --column-insert   jadetest > experience/experience_category.sql
pg_dump   -h localhost   -p 5432   -U jadeuser -W   --table="core_experience_gallery"   --data-only   --column-insert   jadetest > experience/experience_gallery.sql
pg_dump   -h localhost   -p 5432   -U jadeuser -W   --table="core_experience_homes"   --data-only   --column-insert   jadetest > experience/experience_homes.sql
pg_dump   -h localhost   -p 5432   -U jadeuser -W   --table="core_experience_hosts"   --data-only   --column-insert   jadetest > experience/experience_host.sql

find home/ -name \*.sql -exec sed -i "s/core_/homes_/g" {} \;
find experience/ -name \*.sql -exec sed -i "s/core_/experiences_/g" {} \;
jade1995