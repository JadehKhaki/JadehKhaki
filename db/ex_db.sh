#!/bin/bash

psql   -h localhost   -p 5432   -U jadeuser -W   jadetest -f home/cost.sql
psql   -h localhost   -p 5432   -U jadeuser -W   jadetest -f home/homecategory.sql
psql   -h localhost   -p 5432   -U jadeuser -W   jadetest -f home/menu.sql
psql   -h localhost   -p 5432   -U jadeuser -W   jadetest -f home/room.sql
psql   -h localhost   -p 5432   -U jadeuser -W   jadetest -f home/serviceplace.sql
psql   -h localhost   -p 5432   -U jadeuser -W   jadetest -f home/supplies.sql
psql   -h localhost   -p 5432   -U jadeuser -W   jadetest -f home/home.sql
psql   -h localhost   -p 5432   -U jadeuser -W   jadetest -f home/home_category.sql
psql   -h localhost   -p 5432   -U jadeuser -W   jadetest -f home/home_gallery.sql
psql   -h localhost   -p 5432   -U jadeuser -W   jadetest -f home/home_rooms.sql
psql   -h localhost   -p 5432   -U jadeuser -W   jadetest -f home/home_service_places.sql

psql   -h localhost   -p 5432   -U jadeuser -W   jadetest -f experience/constraints.sql
psql   -h localhost   -p 5432   -U jadeuser -W   jadetest -f experience/expcategory.sql
psql   -h localhost   -p 5432   -U jadeuser -W   jadetest -f experience/expcost.sql
psql   -h localhost   -p 5432   -U jadeuser -W   jadetest -f experience/experience.sql
psql   -h localhost   -p 5432   -U jadeuser -W   jadetest -f experience/experience_additional_price.sql
psql   -h localhost   -p 5432   -U jadeuser -W   jadetest -f experience/experience_category.sql
psql   -h localhost   -p 5432   -U jadeuser -W   jadetest -f experience/experience_gallery.sql
psql   -h localhost   -p 5432   -U jadeuser -W   jadetest -f experience/experience_homes.sql
psql   -h localhost   -p 5432   -U jadeuser -W   jadetest -f experience/experience_host.sql
