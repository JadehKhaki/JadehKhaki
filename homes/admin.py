from django.contrib import admin

from .models import (HomeCategory, Supplies, Menu, Cost,
                     Room, ServicePlace, Home)


@admin.register(Home)
class HomeAdmin(admin.ModelAdmin):
    list_display = ('id', 'title',)
    # relatiosn = ('person', 'cost', 'supplies', 'address', 'phoneNo', 'servicePlaces',)
    filter_horizontal = ('category', 'service_places', 'gallery', 'rooms')


@admin.register(Cost)
class CostAdmin(admin.ModelAdmin):
    list_display = ('id', 'ordinary_price',)


@admin.register(Supplies)
class SuppliesAdmin(admin.ModelAdmin):
    list_display = ('id',)


@admin.register(ServicePlace)
class ServicePlaceAdmin(admin.ModelAdmin):
    list_display = ('id', 'type', 'title')


@admin.register(Room)
class RoomAdmin(admin.ModelAdmin):
    list_display = ('id',)


@admin.register(Menu)
class MenuAdmin(admin.ModelAdmin):
    list_display = ('id', 'title',)


@admin.register(HomeCategory)
class HomeCategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'title',)


