import uuid as uuid_lib

from django.db import models
from django.db.models import Q

from core.models import Person, Address, Image


class HomeCategory(models.Model):
    ECOTOURISM = 'خانه‌ی بومگردی'
    LOCAL = 'خانه‌ی محلی'
    NOMADIC = 'چادر عشایری'
    HOME_CHOICES = (
        (ECOTOURISM, 'Ecotourism'),
        (LOCAL, 'Local'),
        (NOMADIC, 'Nomadic'),
    )
    uuid = models.UUIDField(db_index=True, default=uuid_lib.uuid4, unique=True, editable=False)
    explanation = models.CharField(max_length=2048)
    # Images:
    image = models.ForeignKey(Image, related_name='home_category', null=True, on_delete=models.CASCADE)
    title = models.CharField(unique=True, max_length=20, choices=HOME_CHOICES, default=LOCAL)

    def __str__(self):
        return self.title


class Supplies(models.Model):
    uuid = models.UUIDField(db_index=True, default=uuid_lib.uuid4, unique=True, editable=False)
    cooler = models.BooleanField()
    iron = models.BooleanField()
    washing_machine = models.BooleanField()
    # gas = models.BooleanField()
    heater = models.BooleanField()
    # hanger = models.BooleanField()
    # techPersonnel = models.BooleanField()  # what was it :-?
    water = models.BooleanField()
    shampoo = models.BooleanField()
    breakfast = models.BooleanField()  # should we make another class for it as it may cost more money?
    # jarakhti = models.BooleanField()
    public_bath = models.BooleanField()  # :-?
    tv = models.BooleanField()
    parking = models.BooleanField()
    credit_card = models.BooleanField()  # :-?
    # handWash = models.BooleanField()
    internet = models.BooleanField()
    public_wc = models.BooleanField()  # :-?
    # dressWash = models.BooleanField()
    # englishPersonnel = models.BooleanField()  # :-?
    kitchen = models.BooleanField()
    # airConditioner = models.BooleanField()
    # hairDryer = models.BooleanField()
    electricity = models.BooleanField()
    bed = models.BooleanField(default=False)
    addition = models.CharField(blank=True, max_length=128)

    def __str__(self):
        return '[Supplies] '  # add home title (how for relation?)


class Menu(models.Model):
    uuid = models.UUIDField(db_index=True, default=uuid_lib.uuid4, unique=True, editable=False)
    title = models.CharField(max_length=256, blank=True)
    breakfast = models.CharField(max_length=1024, blank=True)
    lunch = models.CharField(max_length=1024, blank=True)
    dinner = models.CharField(max_length=1024, blank=True)

    def __str__(self):
        return self.title


class Cost(models.Model):
    uuid = models.UUIDField(db_index=True, default=uuid_lib.uuid4, unique=True, editable=False)
    ordinary_price = models.DecimalField(max_digits=8, decimal_places=0)
    peak_price = models.DecimalField(max_digits=8, decimal_places=0)
    norouz_price = models.DecimalField(max_digits=8, decimal_places=0)
    peak_time = models.CharField(max_length=128)  # should it be separated with date type for each?

    def get_cost(self):
        return self.ordinary_price

    def __str__(self):
        return str(self.ordinary_price)


class ServicePlace(models.Model):
    uuid = models.UUIDField(db_index=True, default=uuid_lib.uuid4, unique=True, editable=False)
    type = models.CharField(max_length=16)
    title = models.CharField(max_length=16)
    # Relations
    # address = models.OneToOneField(Address)  # This is correct
    # phone number
    address = models.ForeignKey(Address, related_name='service', on_delete=models.CASCADE)  # Test edition

    def __str__(self):
        return self.type + ' ' + self.title


class Room(models.Model):
    uuid = models.UUIDField(db_index=True, default=uuid_lib.uuid4, unique=True, editable=False)
    name = models.CharField(max_length=30, blank=True)
    min_capacity = models.IntegerField(null=True, blank=True)
    max_capacity = models.IntegerField(default=100)
    room_cost = models.OneToOneField(Cost, null=True, related_name='room_of_cost', on_delete=models.CASCADE)
    person_cost = models.OneToOneField(Cost, null=True, related_name='room_of_person_cost', on_delete=models.CASCADE)

    def get_cost(self, count):
        return self.room_cost.get_cost() + max((count - self.min_capacity), 0) * self.person_cost.get_cost()

    def get_min_cost(self):
        return self.get_cost(1)


class SearchManager(models.Manager):
    def search(self, location, title):
        return super().get_queryset().filter(
            (
                Q(address__city_town__city__contains=location) |
                Q(address__city_town__town__contains=location) |
                Q(address__city_town__province__name__contains=location)
            ) &
            (
                Q(title__contains=title) |
                Q(category__title__contains=title)
            )
        )


class Home(models.Model):
    uuid = models.UUIDField(db_index=True, default=uuid_lib.uuid4, unique=True, editable=False)
    title = models.CharField(max_length=64)
    explanation = models.TextField(max_length=2048)
    tiny_explanation = models.CharField(max_length=40, blank=True)
    description_tag = models.CharField(max_length=400, blank=True)
    rating = models.IntegerField(blank=True, null=True, default=0)
    additional_notes = models.CharField(max_length=1024, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    cancellation = models.CharField(max_length=256, blank=True)
    url_title = models.CharField(max_length=100, unique=True)
    # Relations:
    person = models.ForeignKey(Person, related_name='homehome', on_delete=models.CASCADE)
    food_menu = models.ForeignKey(Menu, related_name='home', null=True, blank=True, on_delete=models.CASCADE)
    # These are correct:
    # cost = models.OneToOneField(Cost)
    # supplies = models.OneToOneField(Supplies)  # befrest
    # address = models.OneToOneField(Address)
    # <Test>
    supplies = models.ForeignKey(Supplies, on_delete=models.CASCADE)  # befrest
    address = models.ForeignKey(Address, related_name='home_home', on_delete=models.CASCADE)
    # </Test>
    rooms = models.ManyToManyField('Room', related_name='home', blank=True)
    service_places = models.ManyToManyField('ServicePlace', related_name='home', blank=True)
    category = models.ManyToManyField(HomeCategory)
    # Images:
    main_image = models.ForeignKey(Image, null=True, related_name='home_home', on_delete=models.CASCADE)
    gallery = models.ManyToManyField(Image, related_name='home_homeGallery')
    # Add title deed image

    # Managers
    objects = SearchManager()

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['created_at']


