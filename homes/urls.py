from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from rest_framework.urlpatterns import format_suffix_patterns

from . import views as homes_views

urlpatterns = [
    url(r'^api/search_homes[/]?$',
        homes_views.HomeSearch.as_view(),
        name='home-search'),
    url(r'^api/homes[/]?$',
        homes_views.HomeList.as_view(),
        name='home-list'),
    url(r'^api/homes/(?P<url_title>[\w-]+)[/]?$',
        homes_views.HomeDetail.as_view(),
        name='home-detail'),
    url(r'^api/best_homes[/]?$',
        homes_views.BestHomes.as_view(),
        name='best-homes'),
    ]

