from rest_framework import serializers

from core.serializers import ImageSerializer, AddressSerializer, PersonSerializer

from .models import HomeCategory, Supplies, Cost, Room, Menu, Home


class HomeCategorySerializer(serializers.ModelSerializer):
    image = ImageSerializer()

    class Meta:
        model = HomeCategory
        fields = ('title', 'explanation', 'image')


class SuppliesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Supplies
        exclude = ('id',)


class CostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cost
        exclude = ('id',)


class RoomSerializer(serializers.ModelSerializer):
    room_cost = CostSerializer()
    person_cost = CostSerializer()

    class Meta:
        model = Room
        exclude = ('id',)


class FoodMenuSerializer(serializers.ModelSerializer):
    class Meta:
        model = Menu
        exclude = ('id',)


class HomeSerializer(serializers.ModelSerializer):
    main_image = ImageSerializer()
    gallery = ImageSerializer(many=True)
    address = AddressSerializer()
    person = PersonSerializer()
    supplies = SuppliesSerializer()
    rooms = RoomSerializer(many=True)
    food_menu = FoodMenuSerializer()
    category = HomeCategorySerializer(many=True)

    class Meta:
        model = Home
        fields = ('uuid', 'url_title', 'title', 'explanation', 'tiny_explanation',
                  'rating', 'food_menu', 'additional_notes', 'created_at',
                  'person', 'supplies', 'address', 'rooms', 'service_places',
                  'category', 'main_image', 'gallery', 'cancellation')


class HomeMinSerializer(serializers.HyperlinkedModelSerializer):
    main_image = ImageSerializer()
    address = AddressSerializer()
    cost = serializers.SerializerMethodField()
    type = serializers.SerializerMethodField()
    url = serializers.HyperlinkedIdentityField(view_name='home-detail', lookup_field='url_title')

    def get_cost(self, home):  # min cost
        home_rooms = home.rooms.all()
        min_cost = 'نامشخص'
        for room in home_rooms:
            cost = room.get_min_cost()
            if min_cost == 'نامشخص':
                min_cost = cost
            else:
                min_cost = min(cost, min_cost)
        return min_cost

    def get_type(self, home):
        return 'home'

    class Meta:
        model = Home
        fields = ('uuid', 'url_title', 'url', 'title', 'explanation', 'main_image',
                  'rating', 'address', 'created_at', 'type', 'cost')


