
from rest_framework import generics, permissions
from core.libs.language import to_persian
from .models import Home, HomeCategory
from .serializers import HomeMinSerializer, HomeSerializer


class HomeList(generics.ListAPIView):
    permission_classes = (permissions.AllowAny,)
    queryset = Home.objects.all()
    serializer_class = HomeMinSerializer
    lookup_field = 'url_title'


class HomeDetail(generics.RetrieveAPIView):
    permission_classes = (permissions.AllowAny,)
    queryset = Home.objects.all()
    serializer_class = HomeSerializer
    lookup_field = 'url_title'


# Should we index by rating?
class BestHomes(generics.ListAPIView):
    permission_classes = (permissions.AllowAny,)

    def get_queryset(self):
        queryset = Home.objects.order_by('-rating')
        return queryset[0:4]
    serializer_class = HomeMinSerializer


class HomeSearch(generics.ListAPIView):
    permission_classes = (permissions.AllowAny,)

    def get_queryset(self):
        location = self.request.GET.get('location', '')
        title = self.request.GET.get('title', '')
        location = to_persian(location)
        title = to_persian(title)
        if location == 'all':
            location = ''
        if title == 'all':
            title = ''
        queryset = Home.objects.search(location, title)
        return queryset
    serializer_class = HomeMinSerializer


