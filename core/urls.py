from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from rest_framework.urlpatterns import format_suffix_patterns
from django.views.generic import TemplateView

from . import views as core_views

urlpatterns = static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + [
    url(r'^api/images[/]?$',
        core_views.ImageList.as_view(),
        name='image-list'),
    url(r'^api/locationSuggestion[/]?$',
        core_views.LocationSuggestion.as_view(),
        name='location-suggestion'),
    url(r'^api/titleSuggestion[/]?$',
        core_views.CategoryTitleSuggestion.as_view(),
        name='title-suggestion'),
    url(r'^api/team[/]?$',
        core_views.MemberList.as_view(),
        name='member-list'),
    url(r'^api/send_email[/]?$',
        core_views.send_email,
        name='send-email'),
    url('^', include('homes.urls')),
    url('^', include('experiences.urls')),
    url('^', include('zarinpal.urls')),
    url(r'^robots\.txt$', TemplateView.as_view(template_name='robots.txt')),
    url(r'^sitemap\.xml$', TemplateView.as_view(template_name='sitemap.xml')),
    url(r'^', include('views.urls')),
]

urlpatterns = format_suffix_patterns(urlpatterns)
