from django.contrib import admin

from .models import (Address, CityTown, Image, Language, Member,
                     Person, PersonTag, PhoneNo, Province)


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    list_display = ('id', 'first_name', 'last_name',)
    # relations = ('address', 'phoneNO',)


@admin.register(PhoneNo)
class PhoneNoAdmin(admin.ModelAdmin):
    list_display = ('id', 'number',)


@admin.register(Province)
class ProvinceAdmin(admin.ModelAdmin):
    list_display = ('id', 'name',)


@admin.register(CityTown)
class CityAdmin(admin.ModelAdmin):
    list_display = ('id', 'city', 'town',)
    # add province (check how to add relations display)


@admin.register(Address)
class AddressAdmin(admin.ModelAdmin):
    list_display = ('id',)


@admin.register(Language)
class LanguageAdmin(admin.ModelAdmin):
    list_display = ('id', 'language',)


@admin.register(PersonTag)
class PersonTagAdmin(admin.ModelAdmin):
    list_display = ('id', 'tag',)


@admin.register(Image)
class ImageAdmin(admin.ModelAdmin):
    list_display = ('id', 'title',)


@admin.register(Member)
class MemberAdmin(admin.ModelAdmin):
    list_display = ('name',)
