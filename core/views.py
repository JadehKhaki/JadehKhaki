from django.core.mail import send_mail
from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt, ensure_csrf_cookie
from rest_framework import generics, permissions
from rest_framework.parsers import JSONParser

from .libs.classes import SearchTitle
from .libs.language import to_persian
from .models import CityTown, Image, Member
from .serializers import ImageSerializer, MemberSerializer, SearchTitleSerializer
from homes.models import Home, HomeCategory
from experiences.models import Experience, ExpCategory


def index(request, arg1=None):
    return render(request, 'index.html')


class ImageList(generics.ListAPIView):
    permission_classes = (permissions.AllowAny,)
    queryset = Image.objects.all()
    serializer_class = ImageSerializer


class LocationSuggestion(generics.ListAPIView):
    permission_classes = (permissions.AllowAny,)

    def get_queryset(self):
        location = self.request.GET.get('location', '')
        location = to_persian(location)
        queryset = CityTown.objects.location_match(location)
        res_set = set()
        for q in queryset:
            if location in q.province.name:
                res_set.add(q.province.name)
            if location in q.city:
                res_set.add(q.city)
            if location in q.town:
                res_set.add(q.town)
        result = [SearchTitle(c, 'location') for c in res_set]
        result = sorted(result, key=lambda x: x.title)
        return result
    serializer_class = SearchTitleSerializer


class CategoryTitleSuggestion(generics.ListAPIView):
    permission_classes = (permissions.AllowAny,)

    def get_queryset(self):
        title = self.request.GET.get('title', '')
        title = to_persian(title)
        experience_query = Experience.objects.filter(title__contains=title)
        home_query = Home.objects.filter(title__contains=title)
        category_h_query = HomeCategory.objects.filter(title__contains=title)
        category_e_query = ExpCategory.objects.filter(title__contains=title)
        result_set = set()
        for e in experience_query:
            result_set.add(e.title)
        for h in home_query:
            result_set.add(h.title)
        for ch in category_h_query:
            result_set.add(ch.title)
        for ce in category_e_query:
            result_set.add(ce.title)
        result = [SearchTitle(r, 'title') for r in result_set]
        return result
    serializer_class = SearchTitleSerializer


class MemberList(generics.ListAPIView):
    permission_classes = (permissions.AllowAny,)

    queryset = Member.objects.all()
    serializer_class = MemberSerializer


# Handle Exceptions
@ensure_csrf_cookie
def send_email(request):
    if request.method == 'POST':
        data = JSONParser().parse(request)
        subject = data['subject']
        from_email = data['fromEm']
        message = 'from: ' + from_email + '\n\n\n' + data['message']
        send_mail(subject, message, from_email, ['info@jadehkhaki.com'])
    return JsonResponse({'message': 'Email is sent'}, status=200)
