from __future__ import print_function

import base64
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import httplib2
from apiclient import discovery, errors
from oauth2client import client, file, tools

SCOPES = 'https://www.googleapis.com/auth/gmail.send'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Gmail API Quickstart'


def get_credentials():
    store = file.Storage('credentials.json')
    credentials = store.get()
    if not credentials or credentials.invalid:
        print("nooot!!")
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        credentials = tools.run_flow(flow, store)
        print('Storing credentials')
    return credentials


def send_message(sender, to, subject, msgHtml, msgPlain):
    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    service = discovery.build('gmail', 'v1', http=http)
    message1 = create_message(sender, to, subject, msgHtml, msgPlain)
    send_message_internal(service, "me", message1)


def send_message_internal(service, user_id, message):
    try:
        message = (service.users().messages().send(userId=user_id, body=message).execute())
        print('Message Id: %s' % message['id'])
        return message
    except errors.HttpError as error:
        print('An error occurred: %s' % error)


def create_message(sender, to, subject, msgHtml, msgPlain):
    msg = MIMEMultipart('alternative')
    msg['Subject'] = subject
    msg['From'] = sender
    msg['To'] = to
    msg.attach(MIMEText(msgPlain, 'plain'))
    msg.attach(MIMEText(msgHtml, 'html'))
    raw = base64.urlsafe_b64encode(msg.as_bytes())
    raw = raw.decode()
    body = {'raw': raw}
    return body

# example:
# def main():
#     to = "parmisstavassoli@gmail.com"
#     sender = "jadehkhaki.team@gmail.com"
#     subject = "subject"
#     msgHtml = "Hi<br/>Html Email"
#     msgPlain = "Hi\nPlain Email"
#     send_message(sender, to, subject, msgHtml, msgPlain)
#
# if __name__ == '__main__':
#     main()
