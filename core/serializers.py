from rest_framework import serializers


from .models import (Address, CityTown, Image, Member,
                     Person, Province, PhoneNo)


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = ('title', 'explanation', 'picture')


class ProvinceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Province
        fields = ('name',)


class CityTownSerializer(serializers.ModelSerializer):
    province = ProvinceSerializer()

    class Meta:
        model = CityTown
        fields = ('province', 'city', 'town')


class AddressSerializer(serializers.ModelSerializer):
    city = serializers.SerializerMethodField()
    town = serializers.SerializerMethodField()
    province = serializers.SerializerMethodField()

    def get_city(self, address):
        return address.city_town.city

    def get_town(self, address):
        return address.city_town.town

    def get_province(self, address):
        return address.city_town.province.name

    class Meta:
        model = Address
        fields = ('city', 'town', 'province')


class PersonSerializer(serializers.ModelSerializer):
    photo = ImageSerializer()

    class Meta:
        model = Person
        fields = ('first_name', 'last_name', 'gender', 'photo', 'about')


class SearchTitleSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=64)
    category = serializers.CharField(max_length=10)


class MemberSerializer(serializers.ModelSerializer):
    photo = ImageSerializer()

    class Meta:
        model = Member
        exclude = ('id',)


class PhoneNoSerializer(serializers.ModelSerializer):
    class Meta:
        model = PhoneNo
        exclude = ('id',)
