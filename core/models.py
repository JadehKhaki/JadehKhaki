import uuid as uuid_lib

from django.db import models
from django.db.models import Q

# change into choice: province, city, town, category
# make another table for lang, tag, (addition??)
# check validity: rating, age, min and max groupsize, checkin, checkout


class Image(models.Model):
    uuid = models.UUIDField(db_index=True, default=uuid_lib.uuid4, unique=True, editable=False)
    title = models.CharField(max_length=80)
    explanation = models.CharField(blank=True, max_length=256)
    picture = models.ImageField(default='default.png')

    def __str__(self):
        return self.title


class Province(models.Model):
    uuid = models.UUIDField(db_index=True, default=uuid_lib.uuid4, unique=True, editable=False)
    name = models.CharField(unique=True, max_length=40)

    def __str__(self):
        return self.name


class LocationManager(models.Manager):
    def location_match(self, location):
        return super().get_queryset().filter(
            Q(city__contains=location) |
            Q(town__contains=location) |
            Q(province__name__contains=location)
        )


class CityTown(models.Model):
    uuid = models.UUIDField(db_index=True, default=uuid_lib.uuid4, unique=True, editable=False)
    city = models.CharField(max_length=40)
    town = models.CharField(max_length=40)
    # Relations:
    province = models.ForeignKey(Province, on_delete=models.CASCADE)

    objects = LocationManager()

    def __str__(self):
        return self.city + ' ' + self.town  # add province


class Address(models.Model):
    uuid = models.UUIDField(db_index=True, default=uuid_lib.uuid4, unique=True, editable=False)
    exact_address = models.CharField(max_length=256) # ino
    postal_code = models.CharField(blank=True, max_length=20)
    access_routes = models.CharField(blank=True, max_length=256)
    longitude = models.DecimalField(blank=True, null=True, max_digits=9, decimal_places=6)
    latitude = models.DecimalField(blank=True, null=True, max_digits=9, decimal_places=6)
    # fks: phone number :-?
    # Relations:
    city_town = models.ForeignKey(CityTown, on_delete=models.CASCADE)

    def __str__(self):
        return self.exact_address


class Language(models.Model):
    uuid = models.UUIDField(db_index=True, default=uuid_lib.uuid4, unique=True, editable=False)
    language = models.CharField(unique=True, max_length=40)

    def __str__(self):
        return self.language


# each person can have some tags,
# tag conceptualization for people would be interesting
class PersonTag(models.Model):
    uuid = models.UUIDField(db_index=True, default=uuid_lib.uuid4, unique=True, editable=False)
    tag = models.CharField(unique=True, max_length=64)
    # Images:
    image = models.ForeignKey(Image, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.tag


class Person(models.Model):
    FEMALE = 'FE'
    MALE = 'MA'
    OTHER = 'OT'
    GENDER_CHOICES = (
        (FEMALE, 'Female'),
        (MALE, 'Male'),
        (OTHER, 'Other'),
    )
    uuid = models.UUIDField(db_index=True, default=uuid_lib.uuid4, unique=True, editable=False)
    first_name = models.CharField(max_length=40)
    last_name = models.CharField(max_length=40)
    about = models.CharField(max_length=2048, blank=True)
    gender = models.CharField(max_length=2, choices=GENDER_CHOICES, default=OTHER)
    birth_date = models.DateField()
    national_id = models.CharField(unique=True, max_length=11)
    email = models.EmailField(blank=True, null=True)  # unique?
    # Relations:
    # address = models.OneToOneField(Address) # This is correct
    address = models.ForeignKey(Address, on_delete=models.CASCADE)  # Test edition
    languages = models.ManyToManyField(Language)
    tag = models.ManyToManyField(PersonTag, blank=True)
    # Images:
    photo = models.ForeignKey(Image, null=True, on_delete=models.CASCADE)
    # Add card image

    def __str__(self):
        return self.first_name + ' ' + self.last_name


class PhoneNo(models.Model):
    CELLPHONE = 'CE'
    HOMEPHONE = 'HO'
    OTHER = 'OT'
    PHONE_CHOICE = (
        (CELLPHONE, 'Cell phone'),
        (HOMEPHONE, 'Home phone'),
        (OTHER, 'Other')
    )
    uuid = models.UUIDField(db_index=True, default=uuid_lib.uuid4, unique=True, editable=False)
    country_code = models.CharField(max_length=4, default='+98', blank=True)
    code = models.CharField(max_length=4)
    number = models.CharField(max_length=15)
    phone_type = models.CharField(max_length=2, choices=PHONE_CHOICE, default='CE')
    # Relations
    person = models.ForeignKey(Person, blank=True, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.country_code + self.code + self.number


class Member(models.Model):
    uuid = models.UUIDField(db_index=True, default=uuid_lib.uuid4, unique=True, editable=False)
    name = models.CharField(max_length=20)
    field = models.CharField(max_length=20)
    university = models.CharField(max_length=20)
    photo = models.ForeignKey(Image, null=True, on_delete=models.CASCADE)
    instagram = models.URLField(blank=True, null=True)
    website = models.URLField(blank=True, null=True)
    linkedin = models.URLField(blank=True, null=True)
    twitter = models.URLField(blank=True, null=True)
    email = models.EmailField(blank=True, null=True)

    def __str__(self):
        return self.name

