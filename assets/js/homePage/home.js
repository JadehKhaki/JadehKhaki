import React from 'react';
import {Link} from 'react-router-dom';

import TopListGallery from '../list/topListGallery'


class HomePage extends React.Component{
    render(){
        return (

            <div>

                <div className="front-page-section" id="categories-list">
                    <div className="section-wrap">
                        <SectionTitle title="دنبال چی میگردی؟" subtitle="تجربه‌‌ی لذت‌بخش اقامت به سبک محلی و لمس تجربه‌های هیجان‌انگیز"/>

                        <div className="categories-wrap">
                            <ul className="categories">

                                <CategoryItem cid="homes" ctext="اقامتگاه" />

                                <CategoryItem cid="experiences" ctext="تجربه" />

                            </ul>
                        </div>
                    </div>
                </div>


                <HowItWorks role="normalScreenHIW" />


                <div className="front-page-section" id="top-list">
                    <div className="section-wrap">
                        <SectionTitle title="محبوب‌ترین‌ها" subtitle="آن‌چه گردشگران بیشتر پسندیده‌اند" />

                        <div className="toplist-wrap">
                            <ul className="toplist">

                                <TopListGallery length="4"/>

                            </ul>
                        </div>
                    </div>
                </div>

                <HowItWorks role="smallScreenHIW" />

            </div>

        );
    }
}

export default HomePage;



class SectionTitle extends React.Component {
    render() {
        return (
            <h3 className="widget-title">{this.props.title}
                <span className="widget-subtitle">{this.props.subtitle}</span>
            </h3>
        );
    }
}

class CategoryItem extends React.Component {
    render() {
        return (
            <li className="category-item">
                <div className="category">
                    <div className="category-cover" id={this.props.cid}>
                        <div className="category-icon">

                        </div>
                        <Link className="category-text" to={"/"+this.props.cid}>
                            <span>{this.props.ctext}</span>
                        </Link>
                    </div>
                </div>
            </li>
        );
    }
}


class HowItWorks extends React.Component{
    constructor(props) {
        super(props);
    }

    render(){
            return (
            <div>

                <div className={`front-page-section ${this.props.role}`} id="howItWorks">
                    <div className="section-wrap">
                        <SectionTitle title="سایت ما چطور کار میکنه؟"/>

                        <div>
                             <ul className="steps-list">
                    
                                <li className="step">
                                  <img className="stepImage" src="/static/images/howitworks/step1.jpg" alt="Find" />
                                  <h2>پیدا کن!</h2>
                                  <p>مقصد سفرت رو جستجو کن و پیشنهادهای هیجان‌انگیز جاده‌خاکی رو ببین.  میزبانان ما بهترین و متفاوت‌ترین تجربه‌ها رو در موضوعات مختلف با شما به اشتراک میذارن.</p>
                                </li>
                                <li className="stepArrow">
                                  <img className="arrow" src="/static/images/howitworks/arrow.png" alt="Arrow" />
                                </li>
                                <li className="step">
                                  <img className="stepImage" src="/static/images/howitworks/step2.jpg" alt="Book" />
                                  <h2>رزرو کن!</h2>
                                  <p>تجربه‌ی مورد نظر خودتون رو پیدا کردید؟ درخواست رزرو  رو به صورت آنلاین ثبت کنید تا هماهنگی‌های لازم برای لمس یک تجربه‌ی جدید برای شما فراهم بشه.</p>
                                </li>
                                <li className="stepArrow">
                                  <img className="arrow" src="/static/images/howitworks/arrow.png" alt="Arrow" />
                                </li>
                                <li className="step">
                                  <img className="stepImage" src="/static/images/howitworks/step3.jpg" alt="Enjoy" />
                                  <h2>لذت ببر!</h2>
                                  <p>شما به مقصد رسیدید! میزبان شما با آغوش باز از شما پذیرایی خواهد کرد. تجربه فوق العاده‌ای داشته باشید.</p>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
              
            );
    }
}

