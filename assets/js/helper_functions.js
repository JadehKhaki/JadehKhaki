export function slicePrice(p){
    p = p.toString();
        var price="";
        if(p.length > 3) {
            if ((p.length % 3) > 0)
                price = p.slice(0, (p.length % 3)) + ",";

            for (var i = 0; i < p.length / 3; i++) {
                price = price + p.slice(p.length % 3 + 3 * i, p.length % 3 + 3 * (i + 1));
                if ((i < p.length / 3 - 2) || (i < p.length / 3 - 1 && p.length % 3 == 0))
                    price += ",";
            }
            return price;
        }
        return p;
}