import React from 'react'
import SearchBar from '../search/search'
import SignUp from '../signUp'
import ax from '../ax';

class MainHeader extends React.Component{
    render(){
        return (
            <div className="banner">
                <div className="bannerGrayCover">
                    <MainPageNavbar />
                    <div className="row">
                        <div className="col-sm-1"></div>
                            <div id="titleBoxDiv" className="col-sm-10">
                                <h1 id="main_name">جاده خاکی</h1>
                                <h3 id="main_info">مسیر تجربه‌های هیجان‌انگیز</h3>
                                <SearchBar searchURL="_all" />

                                <ul className="qualityInsuranceIcons">                                    
                                    <li>    
                                        <span id="backup24"></span>
                                        <span>پشتیبانی و رزرو ۲۴ ساعته</span>
                                    </li>

                                    <li>
                                        <span id="moneyGuarantee"></span>
                                        <span>ضمانت بازگشت وجه</span>
                                    </li>

                                    <li>
                                        <span id="checkPerson"></span>
                                        <span>اعتبارسنجی میزبان و گردشگر</span>
                                    </li>

                                    <li>
                                        <span id="hostTraining"></span>
                                        <span>آموزش و تأیید میزبان</span>
                                    </li>

                                    <li>
                                        <span id="qualityInsurance"></span>
                                        <span>کنترل کیفی تجربه‌ها</span>
                                    </li>
                                </ul>

                            </div>
                        <div className="col-sm-1"></div>
                    </div>
                </div>
            </div>
        );
    }
}

class MainPageNavbar extends React.Component{

    constructor(props) {
        super(props);
        this.state={data: [],loaded:false,loggedin:false};
        this.logout=this.logout.bind(this);
    }

    loadItemFromServer() {
          ax.get('/rest-auth/user/')
          .then(function(response){
              this.setState({data:response.data, loaded:true, loggedin:true});

          }.bind(this))
          .catch(function(error){
            if (error.response){
                if(error.response.status==403)
                    this.setState({loggedin:false, loaded:true});
                else
                    this.setState({loaded:false});
            }
          }.bind(this));
    }

    componentWillMount() {
        this.loadItemFromServer();
    }

    logout(e){
          ax.post('/rest-auth/logout/',{
          })
          .then(function(response){
            e.preventDefault();
            window.location="/";
          }.bind(this))
          .catch(function(error){
             e.preventDefault();
            window.location="/";
          }.bind(this));
        }



    render(){
        const link="/";
        var modal="#myModal";
        var loginDisplay="block";
        var helloDisplay="none";
        var id = modal.substring(1, modal.end);
        if (this.state.loaded){
            if (this.state.loggedin){
                loginDisplay="none";
                helloDisplay="block";
            }
        return(
            <div>
                <nav id="mainPageNavbar" className="navbar-inverse navbar">
                    <div id="mainPNavbar">
                        <ul className="nav navbar-nav navbar-right">
                            <li style={{"display":loginDisplay}}><a data-toggle="modal" data-target={modal}>ورود/ثبت‌نام</a></li>
                            <li style={{"display":helloDisplay}} className="dropdown">
                                <a className="dropdown-toggle" data-toggle="dropdown" href="#" id="navHello">سلام {this.state.data.username}
                                    <span id="userCaret" className="caret"></span>
                                </a>
                                <ul className="dropdown-menu" style={{"textAlign": "center"}}>
                                  <li><a href="/profile">پروفایل</a></li>
                                  {/*<li><a href="#">سفارش‌ های من</a></li>*/}
                                  <div className="dropdown-divider"></div>
                                  <li><a onClick={this.logout}>خروج</a></li>
                                </ul>
                            </li>
                        </ul>
                        <div className="navbar-text grow" id="mainHeaderBlog"><div className="btn"><a href="http://www.blog.jadehkhaki.com" target="_blank">وبلاگ جاده‌خاکی</a></div></div>
                    </div>
                </nav>
                <SignUp modal={id} />
            </div>
        )}
        else
            return(<div>Data couldn't load</div>);
    }
}

export default MainHeader;