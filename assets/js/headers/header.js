import React from 'react';
import {Switch, Route} from 'react-router-dom';

import MainHeader from './mainHeader'
import Navbar from './navbar'

class Header extends React.Component{
    render(){
        return (
            <Switch>
               <Route exact path="/" component={MainHeader} />
	           <Route path="/*" component={Navbar} />
            </Switch>
        );
    }
}

export default Header;