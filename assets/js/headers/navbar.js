import React from 'react'
import SignUp from '../signUp'
import ax from '../ax';

class Navbar extends React.Component{
    constructor(props) {
        super(props);
        var activeTab="";
        if(this.props.location.pathname==="/homes")
          activeTab="homes";
        else if(this.props.location.pathname==="/experiences")
          activeTab="experiences";

        this.state={data: [],loaded:false,loggedin:false, activeTab:activeTab};
        this.logout=this.logout.bind(this);
    }

    loadItemFromServer() {
          ax.get('/rest-auth/user/')
          .then(function(response){
              this.setState({data:response.data, loaded:true, loggedin:true});

          }.bind(this))
          .catch(function(error){
            if (error.response){
                if(error.response.status==403)
                    this.setState({loggedin:false, loaded:true});
                else
                    this.setState({loaded:false});
            }
          }.bind(this));
    }

    componentWillMount() {
        this.loadItemFromServer();
    }

    logout(e){
          ax.post('/rest-auth/logout/',{
          })
          .then(function(response){
              window.location.reload();
          }.bind(this))
          .catch(function(error){

          }.bind(this));
        }

    setActive(k){
      this.setState({activeTab:k});
    }
    render(){
        var activeTab=this.state.activeTab;
        const link="/";
        var modal="#myModal";
        var id = modal.substring(1, modal.end);
        var loginDisplay="block";
        var helloDisplay="none";
        if (this.state.loaded){
            if (this.state.loggedin){
                loginDisplay="none";
                helloDisplay="block";
            }
        return (
            <div>
                    <nav className="navbar-inverse navbar navbar-fixed-top">

                      <div className="navbar-header">
                        <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#mainNavbar">
                          <span className="icon-bar"></span>
                          <span className="icon-bar"></span>
                          <span className="icon-bar"></span> 
                        </button>
                        <a className="navbar-brand" href={link}>جاده خاکی</a>
                      </div>
                      <div className="collapse navbar-collapse" id="mainNavbar">
                      <ul className="nav navbar-nav navbar-right">
                        <li className={(activeTab === "experiences") ? "active" : ""} onClick={this.setActive.bind(this,"experiences")}><a href={link+"experiences"}>تجربه</a></li>
                        <li className={(activeTab === "homes") ? "active" : ""} onClick={this.setActive.bind(this,"homes")}><a href={link+"homes"}>اقامتگاه</a></li>
                        <li style={{"display":loginDisplay}}><a data-toggle="modal" data-target={modal}>ورود/ثبت‌نام</a></li>
                        <li style={{"display":helloDisplay}} className="dropdown">
                          <a className="dropdown-toggle" data-toggle="dropdown" href="#" id="navHello">سلام {this.state.data.username}
                            <span id="userCaret" className="caret"></span></a>
                            <ul className="dropdown-menu" style={{"textAlign": "center"}}>
                              <li><a href="/profile">پروفایل</a></li>
                              <li><a onClick={this.logout}>خروج</a></li>
                            </ul>
                        </li>  
                      </ul>
                        <div className="navbar-text" id="navbarBlog"><div id="blogButton" className="btn"><a href="http://www.blog.jadehkhaki.com" target="_blank">وبلاگ جاده‌خاکی</a></div></div>
                        </div>
                    </nav>
                  <SignUp modal={id} />
            </div>
        )}
        else
            return(<div>Data couldn't load</div>);
    }
}

export default Navbar;