import React from 'react'
import {Link} from 'react-router-dom';

import TopListGallery from '../list/topListGallery'

class NotFound extends React.Component{
    constructor(props) {
        super(props);
    }

    render(){
            return (
              <div className="grid">
                <div className="row mainContent">
                  <div className="col-sm-1"></div>
                  <div id="errorDiv" className="col-sm-10"> 
                    <div id="errorTextDiv">
                      <h1 className="entry-title">خطا!</h1>
                      <h3>متاسفانه صفحه مورد نظر شما پیدا نشد.</h3>
                      <br/>
                      <Link id="errorPageLink" to="/">بازگشت به صفحه اصلی</Link>
                    </div>
                    <img id="errorImage" src="/static/images/no_result.jpg" />

                    <div>
                      <h3 className="widget-title">محبوب‌ترین‌های سایت</h3>
                      <div className="toplist">
                        <TopListGallery length="4"/>
                      </div>
                    </div>

                  </div>
                  <div className="col-sm-1"></div>
                </div>
              </div>
            
            );
    }
}

export default NotFound;