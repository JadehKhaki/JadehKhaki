import React from 'react';

class NoResult extends React.Component{
    
    render() {
        return (
              <div id="noSearchResultDiv">
              	<p id="noSearchResultHeader">متأسفانه نتیجه‌ای یافت نشد</p>
              	<img id="noSearchResultImg" src="/static/images/no_result.jpg" />
              </div>
            
            );
    }
}

export default NoResult;