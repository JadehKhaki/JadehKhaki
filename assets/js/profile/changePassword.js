import React from 'react'
import ax from '../ax';
import { Button } from 'react-bootstrap'


class ChangePasswordBox extends React.Component{
	constructor(props) {
        super(props);
        this.state={
        	new_password1:"",
        	new_password2:"",
        	passChange:{
        		status:"",
        		data:{}
        	}
        };
        this.changePassword = this.changePassword.bind(this);
        this.changeHandler = this.changeHandler.bind(this);
    }

    changeHandler(p){
    	this.setState({[p.target.id]: p.target.value});
    }


    changePassword(){
    	ax.post('/rest-auth/password/change/',{
    		new_password1: this.state.new_password1,
    		new_password2: this.state.new_password2
    	}).then(function(response){
    		this.setState({
    			new_password1:"",
    			new_password2:"",
    			passChange:{
    				status: "successful",
    				data: response.data
    			}
    		});
          }.bind(this))
          .catch(function(error){
            this.setState({
            	new_password1:"",
            	new_password2:"",
            	passChange:{
            		status: "fail",
            		data:error.response.data
            	}
            });
          }.bind(this));
    }

	render(){
		var errors=[];
		var errorsList=[];

		for(var e in this.state.passChange.data.new_password1){
			errors.push(this.state.passChange.data.new_password1[e]);
		}

		for(var e in this.state.passChange.data.new_password2){
			errors.push(this.state.passChange.data.new_password2[e]);
		}



      if (this.state.passChange.status=="fail"){
        if(this.state.passChange.data){
          for(var e in errors){
            if(errors[e]=="The two password fields didn't match."){
              var error = (<div key="4" style={{textAlign:"center"}} className="alert alert-danger alert-dismissible fade in">رمز عبور با تکرار رمز عبور یکسان نیست.</div>);
              errorsList.push(error);
            }
            if(errors[e]== "This password is too short. It must contain at least 8 characters."){
              var error = (<div key="5" style={{textAlign:"center"}} className="alert alert-danger alert-dismissible fade in">رمز عبور انتخاب شده کمتر از ۸ کاراکتر است</div>);
              errorsList.push(error);
            }
            if(errors[e]== "This password is too common."){
              var error = (<div key="5" style={{textAlign:"center"}} className="alert alert-danger alert-dismissible fade in">رمز عبور انتخاب شده ضعیف است.(از حروف و عدد استفاده کنید)</div>);
              errorsList.push(error);
            }
            if(errors[e]== "The password is too similar to the username."){
              var error = (<div key="5" style={{textAlign:"center"}} className="alert alert-danger alert-dismissible fade in">شباهت رمز عبور انتخاب شده با نام کاربری زیاد است.</div>);
              errorsList.push(error);
            }
          }
        }

      }
		return(
				<div id="itemSummary">
				<div style={{marginTop:"3em"}} className="row">
					<div className="row" style={{textAlign:"center"}}>
						<h4>تغییر رمز عبور</h4>
					</div>
					{this.state.passChange.status=="successful" && <div style={{textAlign:"center"}} className="alert alert-success alert-dismissible fade in">تغییر رمز عبور با موفقیت انجام شد.</div>}
					{this.state.passChange.status=="fail" && errorsList}
					<div className="col-md-6">
						<div className="inputWrapper">
							<span className="fas fa-key"></span>
							<input type="password" name="newPassRepeat" id="new_password2" value={this.state.new_password2} onChange={this.changeHandler} placeholder="تکرار رمز عبور جدید" />
						</div>
					</div>
					<div className="col-md-6">
						<div className="inputWrapper">
						  	<span className="fas fa-key"></span>
							<input type="password" name="newPass" id="new_password1" value={this.state.new_password1} onChange={this.changeHandler} placeholder="رمز عبور جدید" />
						</div>
					</div>

					{/*<div className="col-md-4">
											<div className="inputWrapper">
												<input type="password" name="currentPass" placeholder="رمز عبور فعلی" />
											</div>
										</div>*/}

				</div>
				<div className="row">
					<div className="col-md-4">
					</div>
					<div style={{textAlign:"center"}} className="col-md-4">
						<Button onClick={this.changePassword} className="form-control btn btn-login" style={{width:"100%"}}>تغییر رمز عبور</Button>
					</div>
					<div className="col-md-4">
					</div>
				</div>
				</div>
		)
	}
}


export default ChangePasswordBox;
