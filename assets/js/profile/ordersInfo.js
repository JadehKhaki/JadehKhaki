import React from 'react'
import ax from '../ax';
import moment from 'jalali-moment';

import HandleReservationModal from './handleReservationModal.js';
import { Button } from 'react-bootstrap';
import {Link} from 'react-router-dom';

class OrdersInfo extends React.Component {
	constructor(props) {
		super(props);
		this.state =  {exp_data:[], home_data:[], loaded:false, index: null, type: null, operation:null};

		this.loadReservationsFromServer = this.loadReservationsFromServer.bind(this);
	}

	loadReservationsFromServer() {
		ax.get('api/experience_reservations/')
			.then(function (response) {
				var tmp = response.data;
				tmp.sort(function(b,a) {return (a.created_at > b.created_at) ? 1 : ((b.created_at > a.created_at) ? -1 : 0);} ); // sort by date
				this.setState({exp_data: tmp, loaded: true});
			}.bind(this)).catch(function (error) {
			this.setState({notFound: true});
		}.bind(this));

		ax.get('api/home_reservations/')
		.then(function(response){
			var tmp = response.data;
			tmp.sort(function(b, a) {
				return (a.created_at > b.created_at)? 1:((b.created_at > a.created_at) ? -1 : 0);
			});
			this.setState({home_data: tmp, loaded:true});
		}.bind(this));

	}

	componentDidMount() {
		this.loadReservationsFromServer()
	}

	proceedReservation (i, type) {
		this.setState({operation: 'proceed', index: i});
		if (type==='exp')
			this.setState({type: 'exp'});
		else
			this.setState({type: 'home'});
		$('#handleReservationModal').modal('show');
	}

	cancelReservation (i, type){
		this.setState({operation: 'cancel', index: i});
		if (type==='exp')
			this.setState({type: 'exp'});
		else
			this.setState({type: 'home'});
		$('#handleReservationModal').modal('show');
	}

	viewReservationDetails (i, type) {
		this.setState({operation: 'view', index: i});
		if (type==='exp')
			this.setState({type: 'exp'});
		else
			this.setState({type: 'home'});
		$('#handleReservationModal').modal('show');
	}

	render() {
		var status_dict = {'PR': 'در دست بررسی',
			'PO': 'امکان‌پذیر',
			'IM': 'امکان‌ناپذیر',
			'PA': 'پرداخت‌شده',
			'LA': 'از زمان پرداخت گذشته',
			'CA': 'لغو شده',
		};
		var exp_reservation_rows = [];

		if (this.state.loaded && this.state.exp_data.length > 0) {
			for (var i = 0; i < this.state.exp_data.length; i++) {
				let index = i;
				var row = (
					<tr key={index}>
						<th scope="row">{index + 1}</th>
						<td><Link to={"/experience/" + this.state.exp_data[index].url_title} target="_blank">{this.state.exp_data[index].experience}</Link></td>
						<td>{moment(this.state.exp_data[index].created_at, 'YYYY-MM-DD HH:mm:ss').locale('fa').format('HH:mm:ss YYYY-MM-DD')}</td>
						<td>{status_dict[this.state.exp_data[index].status]}
							<br/>
							<span hidden={this.state.exp_data[index].status !== 'PO'}>
								<button className="btn btn-success reservationOrderButton" onClick={(e)=> {this.proceedReservation(index, 'exp');}}>پرداخت</button>
                  				<button className="btn btn-danger reservationOrderButton" onClick={(e)=> {this.cancelReservation(index, 'exp');}}>لغو</button>
							</span>

							<span hidden={this.state.exp_data[index].status !== 'PR'}>
                  				<button className="btn btn-danger reservationOrderButton" onClick={(e)=> {this.cancelReservation(index, 'exp');}}>لغو درخواست</button>
							</span>
						</td>
						<td><button className="btn reservationOrderButton" onClick={(e)=> {this.viewReservationDetails(index, 'exp');}}>جزئیات</button></td>
					</tr>
				);
				exp_reservation_rows.push(row);
			}
			var exp_reservation_table = (
				<table className="table">
					<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col">تجربه</th>
						<th scope="col">تاریخ درخواست</th>
						<th scope="col">وضعیت</th>
						<th scope="col"></th>
					</tr>
					</thead>
					<tbody>
					{exp_reservation_rows}
					</tbody>
				</table>

			);

		}

		var home_reservation_rows = [];
		if (this.state.loaded && this.state.home_data.length > 0) {
			for (var i = 0; i < this.state.home_data.length; i++) {
				let index = i;
				var row = (
					<tr key={index}>
						<th scope="row">{index + 1}</th>
						<td><Link to={"/home/" + this.state.home_data[index].url_title} target="_blank">{this.state.home_data[index].home}</Link></td>
						<td>{this.state.home_data[index].room}</td>
						<td>{moment(this.state.home_data[index].created_at, 'YYYY-MM-DD HH:mm:ss').locale('fa').format('HH:mm:ss YYYY-MM-DD')}</td>
						<td>{status_dict[this.state.home_data[index].status]}
							<br/>
							<span hidden={this.state.home_data[index].status !== 'PO'}>
								<button className="btn btn-success reservationOrderButton" onClick={(e)=> {this.proceedReservation(index, 'home');}}>پرداخت</button>
                  				<button className="btn btn-danger reservationOrderButton" onClick={(e)=> {this.cancelReservation(index, 'home');}}>لغو</button>
							</span>

							<span hidden={this.state.home_data[index].status !== 'PR'}>
                  				<button className="btn btn-danger reservationOrderButton" onClick={(e)=> {this.cancelReservation(index, 'home');}}>لغو درخواست</button>
							</span>
						</td>
						<td><button className="btn reservationOrderButton" onClick={(e)=> {this.viewReservationDetails(index, 'home');}}>جزئیات</button></td>
					</tr>
				);

				home_reservation_rows.push(row);
			}
			var home_reservation_table = (
				<table className="table">
					<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col">خانه</th>
						<th scope="col">اتاق</th>
						<th scope="col">تاریخ درخواست</th>
						<th scope="col">وضعیت</th>
						<th scope="col"></th>
					</tr>
					</thead>
					<tbody>
					{home_reservation_rows}
					</tbody>
				</table>

			);

		}

		return (
			<div>
				<HandleReservationInfo type={this.state.type} reservation={this.state.type==='exp'? this.state.exp_data[this.state.index] : this.state.home_data[this.state.index]} buttonFunction={this.state.operation}/>

				<div id="itemDescriptionRow" style={this.props.style}>
					<header style={{textAlign: "center"}}>
						<img src="static/images/profile/delivery-list.svg" alt="order" width="173" height="170"/>
						<h1>سفارش‌ های من</h1>
					</header>

					<div className="itemSummary table-responsive">
						<h2 className="orderInfosCategoryHeader">«رزروهای تجربه»</h2>
						{exp_reservation_table}
					</div>
					<div className="itemSummary table-responsive">
						<h2 className="orderInfosCategoryHeader">«رزروهای اقامتگاه»</h2>
						{home_reservation_table}
					</div>
				</div>
			</div>
		);
	}
}


class HandleReservationInfo extends React.Component {
  constructor(props) {
    super(props);
    this.getReserveInfo = this.getReserveInfo.bind(this);
  }

  getReserveInfo(){
  	if (this.props.reservation !== undefined){
  		if (this.props.type==='exp')
		    return {
		    	uuid: this.props.reservation.uuid,
	    		reservation: this.props.reservation.experience,
	    		date: this.props.reservation.check_in,
	    		count: this.props.reservation.count,
	    		price: this.props.reservation.price};

	    else if (this.props.type==='home')
	    	return {
	    		uuid: this.props.reservation.uuid,
	    		reservation: this.props.reservation.home,
	    		room: this.props.reservation.room,
	    		checkIn: this.props.reservation.check_in,
	    		checkOut: this.props.reservation.check_out,
	    		count: this.props.reservation.count,
	    		price: this.props.reservation.price};

    	else
    		return{
    			uuid: null,
    			reservation: null,
    			room: null,
    			check_in: null,
    			check_out: null,
    			date: null,
    			count: null,
    			price: null
    		};
	}

	else{
		return{
			uuid: null,
			reservation: null,
			room: null,
			check_in: null,
			check_out: null,
			date: null,
			count: null,
			price: null
		};
	}
  }

  render() {
	return(
		<HandleReservationModal type={this.props.type} reserveInfo={this.getReserveInfo()} buttonFunction={this.props.buttonFunction} />
    );
  }
}

export default OrdersInfo;