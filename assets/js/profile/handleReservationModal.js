import React from 'react';
import {Redirect } from 'react-router';
import ax from '../ax';

class HandleReservationModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {proceed: false, cancel: false};

    this.proceedReservation = this.proceedReservation.bind(this);
    this.cancelReservation = this.cancelReservation.bind(this);
  }

  proceedReservation (){
    console.log("proceed");

    this.setState({proceed: true});
  }

  cancelReservation () {
    console.log("cancel");

    var cancellUrl = "/api/cancel/?uuid=" + this.props.reserveInfo.uuid + "&type=" + (this.props.type==='exp'? 'experience':'home');

    ax.post(cancellUrl)
    .then(function(response){
        this.setState({cancel: true});
    }.bind(this))
  }

  render() {
    if (this.state.proceed){
      $('#handleReservationModal').modal('hide');
      var proceedUrl = "/request/?type=" + (this.props.type==='exp'? 'experience':'home') + "&uuid=" + this.props.reserveInfo.uuid;
      window.location.reload();
      return(<Redirect to={proceedUrl}/>);
    }

    else if (this.state.cancel){
      window.location.reload();
    }

    else

      return(
        <div className="container modal fade" id="handleReservationModal" role="dialog">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-body">
                <button style={{paddingLeft:"10px"}} type="button" className="close" data-dismiss="modal">&times;</button>
                

                <h3 className="handleReservationWarning" style={{color: 'grey', display: (this.props.buttonFunction=='view'? 'block':'none')}}>جزئیات رزرو </h3>
                <h3 className="handleReservationWarning" style={{color: 'green', display: (this.props.buttonFunction=='proceed'? 'block':'none')}}>آیا این رزرو را برای پرداخت تأیید می‌کنید؟</h3>
                <h3 className="handleReservationWarning" style={{color: 'red', display: (this.props.buttonFunction=='cancel'? 'block':'none')}}>آیا مطمئنید که می‌خواهید این رزرو را لغو کنید؟</h3>
                <h5 className="handleReservationWarning" style={{color: 'red', display: (this.props.buttonFunction=='cancel'? 'block':'none')}}>پس از لغو، امکان بازیابی این رزرو وجود نخواهد داشت.</h5>

                <div className="handleReservationDetails">
                  <h4>رزرو {this.props.type==='exp'? 'تجربه:': 'اقامتگاه:'} {this.props.reserveInfo.reservation}</h4>
                  <h4 style={{display: this.props.type==='exp'? 'none': 'block'}}>{this.props.type==='exp'? '' : 'اتاق:' + this.props.reserveInfo.room}</h4>
                  <h4>تعداد نفرات: {this.props.reserveInfo.count}</h4>
                  <div style={{display: this.props.type==='home'? 'none': 'block'}}>
                    <h4>{this.props.type==='home'? '' : 'تاریخ:' + this.props.reserveInfo.date}</h4>
                  </div>

                  <div style={{display: this.props.type==='exp'? 'none': 'block'}}>
                    <h4>{this.props.type==='exp'? '' : 'تاریخ ورود:' + this.props.reserveInfo.checkIn}</h4>
                    <h4>{this.props.type==='exp'? '' : 'تاریخ خروج:' + this.props.reserveInfo.checkOut}</h4>
                  </div>
                  <h4 className="finalize-reservation-price">هزینه: {this.props.reserveInfo.price}</h4>
                </div>

                <div className="reservationDetailsButtonsDiv">
                  <button style={{display: (this.props.buttonFunction=='proceed'? 'block':'none')}} className="btn btn-success reservationDetailsButton" onClick={this.proceedReservation}>تأیید و پرداخت</button>
                  <button style={{display: (this.props.buttonFunction=='cancel'? 'block':'none')}} className="btn btn-danger reservationDetailsButton" onClick={this.cancelReservation}>لغو رزرو</button>
                  <button style={{display: (this.props.buttonFunction=='view'? 'none':'block')}} data-dismiss="modal" className="btn reservationDetailsButton">بستن پنجره</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
  }
}

export default HandleReservationModal;