import React from 'react'
import ax from '../ax';
import {Link} from 'react-router-dom';

import UserInfo from './userInfo';
import OrdersInfo from './ordersInfo';


class Profile extends React.Component{
	 constructor(props) {
        super(props);

        var _orderInfoStyle = "none";
        var _userInfoStyle = "block";
        var _userInfoPillClass = "active";
        var _orderInfoPillClass = "";

        const url = window.location.href;
        const startChar = url.indexOf('#', 8);
        const path = url.substr(startChar, url.length);
        if(path.substr(0, 7) === '#orders'){
            _orderInfoStyle = "block";
            _userInfoStyle = "none";
            _userInfoPillClass = "";
            _orderInfoPillClass = "active";
        }

        this.state={
            data: [],
            loaded:false,
            profile:true,
            signedIn: true,
            stickyClass:"",
            orderInfoStyle: _orderInfoStyle,
            userInfoStyle: _userInfoStyle,
            userInfoPillClass: _userInfoPillClass,
            orderInfoPillClass: _orderInfoPillClass
        };

        //this.onScroll=this.onScroll.bind(this);
        this.userInfoClick=this.userInfoClick.bind(this);
        this.orderInfoClick=this.orderInfoClick.bind(this);
    }

    loadUserFromServer(){
    	ax.get('/rest-auth/user/')
            .then(function(response){
                this.setState({
                	data:{
                		phone_no:{
                			code:"",
                			number:""
                		},
                		email:response.data.email,
                		first_name:"",
                		last_name:"",
                		username:response.data.username,
                		national_id:"",
                		photo:{
                			picture:""
                		},
                		card_photo:{
                			picture:""
                		}
                	},
                	loaded:true,
                	profile:false
                });
            }.bind(this)).catch(function (error){
            	this.setState({signedIn: false});
            }.bind(this));
    }

    loadItemFromServer() {
        ax.get('api/profile/')
            .then(function(response){
                this.setState({
                	data:response.data,
                	loaded:true,
                	profile:true
                });
            }.bind(this)).catch(function (error){
            	this.loadUserFromServer();
            }.bind(this));
    }

    componentWillMount() {
        //this.loadItemFromServer();
        //window.removeEventListener("scroll", this.onScroll, false);
    }

    componentDidMount() {
        this.loadItemFromServer();
        //window.addEventListener("scroll", this.onScroll, false);
    }

    userInfoClick(){
        window.location.hash='info';

    	this.setState({
    		userInfoPillClass:"active",
    		orderInfoPillClass:"",
    		userInfoStyle:"block",
    		orderInfoStyle:"none"
    	});
    }

    orderInfoClick(){
        window.location.hash='orders';

    	this.setState({
    		userInfoPillClass:"",
    		orderInfoPillClass:"active",
    		userInfoStyle:"none",
    		orderInfoStyle:"block"
    	});
    }

    /*onScroll() {
      var topSpace = 100 + 36 * document.documentElement.clientWidth / 100;
      if (window.scrollY >= topSpace ) {
          this.setState({stickyClass: "sticky"});
      } else if (window.scrollY < topSpace) {
          this.setState({stickyClass: ""});
      }
    }*/

    render(){
        if (!this.state.signedIn){
            return (
                <div className="grid">
                    <div className="col-sm-1"></div>
                    <div id="itemDescriptionRow" className="col-sm-10">
                        <h3>برای دسترسی به این صفحه، باید ابتدا به حساب کاربری خود وارد شوید.</h3>
                        <Link to="/"><button className="btn reserveButton">بازگشت به صفحه اصلی</button></Link>
                    </div>
                    <div className="col-sm-1"></div>
                </div>
            );
        }

        if(this.state.loaded) {
            return (
            	<div style={{marginTop:"5em"}} className="container">

	                <div className="row mainContent">

	                  <div className="col-md-10">
	                   <UserInfo style={{display:this.state.userInfoStyle}} userdata={this.state.data} profile={this.state.profile} />
                       <OrdersInfo style={{display:this.state.orderInfoStyle}} />
	                  </div>

	                  <div id="profileMenu" style={{marginTop:"1em"}} className="col-md-2">
	                      <ul className="nav nav-pills nav-stacked">
						    <li className={this.state.userInfoPillClass} onClick={this.userInfoClick}><a id="userInfo">اطلاعات کاربری</a></li>
						    <li className={this.state.orderInfoPillClass} onClick={this.orderInfoClick}><a id="orderInfo">سفارش‌ های من</a></li>
						  </ul>
	                  </div>

	                </div>

              </div>
            
            );
        }
        return(<div>Data couldn't load</div>);
    }
}


export default Profile;