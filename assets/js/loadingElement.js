import React from 'react';

class LoadingElement extends React.Component{
    constructor(props) {
        super(props);
    }

    render() {
        return (<i style={{fontSize: (this.props.size=='undefined'? '20': this.props.size)+'px',
                        display: (this.props.display=='undefined'? 'inherit': this.props.display)}}
                        className="fa fa-spinner fa-spin buttonload"></i>);
    }
}

export default LoadingElement;
