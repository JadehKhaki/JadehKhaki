import React from 'react';

import CategoryList from '../list/categoryList';
import SearchBar from './search';


class SearchResults extends React.Component{
    render(){
        var location_query = this.props.match.params.searchParams.split("&")[0];
        var title_query = this.props.match.params.searchParams.split("&")[1];
        var location = location_query.split("=")[1];
        var title = title_query.split("=")[1];
        
        if (this.props.match.params.searchURL==="_all"){
            var search_url = "/api/search_homes/?location=" + location + "&title=" + title;
            var search_url2 = "/api/search_experiences/?location=" + location + "&title=" + title;
        }

        else{
            var search_url = "/api/search"+ this.props.match.params.searchURL +"/?location=" + location + "&title=" + title;
            var search_url2 = null;
        }

        return (
            <div className="categoryList-wrap">
                <div className="row category-search-div">
                    <div className="col-sm-1"></div>
                    <div className="col-sm-10">
                        <SearchBar where={location} what={title} searchURL={this.props.match.params.searchURL} />
                    </div>
                    <div className="col-sm-1"></div>
                </div>

                <ul className="categoryList">

                    <CategoryList url={search_url} url2={search_url2} />

                </ul>
            </div>
        );
    }
}

export default SearchResults;