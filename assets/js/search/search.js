import React from 'react'
import ax from '../ax';

class SearchBar extends React.Component{
    constructor(props) {
        super(props);
        var where_ = this.props.where==null? '' : this.props.where;
        var what_ = this.props.what==null? '' : this.props.what;

        this.state = {location: where_ , title: what_ };
        this.getLocation=this.getLocation.bind(this);
        this.getTitle=this.getTitle.bind(this);
        this.search=this.search.bind(this);
    }

    getLocation (where){
      this.setState({location: where});
    }

    getTitle(what){
      this.setState({title: what});
    }

    search(e){
      e.preventDefault();
      window.location = '/search' + this.props.searchURL + '/location=' + this.state.location + '&title=' + this.state.title;
    }

    render(){
            return (	
            	<div id="searchDiv">
            	<form onSubmit={this.search}>
            		<div>
                        <List id="what" placeholder="مثلا صیادی" sendContent={this.getTitle} val={this.state.title} />
            			<List id="where" placeholder="مثلا تهران" sendContent={this.getLocation} val={this.state.location} />
	            			<button type="submit" id="searchButton"><i className="fas fa-search"></i></button>
               		</div>
                </form>
                </div>
            
            );
        }
}


class List extends React.Component{
  constructor(props) {
        super(props);
        this.state = {classes:"dropdown withBefore", data:[], inputValue: this.props.val};
        this.searchWithList=this.searchWithList.bind(this);
        this.search=this.search.bind(this);
  }

  searchWithList(e){
    var input = e.target.id;
  	this.setState({classes:"dropdown withBefore", inputValue:input});
    this.props.sendContent(input);
  }

  loadFromServer(input){
      var url = '';
      if (this.props.id=='where') {
          url = 'api/locationSuggestion/';
          if (input)
              url = url + '?location=' + input;
      }
      else {
          url = 'api/titleSuggestion/';
          url = url + '?title=' + input;
      }
      ax.get(url)
          .then(function(response){
              this.setState({data:response.data});
          }.bind(this));
  }

  search(e){
      var input = e.target.value;
      this.loadFromServer(input);
      this.setState({classes:"dropdown withBefore", inputValue:e.target.value});
      this.props.sendContent(input);
  }

  componentWillMount(){
      this.loadFromServer(this.state.inputValue);
  }

  
  render(){
    return (
			<div id={this.props.id} className={this.state.classes}>

	            <input type="text" value={this.state.inputValue} onChange={this.search} placeholder={this.props.placeholder} className="dropdown-toggle searchInput" data-toggle="dropdown" autoComplete="false" />
	           	<ul className="dropdown-menu" onClick={this.searchWithList} id="searchDropdown">
					{this.state.data.map(function(item) { return <li id={item.title} data-category={item.title} key={item.title}>{item.title}</li> })}
				</ul> 
	        </div>
      )
  }
};

export default SearchBar;