import React from 'react';
import LoadingElement from './loadingElement';

class RedirectingZarinpal extends React.Component{
    constructor(props) {
        super(props);
    }

    render(){
            return (
              <div className="grid">
                <div className="row mainContent">
                  <div className="col-sm-1"></div>
                  <div id="itemDescriptionRow" className="col-sm-10">
                    <article>    
                      <header id="aboutUsHeader">
                        <h1 id="redirectingHeader">در حال انتقال به درگاه پرداخت اینترنتی زرین‌پال</h1>
                        <h3>از شکیبایی شما سپاسگزاریم</h3>
                        <div className="redirectingElement"><LoadingElement size='100'/></div>
                      </header>
                    </article>
                  </div>
                  <div className="col-sm-1"></div>
                </div>
              </div>
            
            );
    }
}

export default RedirectingZarinpal;