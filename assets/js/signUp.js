import React from 'react'
import ax from './ax';
import { Button } from 'react-bootstrap'

class SignUp extends React.Component{
    constructor(props) {
        super(props);
        this.state={
          loginClass:"active",
          signupClass:"",
          loginFormStyle:"block",
          signupFormStyle:"none",
          loginPassword:"",
          loginUsername:"",
          confirmPassword:"",
          signupPassword:"",
          signupUsername:"",
          email:"",
          login:"",
          registration:{
            status:"",
            data:{}
          }
        };

        this.changeToSignup=this.changeToSignup.bind(this);
        this.changeToLogin=this.changeToLogin.bind(this);
        this.login=this.login.bind(this);
        this.register=this.register.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
      this.setState({[event.target.id.substring(this.props.modal.length)]: event.target.value});
    }

    changeToSignup(){
        this.setState({
          loginClass:"",
          signupClass:"active",
          loginFormStyle:"none",
          signupFormStyle:"block"
        });
    }

    changeToLogin(){
      this.setState({
        loginClass:"active",
        signupClass:"",
        loginFormStyle:"block",
        signupFormStyle:"none"
      });

    }

    validateLoginForm() {
      return this.state.loginPassword.length > 0 && this.state.loginUsername.length > 0;
    }

    validateSignupForm() {
      return (this.state.confirmPassword==this.state.signupPassword) && (this.state.signupPassword.length > 0 && this.state.signupUsername.length > 0 && this.state.confirmPassword.length > 0 && this.state.email.length > 0);
    }

    login(e){
        var pass=this.state.loginPassword;
        var user=this.state.loginUsername;
          ax.post('/rest-auth/login/',{
              password: pass,
              username: user,
              email:""

          })
          .then(function(response){
              this.setState({
                loginPassword:"",
                loginUsername:"",
                login:"successful"
              });


              if (typeof this.props.proceedReservation !== 'function')
                window.location.reload();
                
          }.bind(this))
          .catch(function(error){
              this.setState({
                login:"fail"
              });
          }.bind(this));
        }

    register(e){
          ax.post('/rest-auth/registration/',{
              username: this.state.signupUsername,
              email: this.state.email,
              password1: this.state.signupPassword,
              password2: this.state.confirmPassword

          })
          .then(function(response){
              this.setState({
                confirmPassword:"",
                signupPassword:"",
                signupUsername:"",
                email:"",
                registration:{
                  status:"successful",
                  data:{}
                }
              });
              
              if (typeof this.props.proceedReservation !== 'function')
                window.location.reload();

          }.bind(this))
          .catch(function(error){
            this.setState({
              registration:{
                status:"fail",
                data:error.response.data
              }
            });
          }.bind(this));
    }

    render(){

      if (typeof this.props.proceedReservation === 'function' && (this.state.login == 'successful' || this.state.registration.status == 'successful')){
        $('.modal').modal('hide');
        this.props.proceedReservation();
      }


      var errors=[];
      if (this.state.registration.status=="fail" && this.state.registration.data){
        if(this.state.registration.data.username){
          var error = (<div key="1" style={{textAlign:"center"}} className="alert alert-danger alert-dismissible fade in">نام کاربری از قبل انتخاب شده است.</div>);
          errors.push(error);
        }
        if(this.state.registration.data.email){          
          if(this.state.registration.data.email[0]=="A user is already registered with this e-mail address."){
            var error = (<div key="2" style={{textAlign:"center"}} className="alert alert-danger alert-dismissible fade in">ایمیل از قبل ثبتنام شده است.</div>);
            errors.push(error);
          }
          if(this.state.registration.data.email[0]=="Enter a valid email address."){
            var error = (<div key="3" style={{textAlign:"center"}} className="alert alert-danger alert-dismissible fade in">ایمیل وارد شده معتبر نیست.</div>);
            errors.push(error);
          }
        }

        if(this.state.registration.data.password1){
          for(var e in this.state.registration.data.password1){
            if(this.state.registration.data.password1[e]=="This password is too common."){
              var error = (<div key="4" style={{textAlign:"center"}} className="alert alert-danger alert-dismissible fade in">رمز عبور انتخاب شده ضعیف است.(از حروف و عدد استفاده کنید)</div>);
              errors.push(error);
            }
            if(this.state.registration.data.password1[e]== "This password is too short. It must contain at least 8 characters."){
              var error = (<div key="5" style={{textAlign:"center"}} className="alert alert-danger alert-dismissible fade in">رمز عبور انتخاب شده کمتر از ۸ کاراکتر است</div>);
              errors.push(error);
            }
          }
        }

        if(this.state.registration.data.non_field_errors){
          for(var e in this.state.registration.data.non_field_errors){
            if(this.state.registration.data.non_field_errors[e]== "The two password fields didn't match."){
              var error = (<div key="6" style={{textAlign:"center"}} className="alert alert-danger alert-dismissible fade in">تکرار رمز عبور با رمز عبور انتخاب شده یکسان نیست.</div>);
              errors.push(error);
            }
          }
        }

      }
            return (
              <div className="container modal fade" id={this.props.modal} role="dialog">
                <div className="modal-dialog">
                  <div className="modal-content">
                    <div className="modal-body">
                      <div className="row">
                        <div className="col-md-12">
                          <div className="panel panel-login">

                            <div className="panel-heading">
                              <div className="row">
                                <div className="col-xs-6">
                                  <a href="#" className={this.state.loginClass} id="login-form-link" onClick={this.changeToLogin}>ورود</a>
                                </div>
                                <div className="col-xs-6">
                                  <a href="#" className={this.state.signupClass} id="register-form-link" onClick={this.changeToSignup}>ثبت نام</a>
                                </div>
                              </div>
                            </div>

                            <div className="panel-body">
                              <div className="row">
                                <div className="col-lg-12">

                                  <form id={this.props.modal+"login-form"} role="form" style={{display:this.state.loginFormStyle}}>
                                    
                                    <div className="form-group">
                                      <input type="text" name="username" value={this.state.loginUsername} id={this.props.modal+"loginUsername"} tabIndex="1" className="form-control" placeholder="نام کاربری" onChange={this.handleChange}/>
                                    </div>
                                    <div className="form-group">
                                      <input type="password" name="password" value={this.state.loginPassword} id={this.props.modal+"loginPassword"} tabIndex="2" className="form-control" placeholder="رمز عبور" onChange={this.handleChange} />
                                    </div>

                                    <div className="form-group">
                                      <div className="row">
                                        {this.state.login=="successful" && <div style={{textAlign:"center"}} className="alert alert-success alert-dismissible fade in">ورود شما با موفقیت انجام شد.</div>}
                                        {this.state.login=="fail" && <div style={{textAlign:"center"}} className="alert alert-danger alert-dismissible fade in">نام کاربری/رمز عبور اشتباه است.</div>}
                                        <div className="col-sm-6 col-sm-offset-3">
                                          <Button disabled={!this.validateLoginForm()} onClick={this.login} name="login-submit" id={this.props.modal+"login-submit"} tabIndex="4" className="form-control btn btn-login">ورود</Button>
                                        </div>
                                      </div>
                                    </div>

                                    {/*<div className="form-group">
                                                                          <div className="row">
                                                                            <div className="col-lg-12">
                                                                              <div className="text-center">
                                                                                <a href="https://phpoll.com/recover" tabIndex="5" className="forgot-password">رمز عبور خود را فراموش کردید؟</a>
                                                                              </div>
                                                                            </div>
                                                                          </div>
                                                                        </div>*/}
                                  </form>

                                  <form id={this.props.modal+"register-form"} role="form" style={{display:this.state.signupFormStyle}}>
                                    <div className="form-group">
                                      <input type="text" name="username" id={this.props.modal+"signupUsername"} tabIndex="1" className="form-control" placeholder="نام کاربری" value={this.state.signupUsername} onChange={this.handleChange}/>
                                    </div>
                                    <div className="form-group">
                                      <input type="email" name="email" id={this.props.modal+"email"} tabIndex="1" className="form-control" placeholder="آدرس ایمیل" value={this.state.email} onChange={this.handleChange}/>
                                    </div>
                                    <div className="form-group">
                                      <input type="password" name="password" id={this.props.modal+"signupPassword"} tabIndex="2" className="form-control" placeholder="رمز عبور" value={this.state.signupPassword} onChange={this.handleChange}/>
                                    </div>
                                    <div className="form-group">
                                      <input type="password" name="confirm-password" id={this.props.modal+"confirmPassword"} tabIndex="2" className="form-control" placeholder="تکرار رمز عبور" value={this.state.confirmPassword} onChange={this.handleChange}/>
                                    </div>
                                    <div className="form-group">
                                      <div className="row">
                                        {this.state.registration.status=="successful" && <div style={{textAlign:"center"}} className="alert alert-success alert-dismissible fade in">ثبتنام و ورود شما با موفقیت انجام شد.</div>}
                                        {this.state.registration.status=="fail" && errors}
                                        <div className="col-sm-6 col-sm-offset-3">
                                          <Button disabled={!this.validateSignupForm()} onClick={this.register} name="register-submit" id={this.props.modal+"register-submit"} tabIndex="4" className="form-control btn btn-register"> ثبتنام</Button>
                                        </div>
                                      </div>
                                    </div>
                                  </form>

                                </div>
                              </div>
                            </div>

                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
                      

            );
    }
}

export default SignUp;