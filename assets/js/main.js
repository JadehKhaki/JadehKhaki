import React, { Suspense, lazy} from 'react'
import {Switch, Route} from 'react-router-dom'

const HomePage = lazy(() => import('./homePage/home' /* webpackChunkName: "homePage" */));
//import HomePage from './homePage/home'
const HomeItem = lazy(() => import('./Items/Home/homeItem' /* webpackChunkName: "homeItem" */));
//import HomeItem from './Items/Home/homeItem'
const ExperienceItem = lazy(() => import('./Items/Experience/experienceItem' /* webpackChunkName: "expItem" */));
//import ExperienceItem from './Items/Experience/experienceItem'
const AboutUs = lazy(() => import('./aboutus/aboutUs' /* webpackChunkName: "aboutUs" */));
//import AboutUs from './aboutus/aboutUs'
const ContactUs = lazy(() => import('./contact/contactUs' /* webpackChunkName: "contactUs" */));
//import ContactUs from './contact/contactUs'
const HomeList = lazy(() => import('./list/homeList'  /* webpackChunkName: "homeList" */));
//import HomeList from './list/homeList'
const ExperienceList = lazy(() => import('./list/experienceList' /* webpackChunkName: "expList" */));
//import ExperienceList from './list/experienceList'
const Rules = lazy(() => import('./rules' /* webpackChunkName: "rules" */));
//import Rules from './rules'
const Profile = lazy(() => import('./profile/profile' /* webpackChunkName: "profile" */));
//import Profile from './profile/profile'
const NotFound = lazy(() => import('./errors/notFound' /* webpackChunkName: "notFound" */));
//import NotFound from './errors/notFound'
const SearchResults = lazy(() => import('./search/searchResults' /* webpackChunkName: "searchResults" */));
//import SearchResults from './search/searchResults'
const FinalizeHomeReservation = lazy(() => import('./Items/Home/finalizeReservation' /* webpackChunkName: "finalizeHomeRes" */));
//import FinalizeHomeReservation from './Items/Home/finalizeReservation'
const FinalizeExpReservation = lazy(() => import('./Items/Experience/finalizeReservation' /* webpackChunkName: "finalizeExpRes" */));
//import FinalizeExpReservation from './Items/Experience/finalizeReservation'
import RedirectingZarinpal from './redirectingZarinpal'

// import Blog from './blog'

class Main extends React.Component{
    render(){
        return(
            <Suspense fallback={<div>Loading...</div>}>
                <Switch>
                    <Route exact path='/' component={HomePage}/>
                    <Route exact path='/homes' component={HomeList}/>
                    <Route exact path='/experiences' component={ExperienceList}/>
                    <Route path='/home/:id' component={HomeItem}/>
                    <Route path='/experience/:id' component={ExperienceItem}/>
                    <Route path='/about-us/' component={AboutUs}/>
                    <Route path='/contact-us/' component={ContactUs}/>
                    <Route path='/rules/' component={Rules}/>
                    <Route path='/profile' component={Profile}/>
                    <Route path='/search:searchURL/:searchParams' component={SearchResults} />
                    <Route path='/reserve_home' component={FinalizeHomeReservation} />
                    <Route path='/reserve_experience' component={FinalizeExpReservation} />
                    <Route path='/request/' component={RedirectingZarinpal} />
                    <Route component={NotFound}/>
                    
                    {/*<Route path='/blog/' component={Blog}/>*/}
                </Switch>
            </Suspense>
        );
    }
}

export default Main;