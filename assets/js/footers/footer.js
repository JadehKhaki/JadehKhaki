import React from 'react';
import {Switch, Route} from 'react-router-dom';

import CommonFooter from './commonFooter'
import ItemPageFooter from './itemPageFooter'

class Footer extends React.Component{
    render(){
        return (
            <Switch>
                <Route path="/home/*" component={ItemPageFooter} />
                <Route path="/experience/*" component={ItemPageFooter} />
                <Route path="/*" component={CommonFooter} />
            </Switch>
        );
    }
}

export default Footer;