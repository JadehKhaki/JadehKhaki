import React from 'react'
import { Button } from 'react-bootstrap'

class CommentForm extends React.Component{
    constructor(props) {
        super(props);
    }

    render(){
            return (
            	<div id="itemSummary">
					<div id="comments" className="comments-area">
						<div id="respond" className="comment-respond">
							<h4 id="reply-title" className="comment-reply-title">نظر خود را به اشتراک بگذارید </h4>
							<form id="commentform" className="comment-form" noValidate="">
							<p className="comment-form-comment">
								<label>نظر شما:</label>
								<textarea id="comment" name="comment" cols="45" rows="8" aria-required="true" required="required" placeholder="درمورد تجربه‌ی خود به دیگران بگویید"></textarea>
							</p>
							<p className="comment-form-author">
								<label>نام<span className="required">*</span></label>
								<input id="author" placeholder="نام شما" name="author" type="text" value="" size="30" maxLength="245" required="required" />
							</p>

							<p className="comment-form-email">
								<label>ایمیل<span className="required">*</span></label>
								<input autoComplete="email" id="email" placeholder="your@email.com" name="email" type="email" value="" size="30" maxLength="100" aria-describedby="email-notes" required="required" />
							</p>

							<p className="review-title-form">
								<label>عنوان</label>
								<input type="text" id="pixrating_title" name="pixrating_title" value="" placeholder="خلاصه‌ای از توضیحات شما" size="25" />
							</p>

							<p className="form-submit">
								<Button type="submit" id="submit" className="submit">ثبت نظر</Button>
							</p>	
							</form>
						</div>
					</div>
				</div>
            );
    }
}

export default CommentForm;