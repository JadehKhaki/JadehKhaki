import React from 'react'
import ax from '../ax';
import { ControlLabel , FormControl , Button } from 'react-bootstrap'
import CSRFToken from '../csrftoken';
import LoadingElement from '../loadingElement';


class ContactUs extends React.Component{
    constructor(props) {
        super(props);
    }

    render(){
            return (
              <div className="grid">
                <div className="row mainContent">
                  <div className="col-sm-1"></div>
                  <div id="itemDescriptionRow" className="col-sm-10">
                    <header id="aboutUsHeader">
                        <h1 className="entry-title">تماس با ما</h1>
                    </header>
                    
                    <ContactForm />
                    <ContactInfo/>
                  </div>
                  <div className="col-sm-1"></div>
                </div>
              </div>
            
            );
    }
}

class ContactInfo extends React.Component{
    constructor(props) {
        super(props);
    }

    render(){
            return (
                  <div className="col-sm-6">
                    <h4 id="address"><b>آدرس پستی</b></h4>
                    <p>خیابان ولیعصر، ابتدای خیابان شهید مطهری، کوچه منصور، پلاک ۸۳، واحد ۴، استارتاپ جاده‌خاکی</p>
                    <br />
                    
                    <h4 id="phoneNumber"><b>شماره تماس</b></h4>
                    <p className="contactP">۰۲۱-۸۸۵۵۶۵۱۹‌</p>
                    <p className="contactP">۰۲۱-۲۸۴۲۶۲۰۶‌</p>
                    <p className="contactP">۰۹۳۸۳۱۳۷۴۶۶</p>
                    <br />
                    
                    <h4><i className="fab fa-telegram-plane"></i><b>پشتیبانی تلگرام</b></h4>
                    <p className="contactP"><a href="http://www.t.me/jadehkhaki_support"> http://www.t.me/jadehkhaki_support </a></p>
                    <br />
                    
                    <h4 id="email"><b>ایمیل</b></h4>
                    <p className="contactP">info@jadehkhaki.com</p>
                  </div>
            );
    }
}

class ContactForm extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
          team:[],
          data:[],
          alert_display:'none',
          name: '',
          email: '',
          message: '',
          blank: '',
          formErrors: {email: '', name: '', message: '', blank: ''},
          emailValid: false,
          nameValid: false,
          messageValid: false,
          blankValid: true,
          formValid: false,
          loadingButtonDisplay: 'none'
        }

        this.sendMail = this.sendMail.bind(this);
        this.handleUserInput = this.handleUserInput.bind(this);
    }

    sendMail(e) {
        if (this.state.blankValid){
          this.setState({loadingButtonDisplay: 'inherit'});
          ax.post('/api/send_email/',{
              subject:this.state.name,
              message:this.state.message,
              fromEm:this.state.email
          })
          .then(function(response){
              this.setState({alert_display: 'inherit'});
              this.setState({loadingButtonDisplay: 'none'});
          }.bind(this));
        }
    }

    handleUserInput(e) {
      const name = e.target.name;
      const value = e.target.value;
      this.setState({[name]: value},
                    () => { this.validateField(name, value) });
    }

    validateField(fieldName, value) {
      let fieldValidationErrors = this.state.formErrors;
      let emailValid = this.state.emailValid;
      let nameValid = this.state.nameValid;
      let messageValid = this.state.messageValid;
      let blankValid = this.state.blankValid;

      switch(fieldName) {
          case 'email':
            emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
            fieldValidationErrors.email = emailValid ? '' : 'ایمیل وارد شده صحیح نیست';
            break;
          case 'name':
            nameValid = value.length > 0;
            fieldValidationErrors.name = nameValid ? '': 'وارد نشده است';
            break;
          case 'message':
            messageValid = value.length > 0;
            fieldValidationErrors.message = messageValid ? '': 'وارد نشده است';
            break;
          case 'blank':
            blankValid = value.length === 0;
            fieldValidationErrors.blank = blankValid ? '': 'لطفا خالی بگذارید';
            break;
          default:
            break;
        }

        this.setState({formErrors: fieldValidationErrors,
                        emailValid: emailValid,
                        nameValid: nameValid,
                        messageValid: messageValid,
                        blankValid: blankValid
                      }, this.validateForm);
    }

    validateForm() {
      this.setState({formValid: this.state.emailValid && this.state.nameValid && this.state.messageValid && this.state.blankValid});
    }

    errorClass(error) {
      return(error.length === 0 ? '' : 'has-error');
    }


     render(){
        return (
              <div className="col-sm-6">
                <div style={{display: this.state.alert_display}} className="alert alert-success alert-dismissible fade in">
                  پیام شما با موفقیت ارسال شد.
                </div>

                <form>
                    <CSRFToken/>
                <div className={`form-group ${this.errorClass(this.state.formErrors.name)}`}>
                  <div className="label-error-div">
                    <ControlLabel className="required">نام</ControlLabel>
                    <ControlLabel className="error">{this.state.formErrors.name}</ControlLabel>
                  </div>
                  <FormControl 
                    placeholder="نام خود را وارد کنید" 
                    type="text"
                    className="form-control" name="name"
                    value={this.state.name}
                    onChange={this.handleUserInput} />
                </div>

                <div className={`form-group ${this.errorClass(this.state.formErrors.email)}`}>
                  <div className="label-error-div">
                    <ControlLabel className="required">ایمیل</ControlLabel>
                    <ControlLabel className="error">{this.state.formErrors.email}</ControlLabel>
                  </div>
                  <FormControl autoComplete='email' type="email" 
                    placeholder="ایمیل خود را وارد کنید"
                    className="form-control" name="email"
                    value={this.state.email}
                    onChange={this.handleUserInput} />
                </div>

                
                <div className={`form-group ${this.errorClass(this.state.formErrors.message)}`}>
                  <div className="label-error-div">
                    <ControlLabel className="required">متن</ControlLabel>
                    <ControlLabel className="error">{this.state.formErrors.message}</ControlLabel>
                  </div>
                  <FormControl componentClass="textarea" 
                    placeholder="متن پیام خود را وارد کنید"
                    rows="6" cols="50" 
                    className="form-control" name="message"
                    value={this.state.message}
                    onChange={this.handleUserInput} />
                </div>

                <div className={`form-group ${this.errorClass(this.state.formErrors.blank)} anti-spam`}>
                  <div className="label-error-div">
                    <ControlLabel className="error">{this.state.formErrors.blank}</ControlLabel>
                  </div>
                  <FormControl 
                    placeholder="لطفا این قسمت را خالی بگذارید" 
                    type="text"
                    className="form-control" name="blank"
                    value={this.state.blank}
                    onChange={this.handleUserInput} />
                </div>

                <Button onClick={this.sendMail} disabled={!this.state.formValid}>ارسال<LoadingElement size='20' display={this.state.loadingButtonDisplay} /></Button>
                </form>
              </div>
        );
    }
}

export default ContactUs;