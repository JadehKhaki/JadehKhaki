import React from 'react'
import ax from '../ax';

class AboutUs extends React.Component{
    constructor(props) {
        super(props);
    }

    render(){
            return (
              <div className="grid">
                <div className="row mainContent">
                  <div className="col-sm-1"></div>
                  <div id="itemDescriptionRow" className="col-sm-10">
                    <OurStartUp />
                    <br />
                    <br />
                    <OurTeam />
                  </div>
                  <div className="col-sm-1"></div>
                </div>
              </div>
            
            );
    }
}

class OurStartUp extends React.Component{
    constructor(props) {
        super(props);
    }

    render(){
            return (
                  <div>
                    <article>    
                      <header id="aboutUsHeader">
                        <h1 className="entry-title">درباره ما</h1>
                        <img className=" wp-image-554 aligncenter" src="http://jadehkhaki.com/wp-content/uploads/2018/02/Untitled-1-300x294.png" alt="" width="173" height="170" />
                      </header>

                      <div className="entry-content">
                        <p>تا حالا پیش اومده که از یکنواختی سفرها خسته شده باشی؟</p>
                        <p>که آخر هفته‌ها دنبال یک فعالیت یا سرگرمی جدید و جذاب باشی اما همه‌چیز تکراری شده باشه؟</p>
                        <p>پس دل رو بزن به جاده‌خاکی، چون مسیر تجربه‌های هیجان‌انگیز از جاده‌خاکی می‌گذره!</p>
                        <p>جاده‌خاکی سامانه ارائه تجربه‌های متفاوت و لذت‌بخش در مناطق مختلف کشوره که سعی داره به عنوان یک پل ارتباطی بین گردشگرها و جامعه محلی، سفرهای شخصی‌سازی شده‌ و جذاب‌تری رو برای گردشگرها رقم بزنه و اون‌ها رو با جامعه محلی همراه‌تر کنه.</p>
                        <p>گردشگری خلاق و تجربه‌گرا موضوعیه که در دنیا جا افتاده و بسیار طرفدار داره، و جاده‌خاکی به عنوان پیشرو در ارائه این تجربه‌ها در ایران فعالیت میکنه.</p>


                          {/*<p>امروزه گردشگری خلاق به عنوان نسل جدید گردشگری در دنیا بسیار مورد توجه قرار گرفته است و علاقه‌مندان آن نیز روز به روز بیشتر می‌شوند.</p>
                          <p>جاده‌خاکی یک استارتاپ در حوزه‌ی گردشگری دیجیتال است که قصد دارد به عنوان یکی از پیشگامان ارائه‌ی گردشگری تجربه‌گرا در قالب دیجیتال در ایران فعالیت کند.</p>
                          <p>به همین منظور، جاده‌خاکی پلی ارتباطی میان گردشگران و افراد بومی هر منطقه می‌زند تا گردشگران بتوانند خدمات مورد نظر خود را به طور مستقیم از افراد بومی دریافت کرده و سفر جذاب‌تری را تجربه کنند و همچنین افراد محلی بتوانند از این طریق، کسب‌و‌کار پایدار در حوزه‌ی گردشگری ایجاد کنند.</p>*/}
                        {/*<p dir="rtl" >جاده&zwnj;خاکی استارتاپی نوپا در حوزه&zwnj;ی گردشگری دیجیتال است که از طریق وبسایت، پلی ارتباطی میان گردشگران و افراد بومی هر منطقه می&zwnj;زند که گردشگران بتوانند به طور مستقیم از افراد بومی خدمات بگیرند. این خدمات در قالب تجربه&zwnj;های بومی خاص هر منطقه (مثلا سفالگری، صیادی، نجوم و …) و یا اقامتگاه&zwnj;های بومی موجود در هر منطقه می&zwnj;باشد. بعلاوه جاده&zwnj;خاکی با راهنمایان گردشگری مناطق مختلف هماهنگ می&zwnj;کند و بسته&zwnj;های گردشگری که می&zwnj;توانند ارائه دهند را در قالب تورهای محلی از طرف خودشان روی وبسایت قرار میدهد. گردشگران می&zwnj;توانند از طریق جاده&zwnj;خاکی از این خدمات بهره ببرند.</p>*/}
                        {/*<p dir="rtl" >هدف اصلی جاده&zwnj;خاکی کمک به توسعه&zwnj;ی گردشگری پایدار، اشتغال&zwnj;آفرینی برای بومیان هر منطقه و نیز رونق اقتصاد محلی می&zwnj;باشد. رویکرد محوری در بسته&zwnj;های گردشگری ارائه شده توسط تورلیدرها، بومگردی(اکوتوریسم) است که جزء اولویت&zwnj;های مورد تاکید ماست.</p>*/}
                        {/*<p dir="rtl" >این استارتاپ در مسابقه&zwnj;ی استارتاپ ویکند گردشگری پایدار دانشکده کارآفرینی دانشگاه تهران که در مهرماه سال ۱۳۹۶ برگزار شد، توانست رتبه&zwnj;ی دوم را کسب کند و هم&zwnj;اکنون در مرکز رشد سازمان تجاری&zwnj;سازی فناوری و اشتغال دانش&zwnj;آموختگان جهاد دانشگاهی تهران و تحت حمایت این سازمان و دانشکده&zwnj;ی کارآفرینی دانشگاه تهران در حال فعالیت می&zwnj;باشد.</p>*/}
                        {/*<p dir="rtl" >تیم جاده&zwnj;خاکی هم&zwnj;اکنون متشکل از دانش&zwnj;آموختگان بااستعداد دانشگاه&zwnj;های تهران، شریف و خواجه&zwnj;نصیر می&zwnj;باشد که با جدیت تمام در راه رسیدن به اهداف جاده&zwnj;خاکی در حال گام برداشتن هستند.</p>*/}
                      </div>
                    </article>
                  </div>
            
            );
    }
}

class OurTeam extends React.Component{
    constructor(props) {
        super(props);
        this.state={team:[],data:[]};
    }

    loadTeamFromServer() {
      ax.get('/api/team/')
          .then(function(response){
              this.setState({data: response.data});
          }.bind(this))
    }

    componentWillMount(){
      // this.setState({
      //     team:this.state.data[0]
      //   });
      this.loadTeamFromServer();
    }

    render(){
      var team=[];
      for (var i = 0; i < this.state.data.length; i++) {
                var member = (
                    <Member
                    key={this.state.data[i].id}
                    memberImage={this.state.data[i].photo.picture} 
                    memberName={this.state.data[i].name}
                    memberTitle={this.state.data[i].field}
                    memberUniv={this.state.data[i].university}
                    website={this.state.data[i].website}
                    linkedin={this.state.data[i].linkedin}
                    twitter={this.state.data[i].twitter}
                    instagram={this.state.data[i].instagram}
                    email={this.state.data[i].email}
                    />
                );
                team.push(member);
            }
            return (
                  <div>
                    <article>    
                      <header id="aboutUsHeader">
                        <h1 className="entry-title">تیم ما</h1>
                      </header>

                      <div className="entry-content">
                        <ul>
                          {team}
                        </ul>
                      </div>
                    </article>
                  </div>
            
            );
    }
}



class Member extends React.Component{
    constructor(props) {
        super(props);
    }

    render(){
      const website=this.props.website?true:false;
      const linkedin=this.props.linkedin?true:false;
      const twitter=this.props.twitter?true:false;
      const instagram=this.props.instagram?true:false;
      const email=this.props.email?true:false;

            return (
                
                        <li className="member">
                          <img className="avatar" src={this.props.memberImage} alt="Avatar" />
                          <h4>{this.props.memberName}</h4>
                          <br />
                          <h5>{this.props.memberTitle}</h5>
                          <h5>{this.props.memberUniv}</h5>

                          { website && <a href={this.props.website} className="memberWebsite"></a> }
                          { linkedin && <a href={this.props.linkedin} className="memberLinkedin"></a> }
                          { twitter && <a href={this.props.twitter} className="memberTwitter"></a> }
                          { instagram && <a href={this.props.instagram} className="memberInstagram"></a> }
                          { email && <a href={this.props.email} className="memberEmail"></a> }
                        </li>
              
            );
    }
}

export default AboutUs;