import React from "react";
import Slider from "react-slick";
import Card from '../list/galleryCard';


class SimpleSlider extends React.Component {
  render() {
      var settings = {
          slidesToShow: 1,
                dots:true,
                centerMode: true,
                adaptiveHeight: true,
                variableWidth: true,
                responsive: [
                {
                    breakpoint: 1000,
                    settings: {
                        slidesToShow: 1,
                    }
                },
                {
                    breakpoint: 750,
                    settings: {
                        slidesToShow: 1,
                    }
                }
                ]
      };
    var settings2 = {
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };
      var sliderData = this.props.items;
      if (sliderData.length == 1)
          settings = settings2;
    if (sliderData.length > 0) {

        var maxLength = 100;
        var rowList = [];
        for (var i = 0; i < sliderData.length; i++) {
            var card = (<div key={"sliderCard" + i}>
                    <Card src={sliderData[i].picture}
                          title={sliderData[i].title}
                          explanation={sliderData[i].explanation.substr(0, maxLength) + '...'}
                          url={sliderData[i].picture}
                    /></div>
            );
            rowList.push(card);
        }
            return (
                <Slider {...settings}>
                    {rowList}
                </Slider>
            );


    }
    else
        return null;
  }
}

export default SimpleSlider;