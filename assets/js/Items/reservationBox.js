import React from 'react'

class ReservationBox extends React.Component{
    constructor(props) {
        super(props);
    }

    render(){
            return (
            <div id="reservationBGBox">
              <div id="reservationDetails">
                  <h3>رزرواسیون</h3>

                  <p>جهت انجام هماهنگی برای رزرو یا دریافت اطلاعات بیشتر می‌توانید از یکی از روش‌های زیر اقدام کنید:</p>

                  <br />
                  <i className="fab fa-telegram-plane"></i>ارتباط با اکانت تلگرام پشتیبانی:‌
                  <br/>
                  <div className="centerText">t.me/jadehkhaki_support</div>

                  <br />
                  <i id="phoneNumber"></i>تماس با شماره ثابت:
                  <div className="centerText">۲۸۴۲۶۲۰۶(۰۲۱)</div>

                  <br />
                  <i className="fa fa-mobile-alt"></i>تماس با تلفن همراه:
                  <div className="centerText">۰۹۳۸۳۱۳۷۴۶۶</div>

                  <br />
                  <i className="fa fa-at"></i>رزرو آنلاین:
                  <div className="centerText"><button data-toggle="modal" data-target={"#reservationModal"} className="btn reserveButton">رزرو</button></div>
              </div>
              
             </div>
          );
    }
}

export default ReservationBox;