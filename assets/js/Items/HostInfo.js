import React from 'react';

class HostInfo extends React.Component{

    render(){
        var gender="";
        if (this.props.person.gender=="MA")
          gender = "جناب آقای ";
        else if (this.props.person.gender=="FE")
          gender = "سرکار خانم ";

        var name = this.props.person.first_name+" "+this.props.person.last_name;
        var about = (<div className="col-md-8">
            <br/>
            <p>{this.props.person.about}
            </p>
        </div>);
        var title_class = 'col-md-4';
        if(this.props.person.about.length==0) {
            about = <div></div>;
            title_class = 'col-md-12';
        }

        return(
            <div className="single-action-buttons">

                  <div className="itemSummary">
                      <h4 className="host">ارائه دهنده</h4>
                      <div className="row">
                          {about}
                          <div className={title_class}>
                              <div style={{"textAlign": "center"}}>
                                  <img className="hostAvatar" src={this.props.person.photo.picture} alt="Avatar" />
                                  <h4 style={{'wordWrap': 'break-word'}}>{name}</h4>
                              </div>
                          </div>

                      </div>
                  </div>
            </div>
        )
    }
}

export default HostInfo;