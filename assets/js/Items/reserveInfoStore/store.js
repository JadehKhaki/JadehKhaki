import { createStore } from 'redux';
import { saveReservationInfo } from './reducers';

export const reserveInfoStore = createStore (saveReservationInfo);