
export const ADD_ITEM = 'ADD_ITEM'

export function addItem (t, d) {
  return {
  	type: ADD_ITEM,
  	title: t,
  	description: d
  }
}
