import { ADD_ITEM } from './actions';

export function saveReservationInfo (state = {}, action) {
  switch (action.type) {
    case ADD_ITEM:
      
      return Object.assign({}, state, {
            [action.title]: action.description
      });
    
    default:
      return state
  }
}