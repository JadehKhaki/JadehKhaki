import React from 'react';
import ax from '../ax';
import moment from 'moment-jalali';
import { DatePicker } from 'react-persian-datepicker';
import { Button } from 'react-bootstrap';

class UserInfoForm extends React.Component{
  constructor(props) {
    super(props);

    this.state={
      profile: false,

      phoneNumber:"",
      email:"",
      first_name:"",
      last_name:"",
      username: "",
      national_id: "",
      // profilePhoto: this.props.userdata.photo.picture,
      // idPhoto:this.props.userdata.card_photo.picture,
      birth_date:moment(),
      duplicateValue:"none"
    };

    this.changeUserInfo = this.changeUserInfo.bind(this);
    this.changeHandler = this.changeHandler.bind(this);
    this.photoChangeHandler = this.photoChangeHandler.bind(this);
    this.loadUserFromServer = this.loadUserFromServer.bind(this);
    this.getProfile = this.getProfile.bind(this);
  }

  changeHandler(p){
    this.setState({[p.target.id]: p.target.value});
  }

  photoChangeHandler(p){
    this.setState({
      [p.target.id]: URL.createObjectURL(p.target.files[0]),
      [p.target.id+"file"]:p.target.files[0]
    });
                let files = p.target.files || p.dataTransfer.files;
          // if (!files.length) {
          //     console.log('no files');
          // }
  }

  changeUserInfo(){
    if (this.state.profile){
        ax.patch('/api/profile/',{
            //username: this.state.username,
            first_name: this.state.first_name,
            last_name: this.state.last_name,
            national_id: this.state.national_id,
            phone_no: {
              code: this.state.phoneNumber.substring(0,4),
              number: this.state.phoneNumber.substring(4,11),
            },
            // photo: this.state.profilePhoto,
            // card_photo: this.state.idPhoto,
            email: this.state.email,
            birth_date:this.state.birth_date.format("YYYY-MM-DD")
        })
        .then(function(response){
             this.props.onSubmit();
        }.bind(this))
        .catch(function(error){
          if (error.response){
              if(error.response.status==500){
                  this.setState({duplicateValue:"block"});
              }
              else
                  console.log(error.response);
          }
        }.bind(this));
    }
    else{
      ax.post('/api/create_profile/',{
            username: this.state.username,
            first_name: this.state.first_name,
            last_name: this.state.last_name,
            national_id: this.state.national_id,
      phone_no: {
              code: this.state.phoneNumber.substring(0,4),
              number: this.state.phoneNumber.substring(4,11),
            },
            // photo: {title: this.props.userdata.photo.title,explanation: this.props.userdata.photo.explanation, picture:this.state.profilePhoto},
            // card_photo: {title: this.props.userdata.card_photo.title,explanation: this.props.userdata.card_photo.explanation, picture:this.state.idPhoto},
            email: this.state.email,
            birth_date:this.state.birth_date.format("YYYY-MM-DD")

        })
        .then(function(response){
             this.props.onSubmit();
        }.bind(this))
        .catch(function(error){
          if (error.response){
              if(error.response.status==500){
                  this.setState({duplicateValue:"block"});
              }
              else
                  console.log(error.response);
          }
        }.bind(this));
    }
  }

  loadUserFromServer(){
    ax.get('/rest-auth/user/')
      .then(function(response){
        this.setState({
          phone_no:{
            code:"",
            number:""
          },
          email:response.data.email,
          first_name:"",
          last_name:"",
          username:response.data.username,
          national_id:"",
          photo:{
            picture:""
          },
          card_photo:{
            picture:""
          },
        });
      }.bind(this)).catch(function (error){
        console.log("server error");
      }.bind(this));
  }

  getProfile(){
    ax.get('api/profile/')
      .then(function(response){
          this.setState({
            profile: true,

            phoneNumber: (response.data.phone_no.code + response.data.phone_no.number),
            email: response.data.email,
            first_name: response.data.first_name,
            last_name: response.data.last_name,
            username: response.data.username,
            national_id: response.data.national_id,
            // profilePhoto: this.props.userdata.photo.picture,
            // idPhoto:this.props.userdata.card_photo.picture,
            birth_date: moment(response.data.birth_date),
            duplicateValue:"none"
          });
      }.bind(this)).catch(function (error){
        this.loadUserFromServer();
      }.bind(this));
  }

  componentDidMount() {
    this.getProfile();
  }

  render(){
    const styles= {
        calendarContainer: 'calendarContainer',
        dayPickerContainer: 'dayPickerContainer',
        monthsList: 'monthsList',
        daysOfWeek: 'daysOfWeek',
        dayWrapper: 'dayWrapper',
        selected: 'selected',
        heading: 'heading',
        prev: 'prev',
        next: 'next',
        title: 'title',
        selected: 'selected'
    }

    return(
      <div id="itemDescriptionRow">
        
        <div id="itemSummary">
          <div style={{display: this.state.duplicateValue}} className="alert alert-danger alert-dismissible fade in">
            این نام کاربری از قبل انتخاب شده است.
          </div>
          <div style={{textAlign:"center", display: this.state.profile?'none':'block'}} className="alert alert-warning alert-dismissible fade in">برای انجام  رزرو آنلاین، لطفا ابتدا اطلاعات پروفایل خود را تکمیل کنید.</div>
          <div style={{textAlign:"center", display: this.state.profile?'block':'none'}} className="alert alert-warning alert-dismissible fade in">آیا اطلاعات پروفایل خود را تأیید می‌کنید؟</div>
          <p style={{color: 'red'}}>* برای وارد کردن کد ملی و شماره موبایل، کیبورد خود را به انگلیسی تغییر دهید.</p>

          <form id="profileInfo">
            <div className="row">
              <div className="col-md-4 profileInput">
                <div className="inputWrapper">
                  <span className="far fa-user"></span>
                  <input type="text" className="form-control" name="username" placeholder="نام کاربری" id="username" autoComplete='username' value={this.state.username} readOnly required />
                </div>
              </div>
              <div className="col-md-4 profileInput">
                <div className="inputWrapper">
                  <span className="far fa-user"></span>
                  <input className="numberValidation" type="text" name="nationalId" dir="ltr"  pattern="^([0-9]*)$"  minLength="10" maxLength="10" id="national_id" placeholder="کد ملی" autoComplete='nationalId' value={this.state.national_id} onChange={this.changeHandler} required />
                </div>
              </div>
              <div className="col-md-4 profileInput">
                <div className="inputWrapper">
                  <span className="fas fa-mobile-alt"></span>
                  <input className="numberValidation" type="text" dir="ltr" pattern="^(09[0-9]*)$" minLength="11" maxLength="11" name="phoneNumber" id="phoneNumber" placeholder="شماره موبایل"  value={this.state.phoneNumber} autoComplete='tel' onChange={this.changeHandler} required />
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-md-4 profileInput">
                <div className="inputWrapper">
                  <span className="far fa-user"></span>
                  <input type="text" name="name" placeholder="نام" id="first_name" value={this.state.first_name} onChange={this.changeHandler} autoComplete='name' />
                </div>
              </div>
              <div className="col-md-4 profileInput">
                <div className="inputWrapper">
                  <span className="far fa-user"></span>
                  <input type="text" name="name" placeholder="نام خانوادگی" id="last_name" value={this.state.last_name} onChange={this.changeHandler} autoComplete='familyName' />
                </div>
              </div>
              <div className="col-md-4 profileInput"></div>
            </div>

              <div className="row">
                <div className="col-md-4 profileInput">
                  <div className="inputWrapper">
                    <DatePicker value={this.state.birth_date}   onChange={value => this.setState({birth_date: value})} placeholder="اتریخ تولد" calendarStyles={styles} required />
                    <span className="fas fa-birthday-cake"></span>
                  </div>
                </div>
                <div className="col-md-8 profileInput">
                  <div className="inputWrapper">
                    <span className="far fa-envelope"></span>
                    <input type="email" className="form-control" id="email" placeholder="آدرس ایمیل" value={this.state.email} autoComplete='email' readOnly required />
                  </div>
                </div>
              </div>

              {/* ***** national card picture and profile picture ***** */}
              {/*<div className="row">*/}
                {/*<div className="col-md-6">*/}
                  {/*<div className="form-group">*/}
                    {/*<label>انتخاب عکس پروفایل</label>*/}
                    {/*<input type="file" className="form-control-file" id="profilePhoto" onChange={this.photoChangeHandler} accept=".png, .jpg, .jpeg" />*/}
                    {/*<div id="imagePreview" style={{backgroundImage: "url(" + this.state.profilePhoto +")" }}></div>*/}
                  {/*</div>*/}
                {/*</div>*/}
                {/*<div className="col-md-6">*/}
                  {/*<div className="form-group">*/}
                    {/*<label> انتخاب عکس کارت ملی</label>*/}
                    {/*<input type="file" className="form-control-file" id="idPhoto" onChange={this.photoChangeHandler} accept=".png, .jpg, .jpeg" />*/}
                    {/*<div id="imagePreview" style={{backgroundImage: "url(" + this.state.idPhoto +")"}}></div>*/}
                    {/*</div>*/}
                  {/*</div>*/}
                {/*</div>*/}

              <div className="row">
                <div className="col-md-4"></div>
                <div style={{textAlign:"center"}} className="col-md-4">
                  <Button onClick={this.changeUserInfo} className="form-control btn btn-login" style={{width:"100%"}} name="change-submit" id="change-submit">تأیید و ادامه</Button>
                </div>
                <div className="col-md-4"></div>
              </div>

              {/*<ChangePasswordBox />*/}
          </form>
        </div>
      </div>
    );
  }
}


export default UserInfoForm;