import React from 'react';
import {Redirect } from 'react-router';
import moment from 'jalali-moment';
import {DatePicker} from "react-advance-jalaali-datepicker";
import SignUp from '../../signUp';
import {slicePrice} from '../../helper_functions';
import ax from '../../ax';

import { reserveInfoStore } from '../reserveInfoStore/store';
import { addItem } from '../reserveInfoStore/actions';

class ReservationModal extends React.Component {
  constructor(props) {
    super(props);
    var today = moment().format().substring(0,10).replace(/-/g,'/');

    today = moment(today, 'YYYY-MM-DD').locale('fa').format('YYYY-MM-DD');
    this.state = {date: today, selectedOptions: Array.apply(null, new Array(this.props.options.length)).map(function(){return false}),
                  personNum: this.props.constraints.min_group_size, calculatedPrice: 0,
                  loggedin: false, proceed: false, proceedTarget: "/reserve_experience"};


    this.checkLogin = this.checkLogin.bind(this);
    this.changeDate = this.changeDate.bind(this);
    this.calculatePrice = this.calculatePrice.bind(this);
    this.proceedReservation = this.proceedReservation.bind(this);
    // this.slicePrice = this.slicePrice.bind(this);
    this.saveReserveInfo = this.saveReserveInfo.bind(this);
    this.validateReserveForm = this.validateReserveForm.bind(this);
  }


  changeDate(unix, formatted){
    //console.log(unix); // returns timestamp of the selected value, for example.
    //console.log(formatted); // returns the selected value in the format you've entered, forexample, "تاریخ: 1396/02/24 ساعت: 18:30".

    this.setState({date: formatted});
  }

  calculatePrice (){
    var price = 0;
    for (var i=0; i<this.props.basePrices.length; i++){
      var base_price = this.props.basePrices[i];
      if(this.state.personNum >= base_price.min_count && this.state.personNum <= base_price.max_count) {
        price = base_price.cost * this.state.personNum;

      }
    }

    // for (var i=0; i<this.props.options.length; i++){
    //   price += (this.state.selectedOptions[i]? 1:0) * this.props.options[i].price * this.state.personNum;
    // }

    this.setState({calculatedPrice: slicePrice(price.toString())});
  }

  checkLogin (){
    ax.get('/rest-auth/user/')
    .then(function(response){
        this.setState({loggedin: true});
    }.bind(this))
    .catch(function(error){
      if (error.response){
          this.setState({loggedin: false});
      }
    }.bind(this));
  }

  componentDidMount() {
    this.checkLogin();
    this.calculatePrice();
  }

  proceedReservation (){
    this.setState({proceed: true});
  }

  saveReserveInfo(){
    //const unsubscribe = reserveInfoStore.subscribe(() => console.log(reserveInfoStore.getState()))

    reserveInfoStore.dispatch(addItem('category', 'experience'));
    reserveInfoStore.dispatch(addItem('url_title', this.props.url_title));
    reserveInfoStore.dispatch(addItem('experience', this.props.uuid));
    reserveInfoStore.dispatch(addItem('pic', this.props.image));
    reserveInfoStore.dispatch(addItem('options', this.state.selectedOptions));
    reserveInfoStore.dispatch(addItem('date', this.state.date));
    reserveInfoStore.dispatch(addItem('personNum', this.state.personNum));
    reserveInfoStore.dispatch(addItem('price', this.state.calculatedPrice));
    
    //unsubscribe()
  }

  validateReserveForm (){
    return (this.state.personNum >= this.props.constraints.min_group_size && this.state.personNum <= this.props.constraints.max_group_size);
  }

  render() {
    if (this.state.proceed){
      this.saveReserveInfo();
      $('.modal').modal('hide');
      return(<Redirect to={this.state.proceedTarget}/>);
    }

    var reservationTable="";

    if (this.props.options.length>0){
      var reservationTable=[];

      for(var i=0; i< this.props.options.length; i++){
        let index = i;
        var price="";
        if (this.props.options[i].price.toString().length>3)
          price=slicePrice(this.props.options[i].price.toString());
        else
          price=this.props.options[i].price;

        var row=(
          <tr key={index}>
            <td>{this.props.options[i].title}</td>
            <td>{price}</td>
            <td><button
                className={(this.state.selectedOptions[index]? 'checkedOption' : 'uncheckedOption') + " btn"}
                onClick={(e) =>
                {
                  var tmp = this.state.selectedOptions;
                  tmp[index] = !tmp[index];
                  this.setState({selectedOptions: tmp}, this.calculatePrice);
                }
                }>
            </button></td>
          </tr>
        );

        reservationTable.push(row);
      }
    }

    return(
      <div>
        <SignUp modal="signUpLogIn" proceedReservation={this.proceedReservation} />
        <div className="modal fade" id="reservationModal" role="dialog">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-body">
                <button style={{paddingLeft:"10px"}} type="button" className="close" data-dismiss="modal">&times;</button>

                <div className="datePicker">
                  <div className="datePickerItem">
                    <b>تاریخ:</b>
                    <DatePicker placeholder="تاریخ" format="jYYYY-jMM-jDD" onChange={this.changeDate} id="date" preSelected={this.state.date} />
                  </div>
                  <div className="datePickerItem">
                    <b>تعداد نفرات:</b>
                    <input className="personNum" type="number" value={this.state.personNum} onChange={(e) => {if(0 <= e.target.value && e.target.value <= this.props.constraints.max_group_size){ this.setState({personNum: e.target.value},this.calculatePrice);}}} min={this.props.constraints.min_group_size} max={this.props.constraints.max_group_size}/>
                  </div>
                </div>

                {/*<table className="table reservationTable">*/}
                  {/*<thead>*/}
                    {/*<tr>*/}
                      {/*<th>خدمات</th>*/}
                      {/*<th>هزینه</th>*/}
                      {/*<th>وضعیت انتخاب</th>*/}
                    {/*</tr>*/}
                  {/*</thead>*/}

                  {/*<tbody>*/}
                    {/*{reservationTable}*/}
                  {/*</tbody>*/}
                {/*</table>*/}

                <p className="calculatedPrice">هزینه رزرو تجربه برای {this.state.personNum} نفر: {this.state.calculatedPrice} تومان</p>

                <div className="reservationDetailsButtonsDiv">
                  <button disabled={!this.validateReserveForm()} style={{display: (this.state.loggedin? 'none':'block')}} data-dismiss="modal" data-toggle="modal" data-target="#signUpLogIn" className="btn btn-success reservationDetailsButton" id="proceedReserveButton">ادامه</button>
                  <button disabled={!this.validateReserveForm()} style={{display: (this.state.loggedin? 'block':'none')}} className="btn btn-success reservationDetailsButton" id="proceedReserveButton" onClick={(e)=> {this.proceedReservation()}}>ادامه</button>
                  <button data-dismiss="modal" className="btn btn-danger reservationDetailsButton" id="cancelReserveButton">انصراف</button>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default ReservationModal;