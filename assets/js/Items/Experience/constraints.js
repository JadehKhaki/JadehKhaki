import React from 'react';

class Constraints extends React.Component{
    constructor(props) {
        super(props);
    }
    render(){
      var groupSize = "";
      if (this.props.constraints.min_group_size !== null)
        groupSize += " از " + this.props.constraints.min_group_size;
      if (this.props.constraints.max_group_size !== null)
        groupSize += " تا " + this.props.constraints.max_group_size;

      if (this.props.constraints.min_group_size === this.props.constraints.max_group_size) 
        groupSize = this.props.constraints.max_group_size;

      var displayGroupSize = "none";
      if (this.props.constraints.min_group_size !== null || this.props.constraints.max_group_size !== null)
        displayGroupSize = "inherit";

      var gender="";
      if (this.props.constraints.gender=="MA")
        gender = "آقایان";
      else if (this.props.constraints.gender=="FE")
        gender = "بانوان";
      else
        gender = "آقایان و بانوان"


      var age = "";
      if (this.props.constraints.min_age !== null)
        age += " از " + this.props.constraints.min_age;
      if (this.props.constraints.max_age !== null)
        age += " تا " + this.props.constraints.max_age;

      var displayAge = "none";
      if (this.props.constraints.min_age !== null || this.props.constraints.max_age !== null)
        displayAge = "inherit";

      return (
        <div className="itemSummary">
          <h3>شرایط</h3>
          <h4 className="clock">مدت زمان: {this.props.duration}</h4>
          <h4 className="users" style={{display: displayGroupSize}}>ظرفیت: {groupSize} نفر</h4>
          <p>ویژه {gender}</p>
          <p style={{display: displayAge}}>رده سنی {age} سال</p>
          <p>{this.props.constraints.other_constraints}</p>
        </div>
      );
    }
}

export default Constraints;

