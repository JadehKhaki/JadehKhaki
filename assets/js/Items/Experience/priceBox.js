import React from 'react';
import {slicePrice} from '../../helper_functions'


class PriceBox extends React.Component{
    constructor(props) {
        super(props);
    }


    render(){
      var cancellation="-";
      if (this.props.cancellation!=="")
        cancellation=this.props.cancellation;

      var servicesTable = "-";
      if (this.props.services.length>0){
        var servicesTableRows = [];
        for (var i=0; i<this.props.services.length; i++){
          var price="";
          if (this.props.services[i].prices[0].cost.toString().length>3)
            price=slicePrice(this.props.services[i].prices[0].cost.toString());
          else
            price=this.props.services[i].prices[0];
          var row=(
            <tr key={this.props.services[i].uuid}>
              <th scope="row">{this.props.services[i].name}:</th>
              <td>{price} تومان</td>
            </tr> );
          servicesTableRows.push(row);
        }

        servicesTable=(
          <table className="table servicesTable table-borderless">
            <tbody>
              {servicesTableRows}
            </tbody>
          </table>
        );
      }

      var priceTable="";

      if (this.props.base_prices.length>0){
        var priceTableRows=[];
        for(var i=0; i< this.props.base_prices.length; i++){
          var price="";
          if (this.props.base_prices[i].cost.toString().length>3)
            price=slicePrice(this.props.base_prices[i].cost.toString());
          else
            price=this.props.base_prices[i].cost;
          var row=(
            <tr key={this.props.base_prices[i].uuid}>
              <th scope="row">{this.props.base_prices[i].min_count} - {this.props.base_prices[i].max_count}</th>
              <td>{price} تومان</td>
            </tr> );
          priceTableRows.push(row);
        }

        priceTable=(
          <table className="table table-bordered">
            <thead>
              <tr>
                <th scope="col">تعداد نفرات</th>
                <th scope="col">هزینه به ازای هر نفر</th>
              </tr>
            </thead>
            <tbody>
              {priceTableRows}
            </tbody>
          </table>
        );

      }

      return (
        <div id="priceBox" className="itemSummary">
          <h3 className="dollar">قیمت</h3>
          {priceTable}
          <br />

          {/*<h4 className="servicesTableTitle">خدمات اضافه (هزینه به ازای هر نفر)</h4>*/}
          {/*{servicesTable}*/}

          <h4>شرایط کنسلی</h4>
          <p>{cancellation}</p>
        </div>
      );
    }
}

export default PriceBox;
