import React, { Suspense, lazy} from 'react';
import ax from '../../ax';

const NotFound = lazy(() => import('../../errors/notFound' /* webpackChunkName: "expNotFound" */));
//import NotFound from '../../errors/notFound'
const ReservationModal = lazy(() => import('./resModal'  /* webpackChunkName: "expResModal" */));
//import ReservationModal from './resModal';
import SimpleSlider from '../slider';
import HostInfo from '../HostInfo';
import ReservationBox from '../reservationBox';
import PriceBox from './priceBox';
import Explanation from './explanation';
import AdditionalNotes from './notes';
import Constraints from './constraints';


class ExperienceItem extends React.Component{
    constructor(props) {
        super(props);
        this.state={data: [], gallery:[], loaded:false, notFound:false, stickyClass:""};
        this.scrollToReserve=this.scrollToReserve.bind(this);
        this.scrollToPriceBox=this.scrollToPriceBox.bind(this);
        this.onScroll=this.onScroll.bind(this);
    }

    loadItemFromServer() {
        ax.get('api/experiences/'+this.props.match.params.id)
            .then(function(response){
                this.setState({data:response.data, loaded:true, gallery:response.data.gallery});
            }.bind(this)).catch(function (error){
                this.setState({ notFound: true });
            }.bind(this));
    }

    componentWillMount() {
        this.loadItemFromServer();
        window.removeEventListener("scroll", this.onScroll, false);
    }

    componentDidMount() {
        window.addEventListener("scroll", this.onScroll, false);
    }

    onScroll() {
      var topSpace = 100 + 36 * document.documentElement.clientWidth / 100;
      if (window.scrollY >= topSpace ) {
          this.setState({stickyClass: "sticky"});
      } else if (window.scrollY < topSpace) {
          this.setState({stickyClass: ""});
      }
    }

    scrollToReserve(){
      var element = document.getElementById("smallScreenReservationBox");
      element.scrollIntoView({behavior: "smooth"});
    }

    scrollToPriceBox(){
        var element = document.getElementById("priceBox");
        element.scrollIntoView({behavior: "smooth"});
    }

    render(){
      if (this.state.notFound) {
        return (<NotFound />);
      }

      if(this.state.loaded) {
        var address = this.state.data.address.province+"، "+this.state.data.address.city;
        if (this.state.data.address.town)
          address+="، "+this.state.data.address.town;

            return (
            <div>
            	<div className="categoryList-item">
            		<div className="row" dir="ltr">
                  <SimpleSlider items={this.state.data.gallery}/>
                </div>

                <div className="row">
                  <div className="col-md-5"></div>
                  <div className="col-md-6">
                    <header>
                      <h1 className="itemHeader"> {this.state.data.title} </h1>
                      <p className="itemDescription"> {this.state.data.tiny_explanation}</p>
                    </header>
                  </div>
                  <div className="col-md-1"></div>
                </div>


                <div className="row mainContent">
                  <div className="col-md-1"></div>

                  <div id="normalScreenReservationBox" className="col-md-4">
                    <div id="reservationBoxDiv" className={this.state.stickyClass}>
                      <ReservationBox />
                    </div>
                  </div>

                  <div className="col-md-6">
                    <ItemDescription data={this.state.data}
                                     hosts={this.state.data.hosts}
                                     address={address}
                                     itemExplanation={this.state.data.explanation}
                                     constraints={this.state.data.constraints}
                                     services={this.state.data.services}
                    />
                  </div>

                  <div id="smallScreenReservationBox" className="col-md-4">
                    <ReservationBox />
                  </div>

                  <div className="col-md-1"></div>
                </div>
                    <div id="fixedBottomDiv">
                        <button type="button" id="reserveButton" className="bottomButton btn btn-danger" onClick={this.scrollToReserve}>رزرو</button>
                        <button type="button" id="priceButton" className="bottomButton btn btn-danger" onClick={this.scrollToPriceBox}>جدول قیمت</button>
                    </div>

                <div id="fixedBottomDiv">
                  <button type="button" id="reserveButton" className="bottomButton btn btn-danger" onClick={this.scrollToReserve}>رزرو</button>
                  <button type="button" id="priceButton" className="bottomButton btn btn-danger" onClick={this.scrollToPriceBox}>جدول قیمت</button>
                </div>

                <Suspense fallback={<div>Loading...</div>}>
                  <ReservationModal uuid={this.state.data.uuid} url_title={this.state.data.url_title} image={this.state.data.main_image.picture} basePrices={this.state.data.base_prices} options={this.state.data.services} constraints={this.state.data.constraints} />
                </Suspense>
                

              </div>
            </div>
            );
          }
          return(<div>Data couldn't load</div>);
    }
}

class ItemDescription extends React.Component{
    constructor(props) {
        super(props);
    }

    render(){
          var hosts=[];

          for (var i = 0; i < this.props.data.hosts.length; i++) {
            var gender="";
            if (this.props.data.hosts[i].gender=="MA")
              gender = "جناب آقای ";
            else if (this.props.data.hosts[i].gender=="FE")
              gender = "سرکار خانم ";

            var hostName=gender+this.props.data.hosts[i].first_name+" "+this.props.data.hosts[i].last_name;

            var host= (
              <div key={hostName} style={{"textAlign": "center"}}>
              <img className="hostAvatar" src={this.props.data.hosts[i].photo.picture} alt="Avatar" />
              <h4>{hostName}</h4>
              </div>
              );
            hosts.push(host);
          }
            return (
              <div>
                <div className="row" id="itemDescriptionRow">
                  <div className="single-action-buttons">
                      <h4 className="address">{this.props.address}</h4>
                  </div>
                    <Explanation itemExplanation={this.props.itemExplanation}/>
                    <Constraints constraints={this.props.constraints} duration={this.props.data.duration} />
                    
                    <PriceBox base_prices={this.props.data.base_prices} services={this.props.data.services} cancellation={this.props.data.cancellation}/>

                    <AdditionalNotes host_equipments={this.props.data.host_equipments} guest_equipments={this.props.data.guest_equipments} note={this.props.data.note}/>
                    <HostInfo person={this.props.hosts[0]}/>

                </div>
            </div>
            
            );
    }
}


export default ExperienceItem;