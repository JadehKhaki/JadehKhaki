import React from 'react';

class Explanation extends React.Component{
    constructor(props) {
        super(props);
    }
    render(){
        var itemExplanation="-";
        if (this.props.itemExplanation!=="")
            itemExplanation=this.props.itemExplanation;
        return (
            <div className="itemSummary">
                {itemExplanation.split('\n').map(function(item, key) {
                    return (
                        <p key={key}>
                            {item}
                            <br/>
                        </p>
                    )
                })}
            </div>
        );
    }
}

export default Explanation;
