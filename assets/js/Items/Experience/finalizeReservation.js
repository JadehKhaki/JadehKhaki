import React from 'react';
import ax from '../../ax';
import {Link} from 'react-router-dom';
import UserInfoForm from '../userInfoForm';

import { reserveInfoStore } from '../reserveInfoStore/store';

class FinalizeExpReservation extends React.Component{
	constructor(props) {
    super(props);

    this.state = {completedProfileInfo: false, checkedProfileInfo: false, reserved: false, reserved_display: 'none'};

    //this.checkProfileInfo = this.checkProfileInfo.bind(this);
    this.finalizeReservation = this.finalizeReservation.bind(this);
    this.proceed = this.proceed.bind(this);
  }

  finalizeReservation() {
    ax.post('/api/create_experience_reservation/',{
            check_in: reserveInfoStore.getState()['date'],
            count: reserveInfoStore.getState()['personNum'],
            experience_uuid: reserveInfoStore.getState()['experience']
        })
        .then(function(response){
            this.setState({reserved: true});
            this.setState({reserved_display:'block'});
        }.bind(this))
  }

// check_in, count, experience
  /*checkProfileInfo(){
    ax.get('api/profile/')
      .then(function(response){
          this.setState({
            completedProfileInfo:true
          });
      }.bind(this)).catch(function (error){
          this.setState({completedProfileInfo: false});
      }.bind(this));
  }

  componentWillMount (){
    this.checkProfileInfo();
  }*/

  proceed(){
    this.setState({completedProfileInfo: true, checkedProfileInfo: true});
  }

  render(){

    var reservationQueue = "";

    if (!this.state.checkedProfileInfo){
      reservationQueue = (
        <div>
          <UserInfoForm onSubmit={this.proceed} />
        </div>
      );
    }

    else{
      reservationQueue = (
        <div>
          <header style={{textAlign:"center"}}>

          <img className="finalize-reservation-image" src={reserveInfoStore.getState()['pic']}/>
          <br/><br/>

            <h1>تأیید جزئیات رزرو</h1>
          </header>
          <ExperienceReservationDetails />
          <div className="row">
            <div className="col-sm-4"></div>
            <Link disabled={this.state.reserved} className="col-sm-2 btn btn-danger finalize-reservation-button" to={'/' + reserveInfoStore.getState()['category'] + '/' + reserveInfoStore.getState()['url_title']}>انصراف</Link>
            <button disabled={!this.state.completedProfileInfo || this.state.reserved} className="col-sm-2 btn btn-success finalize-reservation-button" onClick={(e)=> {this.finalizeReservation()}}>تأیید</button>
            <div className="col-sm-4"></div>
          </div>
          <p style={{display:this.state.reserved_display}}>درخواست رزرو شما با موفقیت ثبت شد. لطفا منتظر انجام هماهنگی و تایید از سمت کارشناسان ما باشید.</p>

        </div>
      );
    }

    
    return (
      <div style={{marginTop:"5em"}} className="row">
        <div className="col-md-1"></div>
        <div id="itemDescriptionRow" className="col-md-10">
          <ReservationQueue checkedProfileInfo={this.state.checkedProfileInfo} />
          {reservationQueue}
        </div>
        <div className="col-md-1"></div>
      </div>
    );
  
  }
}


class ExperienceReservationDetails extends React.Component{
   
  constructor(props){
    super(props);

    this.state = {
      title: "",
      options: [],
      loaded: false
    };

    this.loadOptionsFromServer = this.loadOptionsFromServer.bind(this);
  }

  loadOptionsFromServer(){
    ax.get('/api/experiences/' + reserveInfoStore.getState()['url_title'])
      .then(function(response){
          this.setState({
            title: response.data.title,
            options: response.data.services,
            loaded: true
          });
      }.bind(this));
  }

  componentDidMount(){
    this.loadOptionsFromServer();
  }

  render(){
    if (this.state.loaded){
      var options = JSON.parse('[' + reserveInfoStore.getState()['options'] + ']');

      var reservationTable="";
              
      if (!this.state.options || this.state.options.length>0){
        var reservationTable=[];


        for(var i=0; i< this.state.options.length; i++){
          var price="";
          if (this.state.options[i].price.toString().length>3)
            price=this.state.options[i].price;
          else
            price=this.state.options[i].price;

          var row=(
          <tr key={i}>
            <td>{this.state.options[i].title}</td>
            <td>{price}</td>
            <td className={(options[i]? 'checkedOptionDetail' : 'uncheckedOptionDetail')}></td>
          </tr> 
        );

          reservationTable.push(row);
        }
      }

      return (
        <div>
          <h3>رزرو تجربه: {this.state.title}</h3>

          <h4>تعداد نفرات: {reserveInfoStore.getState()['personNum']}</h4>

          <h4>تاریخ: {reserveInfoStore.getState()['date']}</h4>

          {/*<table className="table reservationTable">*/}
            {/*<thead>*/}
              {/*<tr>*/}
                {/*<th>خدمات</th>*/}
                {/*<th>هزینه</th>*/}
                {/*<th>وضعیت انتخاب</th>*/}
              {/*</tr>*/}
            {/*</thead>*/}

            {/*<tbody>*/}
              {/*{reservationTable}*/}
            {/*</tbody>*/}
          {/*</table>*/}

          <h3 className="finalize-reservation-price">هزینه نهایی: {reserveInfoStore.getState()['price']} تومان</h3>

        </div>
      );
    }

    else
      return (<div></div>);
  }
}

class ReservationQueue extends React.Component{
  render(){
    return(
      <ul id="reservationQueueBreadcrumb">
        <li id="reservationQueueBreadcrumbLi" style={{color: this.props.checkedProfileInfo?'#a59f9f':'#444'}}>تأیید اطلاعات پروفایل</li>
        <li id="reservationQueueBreadcrumbLi"><i class="fas fa-angle-left"></i></li>
        <li id="reservationQueueBreadcrumbLi" style={{color: !this.props.checkedProfileInfo?'#a59f9f':'#444'}}>تأیید اطلاعات رزرو</li>
      </ul>
    );
  }
}

export default FinalizeExpReservation;