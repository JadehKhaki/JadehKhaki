import React from 'react';

class AdditionalNotes extends React.Component{
    constructor(props) {
        super(props);
    }
    render(){
      var host_equipments="-"
      if (this.props.host_equipments!=="")
        host_equipments=this.props.host_equipments;

      var guest_equipments="-"
      if (this.props.guest_equipments!=="")
        guest_equipments=this.props.guest_equipments;

      var note="-"
      if (this.props.note!=="")
        note=this.props.note;
      return (
        <div>
        <div className="itemSummary">
          <h4>امکانات میزبان</h4>
          <p>{host_equipments}</p>
          <br />
          <h4>وسایل مورد نیاز گردشگر</h4>
          <p>{guest_equipments}</p>
          <br />
        </div>
        <div className="itemSummary">
          <h3>توضیحات بیشتر</h3>
          <p>{note}</p>
        </div>
        </div>
      );
    }
}

export default AdditionalNotes;
