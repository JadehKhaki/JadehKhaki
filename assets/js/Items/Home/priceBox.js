import React from 'react';
import {slicePrice} from '../../helper_functions'


class PriceBox extends React.Component{
    constructor(props) {
        super(props);
        // this.slicePrice=this.slicePrice.bind(this);
    }

    render(){
      var cancellation=this.props.cancellation;
      var show_cancellation = 'none';
      var show_rooms = 'none';
      var show_total = 'none';
      if (this.props.rooms.length)
        show_rooms = 'block';

      if (this.props.cancellation!=="")
        show_cancellation = 'block';

      if (show_cancellation=='block' || show_rooms == 'block')
        show_total = 'block';

      var roomPriceTable="";

      if (this.props.rooms.length>0){
        var roomPriceTableRows=[];
        for(var i=0; i< this.props.rooms.length; i++){

            var ordinary_price,norouz_price,peak_price="";
            ordinary_price=slicePrice(this.props.rooms[i].room_cost.ordinary_price.toString());
            norouz_price=slicePrice(this.props.rooms[i].room_cost.norouz_price.toString());
            peak_price=slicePrice(this.props.rooms[i].room_cost.peak_price.toString());

          var row=(

            <tr key={i}>
              {/*<th scope="row">{this.props.rooms[i].name}</th>*/}
              <td scope="row"><b>{this.props.rooms[i].name}</b></td>
              <td>{this.props.rooms[i].max_capacity}</td>
              <td><button type="button" className="btn btn-primary" data-toggle="modal" data-target={"#myModal"+i}>توضیحات</button>
              <div className="modal fade" id={"myModal"+i} role="dialog">
              <div className="modal-dialog">
                <div className="modal-content">
                  <div className="modal-body">
                    <button style={{paddingLeft:"10px"}} type="button" className="close" data-dismiss="modal">&times;</button>
                    {/* <p style={{color:"red"}}>زمان اوج: {this.props.rooms[i].room_cost.peak_time}</p>
                    <table className="table">
                      <tbody>
                        <tr>
                          <th scope="row">هزینه‌ی رزرو اتاق هر شب</th>
                          <td>{ordinary_price} تومان</td>
                        </tr>
                        <tr>
                          <th scope="row">هزینه‌ی رزرو اتاق هر شب در ایام نوروز</th>
                          <td>{norouz_price} تومان</td>
                        </tr>
                        <tr>
                          <th scope="row">هزینه‌ی رزرو اتاق هر شب در زمان اوج</th>
                          <td>{peak_price} تومان</td>
                        </tr>
                      </tbody>
                    </table> */}

                    <p style={{display: this.props.rooms[i].person_cost.ordinary_price==0? 'inherit' : 'none'}}>هزینه‌ی اتاق برابر است با {slicePrice(this.props.rooms[i].room_cost.ordinary_price.toString())} تومان.</p>
                    <p style={{display: (this.props.rooms[i].person_cost.ordinary_price!=0 && this.props.rooms[i].min_capacity>0)? 'inherit' : 'none'}}>هزینه‌ی اتاق برای تعداد نفرات تا {this.props.rooms[i].min_capacity} نفر {slicePrice(this.props.rooms[i].room_cost.ordinary_price.toString())} تومان است. در صورت بیشتر شدن تعداد، به ازای هر نفر مقدار {slicePrice(this.props.rooms[i].person_cost.ordinary_price.toString())} تومان به قیمت اتاق اضافه می‌شود.</p>
                    <p style={{display: (this.props.rooms[i].person_cost.ordinary_price!=0 && this.props.rooms[i].min_capacity==0)? 'inherit' : 'none'}}>هزینه‌ی اتاق به ازای هر نفر {slicePrice(this.props.rooms[i].person_cost.ordinary_price.toString())} تومان است.</p>

                  </div>
                </div>
              </div>
            </div>
            </td>






            </tr>



            );
          roomPriceTableRows.push(row);
        }

        roomPriceTable=(
          <table className="table table-bordered">
            <thead>
              <tr>
                <th scope="col">نوع اتاق</th>
                <th scope="col">ظرفیت</th>
                <th scope="col"> هزینه</th>

              </tr>
            </thead>
            <tbody>
              {roomPriceTableRows}
            </tbody>
          </table>
        );

      }

      return (
        <div id="priceBox" className="itemSummary" style={{display: show_total}}>
              {/*<h4 className="dollar">شروع قیمت از {this.props.basePrice} تومان</h4>*/}
              {roomPriceTable}
              <br />
              <div style={{display:show_cancellation}}>
                  <h4>شرایط کنسلی</h4>
                  <p>{cancellation}</p>
              </div>
          </div>
      );
    }
}

export default PriceBox;
