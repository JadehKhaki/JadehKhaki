import React from 'react';

class ItemServices extends React.Component{
  constructor(props){
    super(props);
  }

  render(){
     var services=[];
      for (var i in this.props.itemServices) {
        if(this.props.itemServices[i]==true){
        if (i=="addition")
          continue;
        if(i=="cooler")
          var name="سیستم سرمایشی"
        else if(i=="iron")
          var name="اتو"
        else if(i=="washing_machine")
          var name="لباس شویی"
        else if(i=="heater")
          var name="سیستم گرمایشی"
        else if(i=="water")
          var name="آب"
        else if(i=="shampoo")
          var name="شامپو"
        else if(i=="breakfast")
          var name="صبحانه"
        else if(i=="public_bath")
          var name="حمام"
        else if(i=="tv")
          var name="تلویزیون"
        else if(i=="parking")
          var name="پارکینگ"
        else if(i=="credit_card")
          var name="کارتخوان"
        else if(i=="internet")
          var name="اینترنت"
        else if(i=="public_wc")
          var name="سرویس بهداشتی"
        else if(i=="kitchen")
          var name="آشپزخانه"
        else if(i=="electricity")
          var name="برق"
        else if(i=="bed")
          var name="سرویس خواب"
          var service = (
             <li key={i} id="supplyLI" className={i}>{name}</li>

            );
            if(i!=Object.keys(this.props.itemServices)[0])
              services.push(service);
          }
      }

    if (services.length>0){
      return(
        <div className="itemSummary">
          <h3>خدمات</h3>
          <ul id="suppliesList">
            {services}
          </ul>
        </div>
      );
    }
    else{
      return <div></div>;
    }

  }
}

export default ItemServices;
