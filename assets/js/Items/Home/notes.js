import React from 'react';

class AdditionalNotes extends React.Component{
    constructor(props) {
        super(props);
    }
    render(){
      var food_menu={breakfast:'',lunch:'',dinner:''};
      var show_food_menu = {total: 'none', breakfast:'none',lunch:'none',dinner:'none'}
      if(this.props.food_menu) {
          if (this.props.food_menu.breakfast !== '') {
              show_food_menu.total = 'block';
              show_food_menu.breakfast = 'block';
              food_menu.breakfast=this.props.food_menu.breakfast;
          }
          if (this.props.food_menu.lunch !== '') {
              show_food_menu.total = 'block';
              show_food_menu.lunch = 'block';
              food_menu.lunch=this.props.food_menu.lunch;
          }
          if (this.props.food_menu.dinner !== '') {
              show_food_menu.total = 'block';
              show_food_menu.dinner = 'block';
              food_menu.dinner=this.props.food_menu.dinner;
          }
      }

      var additional_notes='';
      var show_additional_notes = 'none';
      if (this.props.additional_notes!=='') {
          additional_notes = this.props.additional_notes;
          show_additional_notes = 'block';
      }
      return (
        <div>
        <div className="itemSummary">
            <div style={{display:show_food_menu.total}}>
                <h3>منوی غذایی</h3>
                <p style={{display:show_food_menu.breakfast}}>صبحانه: {food_menu.breakfast}</p>
                <p style={{display:show_food_menu.lunch}}>ناهار: {food_menu.lunch}</p>
                <p style={{display:show_food_menu.dinner}}>شام: {food_menu.dinner}</p>
                <br />
            </div>
        </div>
        <div className="itemSummary">
            <div style={{display:show_additional_notes}}>
                <h3>توضیحات بیشتر</h3>
                <p>{additional_notes}</p>
            </div>
        </div>
        </div>
      );
    }
}

export default AdditionalNotes;
