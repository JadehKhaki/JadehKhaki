import React from 'react';
import ax from '../../ax';
import SignUp from '../../signUp';
import {Redirect } from 'react-router';
import moment from 'jalali-moment';
import {DatePicker} from "react-advance-jalaali-datepicker";
import {slicePrice} from '../../helper_functions'

import { reserveInfoStore } from '../reserveInfoStore/store';
import { addItem } from '../reserveInfoStore/actions';

class ReservationModal extends React.Component {
  constructor(props) {
    super(props);
    var today = moment().format().substring(0, 10).replace(/-/g, '/');
    var tomorrow = moment(new Date()).add(1, 'days').format().substring(0, 10).replace(/-/g, '/');
    ;

    today = moment(today, 'YYYY-MM-DD').locale('fa').format('YYYY-MM-DD');
    tomorrow = moment(tomorrow, 'YYYY-MM-DD').locale('fa').format('YYYY-MM-DD');
    this.state = {
      checkInDate: today,
      checkOutDate: tomorrow,
      inputVal: new Array(this.props.rooms.length + 1).join(0).split('').map(parseFloat),
      nightNum: 1,
      personNum: 0,
      calculatedPrice: 0,
      loggedin: false,
      proceed: false,
      proceedTarget: "/reserve_home",
      selectedRoom: 0
    };

    this.changeCheckInDate = this.changeCheckInDate.bind(this);
    this.changeCheckOutDate = this.changeCheckOutDate.bind(this);
    this.calculateNights = this.calculateNights.bind(this);
    this.updatePersonNum = this.updatePersonNum.bind(this);
    this.calculatePrice = this.calculatePrice.bind(this);
    this.proceedReservation = this.proceedReservation.bind(this);
    this.saveReserveInfo = this.saveReserveInfo.bind(this);
    this.validateReserveForm = this.validateReserveForm.bind(this);
    this.handleRoomChange = this.handleRoomChange.bind(this);
    this.countChange = this.countChange.bind(this);
  }

  changeCheckInDate(unix, formatted){
    //console.log(unix); // returns timestamp of the selected value, for example.
    //console.log(formatted); // returns the selected value in the format you've entered, forexample, "تاریخ: 1396/02/24 ساعت: 18:30".
    var in_date = moment.from(formatted, 'fa', 'YYYY-MM-DD').format('YYYY-MM-DD');
    this.setState({checkInDate: in_date}, this.calculateNights);
  }

  changeCheckOutDate(unix, formatted){
    //console.log(unix); // returns timestamp of the selected value, for example.
    //console.log(formatted); // returns the selected value in the format you've entered, forexample, "تاریخ: 1396/02/24 ساعت: 18:30".
    var out_date = moment.from(formatted, 'fa', 'YYYY-MM-DD').format('YYYY-MM-DD');

    this.setState({checkOutDate: out_date}, this.calculateNights);
  }

  calculateNights() {
    var start = moment(this.state.checkInDate, 'YYYY-MM-DD');
    var end = moment(this.state.checkOutDate, 'YYYY-MM-DD');

    var duration = moment.duration(end.diff(start));
    var nights = Math.trunc(duration.asDays());

    this.setState({nightNum : nights}, this.calculatePrice);
  }

  updatePersonNum(){
    var num = 0;
    for (var i=0; i< this.props.rooms.length; i++){
      num += parseInt(this.state.inputVal[i], 10);
    }

    this.setState({personNum: num}, this.calculatePrice);
  }


  calculatePrice (){
    var price = 0;
    var i = this.state.selectedRoom;

    if (this.state.inputVal[i]>0)
      price = ((parseInt(this.props.rooms[i].min_capacity)===0? 0 : ((parseInt(this.state.inputVal[i])===0? 0:1) * parseInt(this.props.rooms[i].room_cost.ordinary_price,10))) + Math.max((parseInt(this.state.inputVal[i],10)) - this.props.rooms[i].min_capacity, 0) * parseInt(this.props.rooms[i].person_cost.ordinary_price,10)) * this.state.nightNum;

    this.setState({calculatedPrice: slicePrice(price.toString())});
  }

  checkLogin (){
    ax.get('/rest-auth/user/')
    .then(function(response){
        this.setState({loggedin: true});
    }.bind(this))
    .catch(function(error){
      if (error.response){
          this.setState({loggedin: false});
      }
    }.bind(this));
  }

  componentDidMount() {
    this.checkLogin();
    this.calculatePrice();
  }

  proceedReservation (){
    this.setState({proceed: true});
  }

  countChange(v, i) {
    var temp = this.state.inputVal; //new Array(this.props.rooms.length + 1).join('0').split('').map(parseFloat);
    temp[i] = v;

    this.setState({inputVal: temp},this.calculatePrice);
    this.setState({personNum: this.state.inputVal[i]});
    
    // var price = (parseInt(this.props.rooms[i].min_capacity)===0? 0 : ((parseInt(this.state.inputVal[i])===0? 0:1) * parseInt(this.props.rooms[i].room_cost.ordinary_price,10))) + Math.max((parseInt(this.state.inputVal[i],10)) - this.props.rooms[i].min_capacity, 0) * parseInt(this.props.rooms[i].person_cost.ordinary_price,10);
    // this.setState({calculatedPrice: slicePrice(price.toString())});

    // this.updatePersonNum();


  }


  saveReserveInfo(){
    //const unsubscribe = reserveInfoStore.subscribe(() => console.log(reserveInfoStore.getState()))

    reserveInfoStore.dispatch(addItem('category', 'home'));
    reserveInfoStore.dispatch(addItem('url_title', this.props.url_title));
    reserveInfoStore.dispatch(addItem('pic', this.props.image));
    reserveInfoStore.dispatch(addItem('checkInDate', this.state.checkInDate));
    reserveInfoStore.dispatch(addItem('checkOutDate', this.state.checkOutDate));
    reserveInfoStore.dispatch(addItem('nightNum', this.state.nightNum));
    reserveInfoStore.dispatch(addItem('inputVal', this.state.inputVal));
    reserveInfoStore.dispatch(addItem('personNum', this.state.personNum));
    reserveInfoStore.dispatch(addItem('price', this.state.calculatedPrice));
    reserveInfoStore.dispatch(addItem('room', this.props.rooms[this.state.selectedRoom].uuid));
    
    //unsubscribe()
  }

  validateReserveForm (){
    return (this.state.personNum > 0 && this.state.nightNum > 0);
  }

  handleRoomChange(changeEvent) {
    this.setState({
      selectedRoom: changeEvent.target.value
    }, this.countChange(this.state.inputVal[changeEvent.target.value],changeEvent.target.value));
}

  render() {

    if (this.state.proceed){
      this.saveReserveInfo();
      $('.modal').modal('hide');
      return(<Redirect to={this.state.proceedTarget}/>);
    }


    var reservationTable="";

    if (this.props.rooms.length>0){
      var reservationTable=[];

      for(var i=0; i< this.props.rooms.length; i++){
        let index = i;
        var color = '#EEEEEE';
        if(this.state.selectedRoom == index)
          color = 'white';

        var labelColor = '#ada9a9';
        if(this.state.selectedRoom == index)
          labelColor = '#f7767e';

        var row=(

          <tr bgcolor={color} key={this.props.rooms[index].uuid}>

            <td>
              <div><label className="roomRadioLabel" style={{color: labelColor}}>
                <input className="roomRadioButton" type="radio"
                       value={index}
                       checked={this.state.selectedRoom === index}
                       onChange={this.handleRoomChange}
                />
                {this.props.rooms[i].name}</label>
              </div>
            </td>
            <td>{this.props.rooms[i].max_capacity}</td>
            <td><input className="personNum" disabled={this.state.selectedRoom != i} type="number" value={this.state.inputVal[index]} onChange={(e) => {if(0 <= e.target.value && e.target.value <= this.props.rooms[index].max_capacity) { this.countChange(e.target.value, index);}}} min="0" max={this.props.rooms[index].max_capacity}/></td>
          </tr>
        );

        reservationTable.push(row);
      }
    }

    return(
      <div>
        <SignUp modal="signUpLogIn" proceedReservation={this.proceedReservation} />
        <div className="modal fade" id="reservationModal" role="dialog">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-body">
                <button style={{paddingLeft:"10px"}} type="button" className="close" data-dismiss="modal">&times;</button>

                <div className="datePicker">
                  <div className="datePickerItem">
                    <b>تاریخ ورود:</b>
                    <DatePicker placeholder="تاریخ ورود" format="jYYYY-jMM-jDD" onChange={this.changeCheckInDate} id="checkInDate" preSelected={this.state.checkInDate} />
                  </div>
                  <div className="datePickerItem">
                    <b>تاریخ خروج:</b>
                    <DatePicker placeholder="تاریخ خروج" format="jYYYY-jMM-jDD" onChange={this.changeCheckOutDate} id="checkOutDate" preSelected={this.state.checkOutDate} />
                  </div>
                </div>

                <form action="">

                  <table className="table reservationTable">
                    <thead>
                      <tr>
                        <th>اتاق</th>
                        <th>ظرفیت</th>
                        <th>تعداد نفرات</th>
                      </tr>
                    </thead>
                    <tbody>
                      {reservationTable}
                    </tbody>
                  </table>
                </form>


                <p className="calculatedPrice">هزینه اقامت در <b>{this.props.rooms[this.state.selectedRoom].name}</b> به مدت <b>{this.state.nightNum}</b> شب برای <b>{this.state.personNum}</b> نفر: <b>{this.state.calculatedPrice}</b> تومان</p>

                <div className="reservationDetailsButtonsDiv">
                  <button disabled={!this.validateReserveForm()} style={{display: (this.state.loggedin? 'none':'block')}} data-dismiss="modal" data-toggle="modal" data-target="#signUpLogIn" className="btn btn-success reservationDetailsButton" id="proceedReserveButton">ادامه</button>
                  <button disabled={!this.validateReserveForm()} style={{display: (this.state.loggedin? 'block':'none')}} className="btn btn-success reservationDetailsButton" id="proceedReserveButton" onClick={(e)=> {this.proceedReservation()}}>ادامه</button>
                  <button data-dismiss="modal" className="btn btn-danger reservationDetailsButton" id="cancelReserveButton">انصراف</button>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default ReservationModal;