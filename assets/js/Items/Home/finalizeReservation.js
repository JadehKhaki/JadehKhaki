import React from 'react';
import ax from '../../ax';
import {Link} from 'react-router-dom';
import UserInfoForm from '../userInfoForm';

import { reserveInfoStore } from '../reserveInfoStore/store';

class FinalizeHomeReservation extends React.Component{
	constructor(props) {
    super(props);

    this.state = {completedProfileInfo: false, checkedProfileInfo: false, reserved: false, reserved_display: 'none'};

    //this.checkProfileInfo = this.checkProfileInfo.bind(this);
    this.finalizeReservation = this.finalizeReservation.bind(this);
    this.proceed = this.proceed.bind(this);
  }

  finalizeReservation() {
    ax.post('/api/create_home_reservation/',{
            check_in: reserveInfoStore.getState()['checkInDate'],
            check_out: reserveInfoStore.getState()['checkOutDate'],
            count: reserveInfoStore.getState()['personNum'],
            room_uuid: reserveInfoStore.getState()['room']
        })
        .then(function(response){
            this.setState({reserved: true});
            this.setState({reserved_display:'block'});
        }.bind(this))
  }

  /*checkProfileInfo(){
    ax.get('api/profile/')
      .then(function(response){
          this.setState({
            completedProfileInfo:true
          });
      }.bind(this)).catch(function (error){
          this.setState({completedProfileInfo: false});
      }.bind(this));
  }

  componentWillMount (){
    this.checkProfileInfo();
  }*/

  proceed(){
    this.setState({completedProfileInfo: true, checkedProfileInfo: true});
  }

  render(){

    var reservationQueue = "";

    if (!this.state.checkedProfileInfo){
      reservationQueue = (
        <div>
          <UserInfoForm onSubmit={this.proceed} />
        </div>
      );
    }

    else{
      reservationQueue = (
        <div>
          <header style={{textAlign:"center"}}>

          <img className="finalize-reservation-image" src={reserveInfoStore.getState()['pic']}/>
          <br/><br/>

            <h1>تأیید جزئیات رزرو</h1>
          </header>
          <HomeReservationDetails />
          <div className="row">
            <div className="col-sm-4"></div>
            <Link disabled={this.state.reserved} className="col-sm-2 btn btn-danger finalize-reservation-button" to={'/' + reserveInfoStore.getState()['category'] + '/' + reserveInfoStore.getState()['url_title']}>انصراف</Link>
            <button disabled={!this.state.completedProfileInfo || this.state.reserved} className="col-sm-2 btn btn-success finalize-reservation-button" onClick={(e)=> {this.finalizeReservation()}}>تأیید</button>
            <div className="col-sm-4"></div>
          </div>
          <p style={{display:this.state.reserved_display}}>درخواست رزرو شما با موفقیت ثبت شد. لطفا منتظر انجام هماهنگی و تایید از سمت کارشناسان ما باشید.</p>

        </div>
      );
    }

    
    return (
      <div style={{marginTop:"5em"}} className="row">
        <div className="col-md-1"></div>
        <div id="itemDescriptionRow" className="col-md-10">
          <ReservationQueue checkedProfileInfo={this.state.checkedProfileInfo} />
          {reservationQueue}
        </div>
        <div className="col-md-1"></div>
      </div>
    );

  }
}

class HomeReservationDetails extends React.Component{
  
  constructor(props) {
    super(props);

    this.state = {
      title: "",
      rooms: [],
      loaded: false
    }

    this.loadRoomsFromServer = this.loadRoomsFromServer.bind(this);
  }

  loadRoomsFromServer(){
    ax.get('/api/homes/' + reserveInfoStore.getState()['url_title'])
      .then(function(response){
          this.setState({
            title: response.data.title,
            rooms: response.data.rooms,
            loaded: true
          });
      }.bind(this));
  }

  componentDidMount (){
    this.loadRoomsFromServer();
  } 

  render(){

    if (this.state.loaded){
      var inputVal = JSON.parse('[' + reserveInfoStore.getState()['inputVal'] + ']');

      var reservationTable="";
              
      if (this.state.rooms.length>0){
        var reservationTable=[];

        for(var i=0; i< this.state.rooms.length; i++){
          var row=(
            <tr key={this.state.rooms[i].uuid}>
              <td>{this.state.rooms[i].name}</td>
              <td>{this.state.rooms[i].max_capacity}</td>
              <td>{inputVal[i]}</td>
            </tr> 
          );

          reservationTable.push(row);
        }
      }

      return (
        <div>
          <h3>رزرو اقامتگاه: {this.state.title}</h3>

          <h4>تعداد نفرات: {reserveInfoStore.getState()['personNum']}</h4>

          <h4>تاریخ ورود: {reserveInfoStore.getState()['checkInDate']}</h4>
          <h4>تاریخ خروج: {reserveInfoStore.getState()['checkOutDate']}</h4>

          <table className="table reservationTable">
            <thead>
              <tr>
                <th>اتاق</th>
                <th>ظرفیت</th>
                <th>تعداد نفرات</th>
              </tr>
            </thead>

            <tbody>
              {reservationTable}
            </tbody>
          </table>

          <h3 className="finalize-reservation-price">هزینه نهایی: {reserveInfoStore.getState()['price']} تومان</h3>

        </div>
      );
    }

    else
      return (<div></div>);
  }
}

class ReservationQueue extends React.Component{
  render(){
    return(
      <ul id="reservationQueueBreadcrumb">
        <li id="reservationQueueBreadcrumbLi" style={{color: this.props.checkedProfileInfo?'#a59f9f':'#444'}}>تأیید اطلاعات پروفایل</li>
        <li id="reservationQueueBreadcrumbLi"><i class="fas fa-angle-left"></i></li>
        <li id="reservationQueueBreadcrumbLi" style={{color: !this.props.checkedProfileInfo?'#a59f9f':'#444'}}>تأیید اطلاعات رزرو</li>
      </ul>
    );
  }
}

export default FinalizeHomeReservation;