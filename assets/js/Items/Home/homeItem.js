import React, { Suspense, lazy} from 'react';
import ax from '../../ax';

const NotFound = lazy(() => import('../../errors/notFound' /* webpackChunkName: "homeNotFound" */));
//import NotFound from '../../errors/notFound'
const ReservationModal = lazy(() => import('./resModal'  /* webpackChunkName: "homeResModal" */));
//import ReservationModal from './resModal';
import ItemServices from './services';
import SimpleSlider from '../slider';
import HostInfo from '../HostInfo';
import ReservationBox from '../reservationBox';
import PriceBox from './priceBox';
import AdditionalNotes from './notes';


class HomeItem extends React.Component{
    constructor(props) {
        super(props);
        this.state={data: [], gallery:[], loaded:false, notFound:false, stickyClass:""};
        this.scrollToReserve=this.scrollToReserve.bind(this);
        this.scrollToPriceBox=this.scrollToPriceBox.bind(this);
        this.onScroll=this.onScroll.bind(this);
    }

    loadItemFromServer() {
        ax.get('api/homes/'+this.props.match.params.id)
            .then(function(response){
                this.setState({data:response.data, loaded:true, gallery:response.data.gallery});
            }.bind(this)).catch(function (error){
                this.setState({ notFound: true });
            }.bind(this));
    }

    componentWillMount() {
        this.loadItemFromServer();
        window.removeEventListener("scroll", this.onScroll, false);
    }

    componentDidMount() {
        window.addEventListener("scroll", this.onScroll, false);
    }

    onScroll() {
      var topSpace = 100 + 36 * document.documentElement.clientWidth / 100;
      if (window.scrollY >= topSpace ) {
          this.setState({stickyClass: "sticky"});
      } else if (window.scrollY < topSpace) {
          this.setState({stickyClass: ""});
      }
    }

    scrollToReserve(){
      var element = document.getElementById("smallScreenReservationBox");
      element.scrollIntoView({behavior: "smooth"});
    }

    scrollToPriceBox(){
        var element = document.getElementById("priceBox");
        element.scrollIntoView({behavior: "smooth"});
    }


    render(){
      if (this.state.notFound) {
        return (<NotFound />);
      }

      if(this.state.loaded) {        
        var address = this.state.data.address.province+"، "+this.state.data.address.city;
        if (this.state.data.address.town)
          address+="، "+this.state.data.address.town;
          return (
            <div>
            	<div className="categoryList-item">
            		<div className="row" dir="ltr">
                  <SimpleSlider items={this.state.data.gallery}/>
                </div>
                <div className="row">
                  <div className="col-md-5"></div>
                  <div className="col-md-6">
                    <header>
                      <h1 className="itemHeader"> {this.state.data.title} </h1>
                      <p className="itemDescription"> {this.state.data.tiny_explanation}</p>
                    </header>
                  </div>
                  <div className="col-md-1"></div>
                </div>


            		<div className="row mainContent">
  	            	<div className="col-md-1"></div>

  	            	<div id="normalScreenReservationBox" className="col-md-4">
                    <div id="reservationBoxDiv" className={this.state.stickyClass}>
                      <ReservationBox />
                    </div>
                  </div>

        					<div className="col-md-6">
                    <ItemDescription rooms={this.state.data.rooms} cancellation={this.state.data.cancellation} itemExplanation={this.state.data.explanation} itemServices={this.state.data.supplies} person={this.state.data.person} address={address} food_menu={this.state.data.food_menu} additional_notes={this.state.data.additional_notes} />
        					</div>

                  <div id="smallScreenReservationBox" className="col-md-4">
                    <ReservationBox />
                  </div>

  					      <div className="col-md-1"></div>
					      </div>

                <div id="fixedBottomDiv">
                  <button type="button" id="reserveButton" className="bottomButton btn btn-danger" onClick={this.scrollToReserve}>رزرو</button>
                  <button type="button" id="priceButton" className="bottomButton btn btn-danger" onClick={this.scrollToPriceBox}>جدول قیمت</button>
                </div>

                <Suspense fallback={<div>Loading...</div>}>
                  <ReservationModal image={this.state.data.main_image.picture} url_title={this.state.data.url_title} rooms={this.state.data.rooms} />
                </Suspense>

              </div>
            </div>
            );
          }
          return(<div>Data couldn't load</div>);
    }
}

// props: rooms, itemExplanation, itemServices, person, address,
// food_menu, additional_notes
class ItemDescription extends React.Component{
    constructor(props) {
        super(props);
    }

    render(){

        return (
          <div>
              <div className="row" id="itemDescriptionRow">
                <h4 className="address">{this.props.address}</h4>
                    <br/>

                <div className="itemSummary">
                    {/*<p style={{"textAlign": "justify"}}> {this.props.itemExplanation}</p>*/}
                    {this.props.itemExplanation.split('\n').map(function(item, key) {
                        return (
                            <p key={key}>
                                {item}
                                <br/>
                            </p>
                        )
                    })}
                </div>
                
                <ItemServices itemServices={this.props.itemServices} />
                <PriceBox rooms={this.props.rooms} cancellation={this.props.cancellation}/>
                <AdditionalNotes food_menu={this.props.food_menu} additional_notes={this.props.additional_notes}/>
                <HostInfo person={this.props.person}/>
              </div>
          </div>
        );
    }
}


export default HomeItem;