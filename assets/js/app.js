import React, {Suspense, lazy} from 'react'

import Main from './main'
const Header = lazy(() => import('./headers/header'  /* webpackChunkName: "headers" */));
//import Header from './headers/header'
const Footer = lazy(() => import('./footers/footer'  /* webpackChunkName: "footers" */));
//import Footer from './footers/footer'


class App extends React.Component{
    render(){
        return (
            <div>
                <Suspense fallback={<div>Loading...</div>}>
                    <Header />
                    <div className="mainBody">
    	                <Main />
    	            </div>
                    <Footer />
                </Suspense>
            </div>
        );
    }
}

export default App;