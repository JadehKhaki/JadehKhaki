const ax = axios.create({
    baseURL: 'http://localhost:8000/'
});

ax.defaults.xsrfCookieName = 'csrftoken'
ax.defaults.xsrfHeaderName = 'X-CSRFToken'
export default ax;
