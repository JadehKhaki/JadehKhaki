import React from 'react';
import ProgressiveImage from "react-progressive-image-loading";


class Card extends React.Component{

    render(){
        return (
            <div>
            <ProgressiveImage
			    preview={this.props.src}
			    src={this.props.src}
			    render={(src, style) => <img src={src} style={style} />}
			/>

			 {/*small photo = preview*/}
                
            </div>
        );
    }
}

export default Card;
