import React from 'react';

import CategoryList from './categoryList';
import SearchBar from '../search/search';


class ExperienceList extends React.Component{

    render(){
        return (
            <div className="categoryList-wrap">
                <div className="row category-search-div">
                    <div className="col-sm-1"></div>
                    <div className="col-sm-10">
                        <SearchBar searchURL="_experiences" />
                    </div>
                    <div className="col-sm-1"></div>
                </div>

                <ul className="categoryList">

                    <CategoryList url="/api/experiences/" />

                </ul>
            </div>
        );
    }
}

export default ExperienceList;