import React from 'react';
import {Link} from 'react-router-dom';
import {slicePrice} from '../helper_functions';

class TopListGalleryCard extends React.Component{

    render(){
        return (
            <Link className="toplist-card" to={this.props.url}>
                <div className="toplist-card-div">
                    <img className="toplist-card-image" src={this.props.src}/>
                    <div className="toplist-card-info">
                        <div className="toplist-card-title">{this.props.title}</div>
                        <div className="toplist-card-explanation">از <b className="cardPrice">{slicePrice(this.props.cost)}</b> تومان</div>
                        <div className="toplist-card-location">
                            <i className="fa fa-map-marker-alt" aria-hidden="true"></i>
                            <p>{this.props.location}</p>
                        </div>
                    </div>
                </div>
            </Link>
        );
    }
}

export default TopListGalleryCard;