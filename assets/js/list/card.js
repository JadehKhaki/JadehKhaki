import React from 'react';
import {Link} from 'react-router-dom';
import {slicePrice} from '../helper_functions';

class Card extends React.Component{

    render(){
        return (
            <Link className="category-card" to={this.props.url}>
                <div className="category-card-div">
                    <img className="category-card-image" src={this.props.src}/>
                    <div className="category-card-info">
                        <div className="category-card-title">{this.props.title}</div>
                        <div className="category-card-explanation">از <b className="cardPrice">{slicePrice(this.props.cost)}</b> تومان</div>
                        <div className="category-card-location">
                            <i className="fa fa-map-marker-alt" aria-hidden="true"></i>
                            <p>{this.props.location}</p>
                        </div>
                    </div>
                </div>
            </Link>
        );
    }
}

export default Card;