import React from 'react';
import ax from '../ax';
import Card from './card';
import LoadingElement from '../loadingElement';
import NoResult from '../errors/noResult';


class CategoryList extends React.Component{
    constructor(props) {
        super(props);
        this.state = {data: [], loaded:false};
    }

    loadGalleryFromServer() {
        ax.get(this.props.url)
            .then(function(response){
                var data = this.state.data;
                data.push.apply(data,response.data);
                data.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));
                this.setState({data: data, loaded:true});
            }.bind(this));

        if (this.props.url2)
            ax.get(this.props.url2)
            .then(function(response){
                var data = this.state.data;
                data.push.apply(data,response.data);
                data.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));
                this.setState({data: data});
            }.bind(this));
    }

    componentDidMount() {
        this.loadGalleryFromServer();
    }

    render() {
        var rowList = [];

        if (!this.state.loaded)
            return (<div className="searchLoadingElement"><LoadingElement size='110'/></div>);

        else if (this.state.data.length === 0)
            return (<NoResult />);

        else if (this.state.data) {
            var galleryData = this.state.data;
            for (var i = 0; i < galleryData.length; i++) {
                var address = galleryData[i].address.province+"، "+galleryData[i].address.city;
                if (galleryData[i].address.town)
                    address+="، "+galleryData[i].address.town;

                var card = (<div key={'galleryCard'+i} className="category-gallery-card">
                    <Card src={galleryData[i].main_image.picture}
                          title={galleryData[i].title}
                          cost={galleryData[i].cost}
                          url={`/${galleryData[i].type}/${galleryData[i].url_title}`}
                          location={address}
                    />
                </div>);
                rowList.push(card);
            }

        }
        return rowList;
    }
}

export default CategoryList;