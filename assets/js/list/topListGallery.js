import React from 'react';
import TopListGalleryCard from './topListGalleryCard';
import ax from '../ax';


class TopListGallery extends React.Component {

    constructor(props) {
        super(props);
        this.state = {home_data: [], experience_data: []};
    }

    loadHomesFromServer() {
        ax.get("/api/best_homes")
            .then(function(response){
                this.setState({home_data:response.data});
            }.bind(this));
    }

    loadExperiencesFromServer() {
        ax.get("/api/best_experiences")
            .then(function(response){
                this.setState({experience_data:response.data});
            }.bind(this));
    }

    componentDidMount() {
        this.loadHomesFromServer();
        this.loadExperiencesFromServer();
    }

    render() {
        var tmp_length = this.state.experience_data.length;
        var galleryData = this.state.experience_data;

        if (tmp_length==this.props.length && this.state.home_data.length>0)
            galleryData[this.props.length-1] = this.state.home_data[0];

        else {
            for (var i = 0; i < this.props.length; i++)
                if (i >= galleryData.length && (i - tmp_length) < this.state.home_data.length) {
                    galleryData.push(this.state.home_data[i - tmp_length]);
                }
        }

        var rowList = [];
        for (var i = 0; i < galleryData.length && i < this.props.length; i++) {
            var address = galleryData[i].address.province+"، "+galleryData[i].address.city;
            if (galleryData[i].address.town)
                address+="، "+galleryData[i].address.town;

            var card = (<div key={'galleryCard'+i} className="toplist-gallery-card">
                <TopListGalleryCard src={galleryData[i].main_image.picture}
                      title={galleryData[i].title}
                      cost={galleryData[i].cost}
                      url={`/${galleryData[i].type}/${galleryData[i].url_title}`}
                      location={address}
                />
            </div>);
            rowList.push(card);
        }
        return rowList;
    }
}
export default TopListGallery;