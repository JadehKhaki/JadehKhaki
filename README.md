# Jadeh Khaki Project

## Installation

1. Create the bundle:
```shell
cd <Project directory>
npm install
npx webpack --config webpack.config.js
```

2. Virtual environment:
```shell
source <path_to_your_virtual_environment>/bin/activate
pip install -r requirements.txt
```

3. Migrate and run server:
```shell
python3 manage.py makemigrations
python3 manage.py migrate
python3 manage.py runserver
```
